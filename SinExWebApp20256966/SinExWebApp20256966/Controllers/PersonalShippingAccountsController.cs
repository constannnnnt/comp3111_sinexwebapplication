﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;
using System.ComponentModel.DataAnnotations;

namespace SinExWebApp20256966.Controllers
{
    public class PersonalShippingAccountsController : AccountController
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: PersonalShippingAccounts/Details/5
        public ActionResult Details(int? id)
        {
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            if (userName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            id = db.ShippingAccounts.SingleOrDefault(c => c.UserName == userName).ShippingAccountId;
            PersonalShippingAccount personalShippingAccount = (PersonalShippingAccount)db.ShippingAccounts.Find(id);
            if (personalShippingAccount == null)
            {
                return HttpNotFound();
            }
            return View(personalShippingAccount);
        }

        // GET: PersonalShippingAccounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PersonalShippingAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShippingAccountId,AddressBuilding,AddressStreet,AddressCity,AddressProvinceCode,AddressPostalCode,CardType,CardNumber,CardSecurityNumber,CardHolderName,CardExpireMonth,CardExpireYear,PhoneNumber,Email,FirstName,LastName")] PersonalShippingAccount personalShippingAccount)
        {
            if (ModelState.IsValid)
            {
                // db.ShippingAccounts.Add(personalShippingAccount);
                // db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(personalShippingAccount);
        }

        // GET: PersonalShippingAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            if (userName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            id = db.ShippingAccounts.SingleOrDefault(c => c.UserName == userName).ShippingAccountId;
            PersonalShippingAccount personalShippingAccount = (PersonalShippingAccount) db.ShippingAccounts.Find(id);
            if (personalShippingAccount == null)
            {
                return HttpNotFound();
            }
            return View(personalShippingAccount);
        }

        // POST: PersonalShippingAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShippingAccountId,AddressBuilding,AddressStreet,AddressCity,AddressProvinceCode,AddressPostalCode,CardType,CardNumber,CardSecurityNumber,CardHolderName,CardExpireMonth,CardExpireYear,PhoneNumber,Email,FirstName,LastName")] PersonalShippingAccount personalShippingAccount)
        {
            if (personalShippingAccount.CardExpireYear == "2017" && Convert.ToInt16(personalShippingAccount.CardExpireMonth) <= 4)
            {
                ViewBag.ExpiryDate = "The card is already expired.";
                return View(personalShippingAccount);
            }

            int CityProvinceMatch = 0;
            int ProvincePostalMatch = 0;
            int CardTypeNumberMatch = 0;

            var pCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
            foreach (var item in pCityProvince)
            {
                if (item.City == personalShippingAccount.AddressCity && item.ProvinceCode == personalShippingAccount.AddressProvinceCode)
                {
                    CityProvinceMatch = 1;
                }
            }

            if (personalShippingAccount.AddressPostalCode == null)
            {
                ProvincePostalMatch = 1;
            }

            if (CityProvinceMatch == 1 && personalShippingAccount.AddressPostalCode != null)
            {
                ProvincePostalMatch = ValidateCityPostalCode(personalShippingAccount.AddressPostalCode, personalShippingAccount.AddressCity);
                if (ProvincePostalMatch == 0)
                {
                    ProvincePostalMatch = ValidateProvincePostalCode(personalShippingAccount.AddressPostalCode, personalShippingAccount.AddressProvinceCode);
                }
            }

            CardTypeNumberMatch = ValidateCard(personalShippingAccount.CardType, personalShippingAccount.CardNumber, personalShippingAccount.CardSecurityNumber);

            if (CityProvinceMatch == 0) ViewBag.CityPronvince = "Your City and Province Code do not match.";
            if (ProvincePostalMatch == 0) ViewBag.ProvincePostal = "Your City/Province Code and Postal Code do not match.";
            if (CardTypeNumberMatch == 0) ViewBag.CardTypeNumber = "Your Card Type and Number do not match or your security code don't match with cardtype.";

            if (ModelState.IsValid && (CityProvinceMatch == 1) && (ProvincePostalMatch == 1) && (CardTypeNumberMatch == 1))
            {
                personalShippingAccount.UserName = User.Identity.Name;
                db.Entry(personalShippingAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(personalShippingAccount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
