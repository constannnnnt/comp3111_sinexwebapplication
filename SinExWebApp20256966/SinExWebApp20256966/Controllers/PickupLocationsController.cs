﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.Controllers
{
    public class PickupLocationsController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: PickupLocations
        public ActionResult Index()
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            var pickupLocations = db.PickupLocations.Where(s => s.ShippingAccountId == account.ShippingAccountId).Include(p => p.ShippingAccount);
            return View(pickupLocations.ToList());
        }

        // GET: PickupLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PickupLocation pickupLocation = db.PickupLocations.Find(id);
            if (pickupLocation == null)
            {
                return HttpNotFound();
            }
            return View(pickupLocation);
        }

        // GET: PickupLocations/Create
        public ActionResult Create()
        {
            ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName");
            return View();
        }

        // POST: PickupLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PickupLocationID,NickName,Location,ShippingAccountId")] PickupLocation pickupLocation)
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            pickupLocation.ShippingAccountId = account.ShippingAccountId;
            int i = db.PickupLocations.Where(s => s.NickName.Equals(pickupLocation.NickName)).Count();

            if (ModelState.IsValid && i==0)
            {
                db.PickupLocations.Add(pickupLocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (i != 0)
            {
                ViewBag.msg = "The NickName already exists!";
            }
            ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", pickupLocation.ShippingAccountId);
            return View(pickupLocation);
        }

        // GET: PickupLocations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PickupLocation pickupLocation = db.PickupLocations.Find(id);
            if (pickupLocation == null)
            {
                return HttpNotFound();
            }
            ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", pickupLocation.ShippingAccountId);
            return View(pickupLocation);
        }

        // POST: PickupLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PickupLocationID,NickName,Location,ShippingAccountId")] PickupLocation pickupLocation)
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            pickupLocation.ShippingAccountId = account.ShippingAccountId;
            int i = db.PickupLocations.Where(s => s.NickName.Equals(pickupLocation.NickName) && s.PickupLocationID != pickupLocation.PickupLocationID).Count();

            if (ModelState.IsValid && i==0)
            {
                db.Entry(pickupLocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (i != 0)
            {
                ViewBag.msg = "The NickName already exists!";
            }
            ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", pickupLocation.ShippingAccountId);
            return View(pickupLocation);
        }

        // GET: PickupLocations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PickupLocation pickupLocation = db.PickupLocations.Find(id);
            if (pickupLocation == null)
            {
                return HttpNotFound();
            }
            return View(pickupLocation);
        }

        // POST: PickupLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PickupLocation pickupLocation = db.PickupLocations.Find(id);
            db.PickupLocations.Remove(pickupLocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
