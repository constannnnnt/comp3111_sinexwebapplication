﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();
        public double ConvertCurrency(string CurrencyCode, double CurrentValue)
        {
            if (Session[CurrencyCode] == null) {
                var Currency = db.Currencies.Select(c => c.CurrencyCode).ToList();
                foreach(var currency in Currency)
                {
                    Session[currency] = db.Currencies.SingleOrDefault(c => c.CurrencyCode == currency).ExchangeRate;
                }
                return (CurrentValue * (double)Session[CurrencyCode]);
            } else
            {
                double exchangeRate = (double)Session[CurrencyCode];
                return (exchangeRate * CurrentValue);
            }
        }
    }
}