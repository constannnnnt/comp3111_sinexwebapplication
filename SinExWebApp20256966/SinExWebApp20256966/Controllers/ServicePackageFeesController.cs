﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;
using SinExWebApp20256966.ViewModels;
using System.Text.RegularExpressions;

namespace SinExWebApp20256966.Controllers
{
    public class ServicePackageFeesController : BaseController
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        public string checkweight(int type, decimal weight)
        {
            if (weight == 0 && type != 1)
            {
                return "Weight should be a positive decimal number in kg to one decimal place.";
            }
            if (weight > 100)
            {
                return "It's too heavy!!!";
            }

            return "np";
        }

        
        public decimal calcost(string package, decimal fee, decimal rate, decimal MinimunFee, decimal penaltyfee, decimal we)
        {

            decimal weightlimit, temp = 0;
            if (package == "Envelope - 250x350mm")
            {
                return fee * rate;
            }
            else
            {
                

                if (package == "Pak - small - 350x400mm" || package == "Pak - large - 450x550mm")
                {
                    weightlimit = 5;
                    temp = fee * we;
                    if (we > weightlimit)
                    {
                        temp = fee * we + penaltyfee;
                    }
                }
                else if (package == "Box - small - 300x250x150mm")
                {
                    weightlimit = 10;
                    temp = fee * we;
                    if (we > weightlimit)
                    {
                        temp = fee * we + penaltyfee;
                    }
                }
                else if (package == "Box - medium - 400x350x250mm")
                {
                    weightlimit = 20;
                    temp = fee * we;
                    if (we > weightlimit)
                    {
                        temp = fee * we + penaltyfee;
                    }
                }
                else if (package == "Box - large - 500x450x350mm")
                {
                    weightlimit = 30;
                    temp = fee * we;
                    if (we > weightlimit)
                    {
                        temp = fee * we + penaltyfee;
                    }
                }
                else
                {
                    temp = fee * we;
                }

                if (temp < MinimunFee)
                {
                    temp = MinimunFee;
                }
            }

                return temp*rate;
        }


        public ActionResult GetPackageCost(string origin, string destination, string ServiceType, string Currency, int? PackageNumber, IList<string> PackageInfo, IList<string> WeightInfo, string submit)
        {
            var CostCalculationModel = new CostCalculationViewModel();

            var serviceTypes = new SelectList(db.ServiceTypes.OrderBy(c => c.ServiceTypeID).Select(c => c.Type).Distinct());
            var origins = new SelectList(db.Destinations.Select(c => c.City).Distinct());
            var destinations = new SelectList(db.Destinations.Select(c => c.City).Distinct());
            var sizes = new SelectList(db.PackageTypeSizes.OrderBy(c => c.PackageTypeID).Select(c => c.Size).Distinct());
            var currencies = new SelectList(db.Currencies.Select(c => c.CurrencyCode).Distinct());

            CostCalculationModel.ServiceTypes = serviceTypes.ToList();
            CostCalculationModel.Origins = origins.ToList();
            CostCalculationModel.Destinations = destinations.ToList();
            CostCalculationModel.Sizes = sizes.ToList();
            CostCalculationModel.CurrencyCodes = currencies.ToList();

            if (String.IsNullOrEmpty(origin) || String.IsNullOrEmpty(destination) || String.IsNullOrEmpty(Currency) || String.IsNullOrEmpty(ServiceType) || submit == "Try Again")
            {
                ViewBag.result = false;
                return View(CostCalculationModel);
            }
            else
            {
                ViewBag.result = true;
                ViewBag.calculation = false;
            }

            decimal cost = 0;
            List<string> SingleCost = new List<string>();
            if (submit == "Create a package")
            {
                if (PackageNumber == 10)
                {
                    ViewBag.packagenumber = PackageNumber;
                    ViewBag.packageinfo = PackageInfo;
                    ViewBag.weightinfo = WeightInfo;
                    ViewBag.msg = "The maximum package number is 10.";
                    return View(CostCalculationModel);
                }

                int ii = Convert.ToInt16(PackageNumber);
                string package = PackageInfo[ii];
                string weight = WeightInfo[ii];

                if (String.IsNullOrEmpty(package))
                {
                    ViewBag.packagenumber = PackageNumber;
                    ViewBag.packageinfo = PackageInfo;
                    ViewBag.weightinfo = WeightInfo;
                    ViewBag.msg = "Please select a package type.";
                    return View(CostCalculationModel);
                }

                var PID = db.PackageTypeSizes.Where(s => s.Size == package).Select(s => s.PackageTypeID).Distinct().Max();

                decimal we = 0;
                if (PID == 1)
                {
                    WeightInfo[ii] = "";
                }
                else if (Regex.IsMatch(weight, @"^\d+(?:\.\d)?$"))
                {
                    we = Convert.ToDecimal(weight);
                }


                if (checkweight(PID, we) != "np")
                {
                    ViewBag.packagenumber = PackageNumber;
                    ViewBag.packageinfo = PackageInfo;
                    ViewBag.weightinfo = WeightInfo;
                    ViewBag.msg = checkweight(PID, we);
                    return View(CostCalculationModel);
                }
                /*
                if (we == 0 && PID != 1)
                {
                    ViewBag.packagenumber = PackageNumber;
                    ViewBag.packageinfo = PackageInfo;
                    ViewBag.weightinfo = WeightInfo;
                    ViewBag.msg = "Weight should be a positive decimal number in kg to one decimal place.";
                    return View(CostCalculationModel);
                }

                if (we > 100)
                {
                    ViewBag.packagenumber = PackageNumber;
                    ViewBag.packageinfo = PackageInfo;
                    ViewBag.weightinfo = WeightInfo;
                    ViewBag.msg = "It's too heavy!!!";
                    return View(CostCalculationModel);
                }
                //else
                {
                    //ViewBag.packagenumber = PackageNumber;
                    //ViewBag.packageinfo = PackageInfo;
                    //ViewBag.weightinfo = WeightInfo;
                    //ViewBag.msg = "Weight should be a positive decimal number in kg to one decimal place.";
                    //return View(CostCalculationModel);
                }
                */
                ViewBag.packagenumber = PackageNumber + 1;
                ViewBag.packageinfo = PackageInfo;
                ViewBag.weightinfo = WeightInfo;
                return View(CostCalculationModel);
            }
            else if (submit == "Stop and calculate total cost")
            {
                decimal penaltyfee = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
                for (int i = 0; i < PackageNumber; i++)
                {
                    string package = PackageInfo[i];
                    string weight = WeightInfo[i];

                    var PID = db.PackageTypeSizes.Where(s => s.Size == package).Select(s => s.PackageTypeID).Distinct();
                    int packagetypeid = PID.Max();

                    var SID = db.ServiceTypes.Where(s => s.Type == ServiceType).Select(s => s.ServiceTypeID).Distinct();
                    int servicetypeid = SID.Max();

                    var ER = db.Currencies.Where(s => s.CurrencyCode == Currency).Select(s => s.ExchangeRate).Distinct();
                    double exchangerate = ER.Max();
                    decimal rate = Convert.ToDecimal(exchangerate);

                    decimal we = 0;
                    if (PID.Max() == 1)
                    {
                    }
                    else
                    {
                        we = Convert.ToDecimal(weight);
                    }

                    decimal Fee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.Fee).Max());
                    decimal MinimunFee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.MinimumFee).Max());

                    decimal result = calcost(package, Fee, rate, MinimunFee, penaltyfee, we);
                    cost += result;
                    SingleCost.Add(Convert.ToString(result));
                    /*
                    if (package == "Envelope - 250x350mm")
                    {
                        cost += Fee * rate;
                        SingleCost.Add(Convert.ToString(Fee * rate));
                    }
                    else
                    {
                        decimal weightlimit, temp = 0;

                        if (package == "Pak - small - 350x400mm" || package == "Pak - large - 450x550mm")
                        {
                            weightlimit = 5;
                            temp = Fee * we;
                            if (we > weightlimit)
                            {
                                temp = Fee * we + penaltyfee;
                            }
                        }
                        else if (package == "Box - small - 300x250x150mm")
                        {
                            weightlimit = 10;
                            temp = Fee * we;
                            if (we > weightlimit)
                            {
                                temp = Fee * we + penaltyfee;
                            }
                        }
                        else if (package == "Box - medium - 400x350x250mm")
                        {
                            weightlimit = 20;
                            temp = Fee * we;
                            if (we > weightlimit)
                            {
                                temp = Fee * we + penaltyfee;
                            }
                        }
                        else if (package == "Box - large - 500x450x350mm")
                        {
                            weightlimit = 30;
                            temp = Fee * we;
                            if (we > weightlimit)
                            {
                                temp = Fee * we + penaltyfee;
                            }
                        }
                        else
                        {
                            temp = Fee * we;
                        }

                        if (temp < MinimunFee)
                        {
                            temp = MinimunFee;
                        }

                        cost += temp * rate;
                        SingleCost.Add(Convert.ToString(temp * rate));
                    }*/
                }
                ViewBag.packagenumber = PackageNumber;
                ViewBag.singlecost = SingleCost;
                ViewBag.packageinfo = PackageInfo;
                ViewBag.weightinfo = WeightInfo;
                ViewBag.calculation = true;
                ViewBag.TotalCost = cost;
                ViewBag.Currecny = Currency;
                ViewBag.servicetype = ServiceType;
                return View(CostCalculationModel);
            }
            else
            {
                ViewBag.packagenumber = 0;
                ViewBag.packageinfo = PackageInfo;
                ViewBag.weightinfo = WeightInfo;
                return View(CostCalculationModel);
            }

        }


        // GET: ServicePackageFees
        public ActionResult Index2()
        {
            var servicePackageFees = db.ServicePackageFees.Include(s => s.PackageType).Include(s => s.ServiceType);
            return View(servicePackageFees.ToList());
        }
        public ActionResult Index(string CurrencyCode)
        {
            decimal penaltyfee = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
            ViewBag.penaltyfee = penaltyfee;

            var servicePackageFeesViewModel = new ServicePackageFeesViewModel();
            var currencies = new SelectList(db.Currencies.Select(c => c.CurrencyCode).Distinct());
            var ServiceTypes = db.ServiceTypes.OrderBy(c => c.ServiceTypeID);
            var PackageTypes = db.PackageTypes.OrderBy(c => c.PackageTypeID);
            servicePackageFeesViewModel.Fees = db.ServicePackageFees.Include(c => c.PackageType).Include(c => c.ServiceType);
            servicePackageFeesViewModel.ServiceTypes = ServiceTypes.Select(c => c.Type).Distinct().ToList();
            servicePackageFeesViewModel.PackageTypes = PackageTypes.Select(c => c.Type).Distinct().ToList();

            if (String.IsNullOrEmpty(CurrencyCode))
            {
                CurrencyCode = "CNY";
            }
            ViewBag.CurrencyCode = CurrencyCode;
            servicePackageFeesViewModel.CurrencyCode = CurrencyCode;
            servicePackageFeesViewModel.CurrencyCodes = currencies.ToList();
            switch (CurrencyCode)
            {
                case "HKD":
                    ViewBag.codesign = "HK$";
                    break;
                case "TWD":
                    ViewBag.codesign = "NT$";
                    break;
                case "MOP":
                    ViewBag.codesign = "MOP$";
                    break;
                default:
                    ViewBag.codesign = "¥";
                    break;
            }
            foreach(ServicePackageFee fee in servicePackageFeesViewModel.Fees)
            {
                fee.Fee = this.ConvertCurrency(CurrencyCode, fee.Fee);
                fee.MinimumFee = this.ConvertCurrency(CurrencyCode, fee.MinimumFee);
            }
            /*
            var servicePackageFees = db.ServicePackageFees.Include(s => s.PackageType).Include(s => s.ServiceType);
            var currencyQuery = new SelectList(db.Currencies.Select(c => c.CurrencyCode).Distinct());
            // var newquery = currencyQuery.ToList();
            ViewBag.Currency = currencyQuery.ToList();
            */
            return View(servicePackageFeesViewModel);
        }

        public bool checknull(string CurrencyCode, string Origin, string Destination, string Package, string Service, string Size)
        {
            
            if (!(String.IsNullOrEmpty(CurrencyCode)) && !(String.IsNullOrEmpty(Origin))
                && !(String.IsNullOrEmpty(Destination)) && !(String.IsNullOrEmpty(Package))
                && !(String.IsNullOrEmpty(Service)) && !(String.IsNullOrEmpty(Size)) )
            { return true; }
            else
            {
                return false;
            }
            
        }




        public ActionResult getCost(string CurrencyCode, string Origin,
            string Destination, string Package, string Size, string Service, double? weight)
        {
            var costViewModel = new ShipmentCostViewModel();
            var serviceTypes = new SelectList(db.ServiceTypes.OrderBy(c => c.ServiceTypeID).Select(c => c.Type).Distinct());
            var packageTypes = new SelectList(db.PackageTypes.OrderBy(c => c.PackageTypeID).Select(c => c.Type).Distinct());
            var origins = new SelectList(db.Destinations.Select(c => c.City).Distinct());
            var destinations = new SelectList(db.Destinations.Select(c => c.City).Distinct());
            var sizes = new SelectList(db.PackageTypeSizes.OrderBy(c => c.PackageTypeID).Select(c => c.Size).Distinct());
            costViewModel.Destinations = destinations.ToList();
            costViewModel.Origins = origins.ToList();
            costViewModel.ServiceTypesList = serviceTypes.ToList();
            costViewModel.PackageTypeList = packageTypes.ToList();
            costViewModel.Sizes = sizes.ToList();
            var currencies = new SelectList(db.Currencies.Select(c => c.CurrencyCode).Distinct());
            var ServiceTypes = db.ServiceTypes.OrderBy(c => c.ServiceTypeID);
            var PackageTypes = db.PackageTypes.OrderBy(c => c.PackageTypeID);
            costViewModel.Fees = db.ServicePackageFees.Include(c => c.PackageType).Include(c => c.ServiceType);
            costViewModel.ServiceTypes = ServiceTypes.Select(c => c.Type).Distinct().ToList();
            costViewModel.PackageTypes = PackageTypes.Select(c => c.Type).Distinct().ToList();

            if (String.IsNullOrEmpty(CurrencyCode))
            {
                CurrencyCode = "CNY";
            }
            if (String.IsNullOrEmpty(Origin))
            {
                Origin = "NA";
            }
            if (String.IsNullOrEmpty(Destination))
            {
                Destination = "NA";
            }
            if (String.IsNullOrEmpty(Package))
            {
                Package = "NA";
            }
            if (String.IsNullOrEmpty(Service))
            {
                Service = "NA";
            }
            if (String.IsNullOrEmpty(Size))
            {
                Size = "NA";
            }
            costViewModel.Rates = db.Currencies.SingleOrDefault(c => c.CurrencyCode == CurrencyCode).ExchangeRate;
            ViewBag.CurrencyCode = CurrencyCode;
            costViewModel.CurrencyCode = CurrencyCode;
            costViewModel.CurrencyCodes = currencies.ToList();
            ViewBag.Origin = Origin;
            ViewBag.Destination = Destination;
            ViewBag.PackageType = Package;
            ViewBag.ServiceType = Service;
            ViewBag.Weight = weight;
            costViewModel.Origin = Origin;
            costViewModel.Destination = Destination;
            costViewModel.Service = Service;
            costViewModel.Package = Package;
            costViewModel.Size = Size;
            switch (CurrencyCode)
            {
                case "HKD":
                    ViewBag.codesign = "HK$";
                    break;
                case "TWD":
                    ViewBag.codesign = "NT$";
                    break;
                case "MOP":
                    ViewBag.codesign = "MOP$";
                    break;
                default:
                    ViewBag.codesign = "¥";
                    break;
            }
            foreach (ServicePackageFee fee in costViewModel.Fees)
            {
                fee.Fee *= costViewModel.Rates;
                fee.MinimumFee *= costViewModel.Rates;
            }

            //if (!(String.IsNullOrEmpty(CurrencyCode)) && !(String.IsNullOrEmpty(Origin)) 
            //&& !(String.IsNullOrEmpty(Destination)) && !(String.IsNullOrEmpty(Package))
            //&& !(String.IsNullOrEmpty(Service)) && !(String.IsNullOrEmpty(Size)) && (weight != null))
            
            if (checknull(CurrencyCode, Origin, Destination, Package, Service, Size) && (weight != null))
            {
                IEnumerable<PackageTypeSize> Sizes = db.PackageTypeSizes.Include(c => c.PackageType);
                double minFee = 0;
                double Fee = 0;
                foreach (ServicePackageFee fee in costViewModel.Fees)
                {
                    if ((fee.PackageType.Type == Package) && (fee.ServiceType.Type == Service))
                    {
                        Fee = fee.Fee;
                        minFee = fee.MinimumFee;
                    }
                }
                string weightLimit = "";
                foreach(var size in Sizes)
                {
                    if ((size.PackageType.Type == Package) && (size.Size == Size))
                    {
                        ViewBag.status = 1;
                        weightLimit = size.WeightLimit;

                        costViewModel.Weight = (double)weight;
                        if (Package == "Envelope")
                        {
                            costViewModel.Cost = Fee;
                        }
                        else if (Package == "Customer")
                        {
                            double cost = Fee * (double)weight;
                            if (cost < minFee)
                            {
                                costViewModel.Cost = minFee;
                            }
                            else
                            {
                                costViewModel.Cost = cost;
                            }
                        }
                        else
                        {
                            int wLength = weightLimit.Length - 2;
                            if (wLength <= 0)
                            {
                                wLength = 0;
                            }
                            string subWeightLimit = weightLimit.Substring(0, wLength);
                            double dweightLimit = 0;
                            if (subWeightLimit.Length > 0)
                            {
                                dweightLimit = Convert.ToDouble(subWeightLimit);
                                double cost = Fee * dweightLimit;
                                if (cost < minFee)
                                {
                                    cost = minFee;
                                }
                                if (weight > dweightLimit)
                                {
                                    cost += 500 * costViewModel.Rates;
                                }
                                costViewModel.Cost = cost;
                            }
                        }
                    }
                }
            }
            return View(costViewModel);
        }

        // GET: ServicePackageFees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicePackageFee servicePackageFee = db.ServicePackageFees.Find(id);
            if (servicePackageFee == null)
            {
                return HttpNotFound();
            }
            return View(servicePackageFee);
        }

        // GET: ServicePackageFees/Create
        public ActionResult Create()
        {
            ViewBag.PackageTypeID = new SelectList(db.PackageTypes, "PackageTypeID", "Type");
            ViewBag.ServiceTypeID = new SelectList(db.ServiceTypes, "ServiceTypeID", "Type");
            return View();
        }

        // POST: ServicePackageFees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServicePackageFeeID,Fee,MinimumFee,PackageTypeID,ServiceTypeID")] ServicePackageFee servicePackageFee)
        {
            if (ModelState.IsValid)
            {
                db.ServicePackageFees.Add(servicePackageFee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PackageTypeID = new SelectList(db.PackageTypes, "PackageTypeID", "Type", servicePackageFee.PackageTypeID);
            ViewBag.ServiceTypeID = new SelectList(db.ServiceTypes, "ServiceTypeID", "Type", servicePackageFee.ServiceTypeID);
            return View(servicePackageFee);
        }

        public ActionResult NewEdit(string PackageType, string ServiceType, double? fee, double? minimunfee)
        {
            ViewBag.PackageType = new SelectList(db.PackageTypes, "Type", "Type");
            ViewBag.ServiceType = new SelectList(db.ServiceTypes, "Type", "Type");
            if (PackageType == null || ServiceType == null || fee == null || minimunfee == null)
            {
                return View();
            }

            if(fee <= 0 || minimunfee <= 0)
            {
                ViewBag.errorMsg = "Please input positive fees.";
                return View();
            }

            int PID = db.PackageTypes.Where(s => s.Type == PackageType).Select(s => s.PackageTypeID).Single();
            int SID = db.ServiceTypes.Where(s => s.Type == ServiceType).Select(s => s.ServiceTypeID).Single();
            ServicePackageFee servicePackageFee = db.ServicePackageFees.Where(s => s.PackageTypeID == PID).Where(s => s.ServiceTypeID == SID).Single();
            servicePackageFee.Fee = (double) fee;
            servicePackageFee.MinimumFee = (double) minimunfee;
            db.Entry(servicePackageFee).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: ServicePackageFees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicePackageFee servicePackageFee = db.ServicePackageFees.Find(id);
            if (servicePackageFee == null)
            {
                return HttpNotFound();
            }
            ViewBag.PackageTypeID = new SelectList(db.PackageTypes, "PackageTypeID", "Type", servicePackageFee.PackageTypeID);
            ViewBag.ServiceTypeID = new SelectList(db.ServiceTypes, "ServiceTypeID", "Type", servicePackageFee.ServiceTypeID);
            return View(servicePackageFee);
        }

        // POST: ServicePackageFees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServicePackageFeeID,Fee,MinimumFee,PackageTypeID,ServiceTypeID")] ServicePackageFee servicePackageFee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicePackageFee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PackageTypeID = new SelectList(db.PackageTypes, "PackageTypeID", "Type", servicePackageFee.PackageTypeID);
            ViewBag.ServiceTypeID = new SelectList(db.ServiceTypes, "ServiceTypeID", "Type", servicePackageFee.ServiceTypeID);
            return View(servicePackageFee);
        }

        // GET: ServicePackageFees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicePackageFee servicePackageFee = db.ServicePackageFees.Find(id);
            if (servicePackageFee == null)
            {
                return HttpNotFound();
            }
            return View(servicePackageFee);
        }

        // POST: ServicePackageFees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServicePackageFee servicePackageFee = db.ServicePackageFees.Find(id);
            db.ServicePackageFees.Remove(servicePackageFee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
