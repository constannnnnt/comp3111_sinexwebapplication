﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.Controllers
{
    public class penaltyfeesController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: penaltyfees
        public ActionResult Index()
        {
            return View(db.penaltyfees.ToList());
        }

        // GET: penaltyfees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            penaltyfee penaltyfee = db.penaltyfees.Find(id);
            if (penaltyfee == null)
            {
                return HttpNotFound();
            }
            return View(penaltyfee);
        }

        // GET: penaltyfees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: penaltyfees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "penaltyfeeID,fee")] penaltyfee penaltyfee)
        {
            if (ModelState.IsValid)
            {
                db.penaltyfees.Add(penaltyfee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(penaltyfee);
        }

        // GET: penaltyfees/Edit/5
        public ActionResult Edit(/*int? id*/)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }*/
            penaltyfee penaltyfee = db.penaltyfees.Find(1);
            if (penaltyfee == null)
            {
                return HttpNotFound();
            }

            return View(penaltyfee);
        }

        // POST: penaltyfees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "penaltyfeeID,fee")] penaltyfee penaltyfee)
        {
            if (penaltyfee.fee >= 0)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(penaltyfee).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index", "ServicePackageFees");
                }
            }
            return View(penaltyfee);
        }

        // GET: penaltyfees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            penaltyfee penaltyfee = db.penaltyfees.Find(id);
            if (penaltyfee == null)
            {
                return HttpNotFound();
            }
            return View(penaltyfee);
        }

        // POST: penaltyfees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            penaltyfee penaltyfee = db.penaltyfees.Find(id);
            db.penaltyfees.Remove(penaltyfee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
