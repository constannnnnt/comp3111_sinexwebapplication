﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.Controllers
{
    public class BusinessShippingAccountsController : AccountController
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: BusinessShippingAccounts
        public ActionResult Index()
        {
            return View(db.ShippingAccounts.ToList());
        }

        
        // GET: BusinessShippingAccounts/Details/5
        public ActionResult Details(int? id)
        {
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            if (userName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            id = db.ShippingAccounts.SingleOrDefault(c => c.UserName == userName).ShippingAccountId;
            BusinessShippingAccount businessShippingAccount = (BusinessShippingAccount)db.ShippingAccounts.Find(id);
            if (businessShippingAccount == null)
            {
                return HttpNotFound();
            }
            return View(businessShippingAccount);
        }
        // GET: BusinessShippingAccounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BusinessShippingAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShippingAccountId,AddressBuilding,AddressStreet,AddressCity,AddressProvinceCode,AddressPostalCode,CardType,CardNumber,CardSecurityNumber,CardHolderName,CardExpireMonth,CardExpireYear,PhoneNumber,Email,ContactPersonName,CompanyName,Department")] BusinessShippingAccount businessShippingAccount)
        {
            if (ModelState.IsValid)
            {
                db.ShippingAccounts.Add(businessShippingAccount);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(businessShippingAccount);
        }

        // GET: BusinessShippingAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            if (userName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            id = db.ShippingAccounts.SingleOrDefault(c => c.UserName == userName).ShippingAccountId;
            BusinessShippingAccount businessShippingAccount = (BusinessShippingAccount)db.ShippingAccounts.Find(id);
            if (businessShippingAccount == null)
            {
                return HttpNotFound();
            }
            return View(businessShippingAccount);
        }

        // POST: BusinessShippingAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShippingAccountId,AddressBuilding,AddressStreet,AddressCity,AddressProvinceCode,AddressPostalCode,CardType,CardNumber,CardSecurityNumber,CardHolderName,CardExpireMonth,CardExpireYear,PhoneNumber,Email,ContactPersonName,CompanyName,Department")] BusinessShippingAccount businessShippingAccount)
        {
            if (businessShippingAccount.CardExpireYear == "2017" && Convert.ToInt16(businessShippingAccount.CardExpireMonth) <= 4)
            {
                ViewBag.ExpiryDate = "The card is already expired.";
                return View(businessShippingAccount);
            }

            int CityProvinceMatch = 0;
            int ProvincePostalMatch = 0;
            int CardTypeNumberMatch = 0;

            var pCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
            foreach (var item in pCityProvince)
            {
                if (item.City == businessShippingAccount.AddressCity && item.ProvinceCode == businessShippingAccount.AddressProvinceCode)
                {
                    CityProvinceMatch = 1;
                }
            }

            if (businessShippingAccount.AddressPostalCode == null)
            {
                ProvincePostalMatch = 1;
            }

            if (CityProvinceMatch == 1 && businessShippingAccount.AddressPostalCode != null)
            {
                ProvincePostalMatch = ValidateCityPostalCode(businessShippingAccount.AddressPostalCode, businessShippingAccount.AddressCity);
                if (ProvincePostalMatch == 0)
                {
                    ProvincePostalMatch = ValidateProvincePostalCode(businessShippingAccount.AddressPostalCode, businessShippingAccount.AddressProvinceCode);
                }
            }

            CardTypeNumberMatch = ValidateCard(businessShippingAccount.CardType, businessShippingAccount.CardNumber, businessShippingAccount.CardSecurityNumber);

            if (CityProvinceMatch == 0) ViewBag.CityPronvince = "Your City and Province Code do not match.";
            if (ProvincePostalMatch == 0) ViewBag.ProvincePostal = "Your City/Province Code and Postal Code do not match.";
            if (CardTypeNumberMatch == 0) ViewBag.CardTypeNumber = "Your Card Type and Number do not match or your security code don't match with cardtype.";


            if (ModelState.IsValid && (CityProvinceMatch == 1) && (ProvincePostalMatch == 1) && (CardTypeNumberMatch == 1))
            {
                businessShippingAccount.UserName = User.Identity.Name;
                db.Entry(businessShippingAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(businessShippingAccount);
        }

        /*
        // GET: BusinessShippingAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BusinessShippingAccount businessShippingAccount = db.ShippingAccounts.Find(id);
            if (businessShippingAccount == null)
            {
                return HttpNotFound();
            }
            return View(businessShippingAccount);
        }

        // POST: BusinessShippingAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BusinessShippingAccount businessShippingAccount = db.ShippingAccounts.Find(id);
            db.ShippingAccounts.Remove(businessShippingAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        */
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
