﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.Controllers
{
    public class DestinationsController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: Destinations
        public ActionResult Index()
        {
            return View(db.Destinations.ToList());
        }

        public ActionResult CheckProvinceCode(string addressProvinceCode)
        {
            var result = db.Destinations.Where(c => c.ProvinceCode == addressProvinceCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Destinations/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // GET: Destinations/Create
        //[Authorize(Roles = "Employee")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Destinations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "City,ProvinceCode")] Destination destination)
        {
            var validProvince = db.Destinations.Select(c => c.ProvinceCode).Distinct().ToList();
            int flag = 0;
            foreach (var province in validProvince)
            {
                if (province == destination.ProvinceCode)
                {
                    flag = 1;
                }
            }
            if (destination.ProvinceCode == "FJ") flag = 1; // In the destinations, it does not include Fujian but we should handle it.
            var validCity = db.Destinations.Select(c => c.City).Distinct().ToList();
            int flag2 = 1;
            foreach (var city in validCity)
            {
                if (city == destination.City)
                {
                    flag2 = 0;
                }
            }
            if (ModelState.IsValid && flag == 1 && (flag2 == 1))
            {
                switch(destination.ProvinceCode)
                {
                    case "HK":
                        destination.CurrencyCode = "HKD";
                        break;
                    case "MC":
                        destination.CurrencyCode = "MOP";
                        break;
                    case "TW":
                        destination.CurrencyCode = "TWD";
                        break;
                    default:
                        destination.CurrencyCode = "CNY";
                        break;
                }
                db.Destinations.Add(destination);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (flag == 0) ViewBag.validProvince = "You must add cities based on the province codes in China.";
            if (flag2 == 0) ViewBag.validCity = "The city you added exists already. Cities in China have different names.";
            return View(destination);
        }

        // GET: Destinations/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // POST: Destinations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "City,ProvinceCode")] Destination destination)
        {
            var validProvince = db.Destinations.Select(c => c.ProvinceCode).Distinct().ToList();
            int flag = 0;
            foreach (var province in validProvince)
            {
                if (province == destination.ProvinceCode)
                {
                    flag = 1;
                }
            }
            if (destination.ProvinceCode == "FJ") flag = 1; // In the destinations, it does not include Fujian but we should handle it.
            var validCity = db.Destinations.Select(c => c.City).Distinct().ToList();
            int flag2 = 1;
            foreach (var city in validCity)
            {
                if (city == destination.City)
                {
                    flag2 = 0;
                }
            }
            if (ModelState.IsValid && flag == 1 && flag2 == 1)
            {
                db.Entry(destination).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (flag == 0) ViewBag.validProvince = "You must add cities based on the province codes in China.";
            if (flag2 == 0) ViewBag.validCity = "The city you added exists already. Cities in China have different names.";

            return View(destination);
        }

        // GET: Destinations/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Destination destination = db.Destinations.Find(id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // POST: Destinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Destination destination = db.Destinations.Find(id);
            db.Destinations.Remove(destination);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
