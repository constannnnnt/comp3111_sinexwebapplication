﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;
using System.Text.RegularExpressions;

namespace SinExWebApp20256966.Controllers
{
    public class RecipientAddressesController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        // GET: RecipientAddresses
        public ActionResult Index()
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            var recipientAddresses = db.RecipientAddresses.Where(s => s.ShippingAccountId == account.ShippingAccountId).Include(r => r.ShippingAccount);
            return View(recipientAddresses.ToList());
        }

        // GET: RecipientAddresses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipientAddress recipientAddress = db.RecipientAddresses.Find(id);
            if (recipientAddress == null)
            {
                return HttpNotFound();
            }
            return View(recipientAddress);
        }

        // GET: RecipientAddresses/Create
        public ActionResult Create()
        {
            var lists = new RecipientAddress();
            var Cities = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            ViewBag.City = Cities.ToList();
            //ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName");
            return View();
        }

        // POST: RecipientAddresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecipientAddressID,NickName,Building,Street,City,Province,PostalCode,ShippingAccountId")] RecipientAddress recipientAddress)
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            recipientAddress.ShippingAccountId = account.ShippingAccountId;
            recipientAddress.Province = db.Destinations.SingleOrDefault(c => c.City == recipientAddress.City).ProvinceCode;
            int i = db.RecipientAddresses.Where(s => s.NickName == recipientAddress.NickName).Count();

            if (ModelState.IsValid && i==0)
            {
                int CityProvinceMatch = 0;
                int ProvincePostalMatch = 0;

                var pCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
                foreach (var item in pCityProvince)
                {
                    if (item.City == recipientAddress.City && (item.ProvinceCode == recipientAddress.Province))
                    {
                        CityProvinceMatch = 1;
                    }
                }

                if (recipientAddress.PostalCode == null)
                {
                    ProvincePostalMatch = 1;
                } else if (CityProvinceMatch == 1 && recipientAddress.PostalCode != null)
                {
                    ProvincePostalMatch = ValidateProvincePostalCode(recipientAddress.PostalCode, recipientAddress.Province);
                }

                if (CityProvinceMatch == 1 && ProvincePostalMatch == 1)
                {
                    db.RecipientAddresses.Add(recipientAddress);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                } else
                {
                    if (CityProvinceMatch == 0) ViewBag.validCityProvince = "The City & Province don't match";
                    if (ProvincePostalMatch == 0) ViewBag.validProvincePostal = "The Province & postal don't match";
                }
            }
            if (i != 0)
            {
                ViewBag.validNickName = "The nickname exists already.";
            }

            var lists = new RecipientAddress();
            var Cities = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            ViewBag.City = Cities.ToList();
            //ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", recipientAddress.ShippingAccountId);
            return View(recipientAddress);
        }

        // GET: RecipientAddresses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipientAddress recipientAddress = db.RecipientAddresses.Find(id);
            if (recipientAddress == null)
            {
                return HttpNotFound();
            }
            var lists = new RecipientAddress();
            var Cities = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            ViewBag.City = Cities.ToList();
            //ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", recipientAddress.ShippingAccountId);
            return View(recipientAddress);
        }

        // POST: RecipientAddresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RecipientAddressID,NickName,Building,Street,City,Province,PostalCode,ShippingAccountId")] RecipientAddress recipientAddress)
        {
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            recipientAddress.ShippingAccountId = account.ShippingAccountId;
            String city = recipientAddress.City;
            recipientAddress.Province = db.Destinations.Where(s => s.City == city).Select(s => s.ProvinceCode).Max();
            int i = db.RecipientAddresses.Where(s => s.NickName == recipientAddress.NickName && s.RecipientAddressID != recipientAddress.RecipientAddressID).Count();

            if (ModelState.IsValid && i == 0)
            {
                int CityProvinceMatch = 0;
                int ProvincePostalMatch = 0;

                var pCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
                foreach (var item in pCityProvince)
                {
                    if (item.City == recipientAddress.City && (item.ProvinceCode == recipientAddress.Province))
                    {
                        CityProvinceMatch = 1;
                    }
                }

                if (recipientAddress.PostalCode == null)
                {
                    ProvincePostalMatch = 1;
                }
                else if (CityProvinceMatch == 1 && recipientAddress.PostalCode != null)
                {
                    ProvincePostalMatch = ValidateProvincePostalCode(recipientAddress.PostalCode, recipientAddress.Province);
                }

                if (CityProvinceMatch == 1 && ProvincePostalMatch == 1)
                {
                    db.Entry(recipientAddress).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    if (CityProvinceMatch == 0) ViewBag.validCityProvince = "The City & Province don't match";
                    if (ProvincePostalMatch == 0) ViewBag.validProvincePostal = "The Province & postal don't match";
                }
            }

            if (i != 0 )
            {
                ViewBag.validNickName = "The nickname exists already.";
            }

            var lists = new RecipientAddress();
            var Cities = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            ViewBag.City = Cities.ToList();
            //ViewBag.ShippingAccountId = new SelectList(db.ShippingAccounts, "ShippingAccountId", "UserName", recipientAddress.ShippingAccountId);
            return View(recipientAddress);
        }

        public int ValidateProvincePostalCode(string postalCode, string province)
        {
            int ProvincePostalMatch = 0;
            string ReResult = Regex.Match(postalCode, @"^[0-9][0-9]").Value;
            string ReResult_TW = Regex.Match(postalCode, @"^[1-9][0-9][0-9]").Value;
            string ReResult_SameProvince = Regex.Match(postalCode, @"^[0-9][0-9][0-9][0-9]").Value;

            switch (province)
            {
                case "BJ":
                    if (ReResult == "10") ProvincePostalMatch = 1; // beijing
                    break;
                case "GZ":
                    if (ReResult == "55" || ReResult == "56") ProvincePostalMatch = 1; // gui zhou
                    break;
                case "NM":
                    if (ReResult == "02" || ReResult == "01") ProvincePostalMatch = 1; // nei meng
                    break;
                case "JS":
                    if (ReResult == "21" || ReResult == "22") ProvincePostalMatch = 1; // jiang su
                    break;
                case "HE":
                    if (ReResult == "30") ProvincePostalMatch = 1; // Tianjin
                    if (ReResult == "05" || ReResult == "06" || ReResult == "07") ProvincePostalMatch = 1; // Heibei district
                    break;
                case "JL":
                    if (ReResult == "13") ProvincePostalMatch = 1; // Jilin
                    break;
                case "HI":
                    if (ReResult == "57") ProvincePostalMatch = 1; // hai nan
                    break;
                case "SD":
                    if (ReResult == "25" || ReResult == "27" || ReResult == "26") ProvincePostalMatch = 1; //Shan dong
                    break;
                case "JX":
                    if (ReResult == "33" || ReResult == "34") ProvincePostalMatch = 1; // Jiangxi
                    break;
                case "GX":
                    if (ReResult == "53" || ReResult == "54") ProvincePostalMatch = 1; // Guangxi
                    break;
                case "XJ": // XIN JIANG
                    if (ReResult == "83") ProvincePostalMatch = 1;
                    if (ReResult == "84") ProvincePostalMatch = 1;
                    break;
                case "HN":
                    if (ReResult == "41" || ReResult == "42") ProvincePostalMatch = 1; // Hunan
                    break;
                case "HB":
                    if (ReResult == "43" || ReResult == "44") ProvincePostalMatch = 1; // Hubei
                    break;
                case "HL":
                    if (ReResult == "16") ProvincePostalMatch = 1;
                    if (ReResult == "15") ProvincePostalMatch = 1; // Heilongjiang
                    break;
                case "SC":
                    if (ReResult == "61" || ReResult == "62" || ReResult == "63" || ReResult == "64") ProvincePostalMatch = 1; // SICHUAN
                    break;
                case "ZJ":
                    if (ReResult == "31" || ReResult == "32") ProvincePostalMatch = 1; // ZHE JIANG
                    break;
                case "YN":
                    if (ReResult == "65" || ReResult == "66" || ReResult == "67") ProvincePostalMatch = 1; // YUNNAN
                    break;
                case "SH":
                    if (ReResult == "20") ProvincePostalMatch = 1; //SHANG HAI
                    break;
                case "SN":
                    if (ReResult == "71" || ReResult == "72") ProvincePostalMatch = 1; //SHANNA XI
                    break;
                case "CQ":
                    if (ReResult == "40") ProvincePostalMatch = 1; //CHONG QING
                    break;
                case "GS":
                    if (ReResult == "73" || ReResult == "74") ProvincePostalMatch = 1;
                    break;
                case "LN":
                    if (ReResult == "11" || ReResult == "12") ProvincePostalMatch = 1; // Liaoning 
                    break;
                case "QH":
                    if (ReResult == "81") ProvincePostalMatch = 1; //QINGHAI
                    break;
                case "AH":
                    if (ReResult == "23" || ReResult == "24") ProvincePostalMatch = 1; // AN HUI
                    break;
                case "XZ":
                    if (ReResult == "85" || ReResult == "86") ProvincePostalMatch = 1; // Tibet
                    break;
                case "NX":
                    if (ReResult == "75") ProvincePostalMatch = 1; //NING XIA
                    break;
                case "MC":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // MACAU
                    break;
                case "TW":
                    if ((ReResult_TW == "100") ||
                        (ReResult_TW == "103") ||
                        (ReResult_TW == "104") ||
                        (ReResult_TW == "105") ||
                        (ReResult_TW == "106") ||
                        (ReResult_TW == "108") ||
                        (ReResult_TW == "110") ||
                        (ReResult_TW == "111") ||
                        (ReResult_TW == "112") ||
                        (ReResult_TW == "114") ||
                        (ReResult_TW == "115") ||
                        (ReResult_TW == "116") ||
                        (ReResult_TW == "200") ||
                        (ReResult_TW == "201") ||
                        (ReResult_TW == "202") ||
                        (ReResult_TW == "203") ||
                        (ReResult_TW == "204") ||
                        (ReResult_TW == "205") ||
                        (ReResult_TW == "206") ||
                        (ReResult_TW == "209") ||
                        (ReResult_TW == "210") ||
                        (ReResult_TW == "211") ||
                        (ReResult_TW == "212") ||
                        (ReResult_TW == "207") ||
                        (ReResult_TW == "208") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "221") ||
                        (ReResult_TW == "222") ||
                        (ReResult_TW == "223") ||
                        (ReResult_TW == "224") ||
                        (ReResult_TW == "226") ||
                        (ReResult_TW == "227") ||
                        (ReResult_TW == "228") ||
                        (ReResult_TW == "231") ||
                        (ReResult_TW == "232") ||
                        (ReResult_TW == "233") ||
                        (ReResult_TW == "234") ||
                        (ReResult_TW == "235") ||
                        (ReResult_TW == "236") ||
                        (ReResult_TW == "237") ||
                        (ReResult_TW == "238") ||
                        (ReResult_TW == "239") ||
                        (ReResult_TW == "241") ||
                        (ReResult_TW == "242") ||
                        (ReResult_TW == "243") ||
                        (ReResult_TW == "244") ||
                        (ReResult_TW == "245") ||
                        (ReResult_TW == "248") ||
                        (ReResult_TW == "249") ||
                        (ReResult_TW == "251") ||
                        (ReResult_TW == "252") ||
                        (ReResult_TW == "253") ||
                        (ReResult_TW == "260") ||
                        (ReResult_TW == "261") ||
                        (ReResult_TW == "262") ||
                        (ReResult_TW == "263") ||
                        (ReResult_TW == "264") ||
                        (ReResult_TW == "265") ||
                        (ReResult_TW == "266") ||
                        (ReResult_TW == "267") ||
                        (ReResult_TW == "268") ||
                        (ReResult_TW == "269") ||
                        (ReResult_TW == "270") ||
                        (ReResult_TW == "272") ||
                        (ReResult_TW == "290") ||
                        (ReResult_TW == "300") ||
                        (ReResult_TW == "301") ||
                        (ReResult_TW == "302") ||
                        (ReResult_TW == "303") ||
                        (ReResult_TW == "304") ||
                        (ReResult_TW == "305") ||
                        (ReResult_TW == "306") ||
                        (ReResult_TW == "307") ||
                        (ReResult_TW == "308") ||
                        (ReResult_TW == "310") ||
                        (ReResult_TW == "311") ||
                        (ReResult_TW == "312") ||
                        (ReResult_TW == "313") ||
                        (ReResult_TW == "314") ||
                        (ReResult_TW == "315") ||
                        (ReResult_TW == "320") ||
                        (ReResult_TW == "324") ||
                        (ReResult_TW == "325") ||
                        (ReResult_TW == "326") ||
                        (ReResult_TW == "327") ||
                        (ReResult_TW == "328") ||
                        (ReResult_TW == "330") ||
                        (ReResult_TW == "333") ||
                        (ReResult_TW == "334") ||
                        (ReResult_TW == "335") ||
                        (ReResult_TW == "336") ||
                        (ReResult_TW == "337") ||
                        (ReResult_TW == "338") ||
                        (ReResult_TW == "350") ||
                        (ReResult_TW == "351") ||
                        (ReResult_TW == "352") ||
                        (ReResult_TW == "353") ||
                        (ReResult_TW == "354") ||
                        (ReResult_TW == "356") ||
                        (ReResult_TW == "357") ||
                        (ReResult_TW == "358") ||
                        (ReResult_TW == "360") ||
                        (ReResult_TW == "361") ||
                        (ReResult_TW == "362") ||
                        (ReResult_TW == "363") ||
                        (ReResult_TW == "364") ||
                        (ReResult_TW == "365") ||
                        (ReResult_TW == "366") ||
                        (ReResult_TW == "367") ||
                        (ReResult_TW == "368") ||
                        (ReResult_TW == "369") ||
                        (ReResult_TW == "400") ||
                        (ReResult_TW == "401") ||
                        (ReResult_TW == "402") ||
                        (ReResult_TW == "403") ||
                        (ReResult_TW == "404") ||
                        (ReResult_TW == "407") ||
                        (ReResult_TW == "408") ||
                        (ReResult_TW == "411") ||
                        (ReResult_TW == "412") ||
                        (ReResult_TW == "413") ||
                        (ReResult_TW == "414") ||
                        (ReResult_TW == "420") ||
                        (ReResult_TW == "421") ||
                        (ReResult_TW == "422") ||
                        (ReResult_TW == "423") ||
                        (ReResult_TW == "424") ||
                        (ReResult_TW == "426") ||
                        (ReResult_TW == "427") ||
                        (ReResult_TW == "428") ||
                        (ReResult_TW == "429") ||
                        (ReResult_TW == "432") ||
                        (ReResult_TW == "433") ||
                        (ReResult_TW == "434") ||
                        (ReResult_TW == "435") ||
                        (ReResult_TW == "436") ||
                        (ReResult_TW == "437") ||
                        (ReResult_TW == "438") ||
                        (ReResult_TW == "439") ||
                        (ReResult_TW == "500") ||
                        (ReResult_TW == "502") ||
                        (ReResult_TW == "503") ||
                        (ReResult_TW == "504") ||
                        (ReResult_TW == "505") ||
                        (ReResult_TW == "506") ||
                        (ReResult_TW == "507") ||
                        (ReResult_TW == "508") ||
                        (ReResult_TW == "509") ||
                        (ReResult_TW == "510") ||
                        (ReResult_TW == "511") ||
                        (ReResult_TW == "512") ||
                        (ReResult_TW == "513") ||
                        (ReResult_TW == "514") ||
                        (ReResult_TW == "515") ||
                        (ReResult_TW == "516") ||
                        (ReResult_TW == "520") ||
                        (ReResult_TW == "521") ||
                        (ReResult_TW == "522") ||
                        (ReResult_TW == "523") ||
                        (ReResult_TW == "524") ||
                        (ReResult_TW == "525") ||
                        (ReResult_TW == "526") ||
                        (ReResult_TW == "527") ||
                        (ReResult_TW == "528") ||
                        (ReResult_TW == "530") ||
                        (ReResult_TW == "540") ||
                        (ReResult_TW == "541") ||
                        (ReResult_TW == "542") ||
                        (ReResult_TW == "544") ||
                        (ReResult_TW == "545") ||
                        (ReResult_TW == "546") ||
                        (ReResult_TW == "551") ||
                        (ReResult_TW == "552") ||
                        (ReResult_TW == "553") ||
                        (ReResult_TW == "555") ||
                        (ReResult_TW == "556") ||
                        (ReResult_TW == "557") ||
                        (ReResult_TW == "558") ||
                        (ReResult_TW == "600") ||
                        (ReResult_TW == "602") ||
                        (ReResult_TW == "604") ||
                        (ReResult_TW == "605") ||
                        (ReResult_TW == "606") ||
                        (ReResult_TW == "607") ||
                        (ReResult_TW == "608") ||
                        (ReResult_TW == "611") ||
                        (ReResult_TW == "612") ||
                        (ReResult_TW == "613") ||
                        (ReResult_TW == "614") ||
                        (ReResult_TW == "615") ||
                        (ReResult_TW == "616") ||
                        (ReResult_TW == "621") ||
                        (ReResult_TW == "622") ||
                        (ReResult_TW == "623") ||
                        (ReResult_TW == "624") ||
                        (ReResult_TW == "625") ||
                        (ReResult_TW == "630") ||
                        (ReResult_TW == "631") ||
                        (ReResult_TW == "632") ||
                        (ReResult_TW == "633") ||
                        (ReResult_TW == "634") ||
                        (ReResult_TW == "635") ||
                        (ReResult_TW == "637") ||
                        (ReResult_TW == "638") ||
                        (ReResult_TW == "640") ||
                        (ReResult_TW == "643") ||
                        (ReResult_TW == "654") ||
                        (ReResult_TW == "636") ||
                        (ReResult_TW == "646") ||
                        (ReResult_TW == "647") ||
                        (ReResult_TW == "648") ||
                        (ReResult_TW == "649") ||
                        (ReResult_TW == "651") ||
                        (ReResult_TW == "652") ||
                        (ReResult_TW == "653") ||
                        (ReResult_TW == "655") ||
                        (ReResult_TW == "700") ||
                        (ReResult_TW == "701") ||
                        (ReResult_TW == "702") ||
                        (ReResult_TW == "704") ||
                        (ReResult_TW == "708") ||
                        (ReResult_TW == "709") ||
                        (ReResult_TW == "710") ||
                        (ReResult_TW == "711") ||
                        (ReResult_TW == "712") ||
                        (ReResult_TW == "713") ||
                        (ReResult_TW == "714") ||
                        (ReResult_TW == "715") ||
                        (ReResult_TW == "716") ||
                        (ReResult_TW == "717") ||
                        (ReResult_TW == "718") ||
                        (ReResult_TW == "719") ||
                        (ReResult_TW == "720") ||
                        (ReResult_TW == "721") ||
                        (ReResult_TW == "722") ||
                        (ReResult_TW == "723") ||
                        (ReResult_TW == "724") ||
                        (ReResult_TW == "725") ||
                        (ReResult_TW == "726") ||
                        (ReResult_TW == "727") ||
                        (ReResult_TW == "730") ||
                        (ReResult_TW == "731") ||
                        (ReResult_TW == "732") ||
                        (ReResult_TW == "733") ||
                        (ReResult_TW == "734") ||
                        (ReResult_TW == "735") ||
                        (ReResult_TW == "736") ||
                        (ReResult_TW == "737") ||
                        (ReResult_TW == "741") ||
                        (ReResult_TW == "742") ||
                        (ReResult_TW == "743") ||
                        (ReResult_TW == "744") ||
                        (ReResult_TW == "800") ||
                        (ReResult_TW == "801") ||
                        (ReResult_TW == "802") ||
                        (ReResult_TW == "803") ||
                        (ReResult_TW == "804") ||
                        (ReResult_TW == "805") ||
                        (ReResult_TW == "806") ||
                        (ReResult_TW == "807") ||
                        (ReResult_TW == "811") ||
                        (ReResult_TW == "812") ||
                        (ReResult_TW == "813") ||
                        (ReResult_TW == "814") ||
                        (ReResult_TW == "815") ||
                        (ReResult_TW == "817") ||
                        (ReResult_TW == "819") ||
                        (ReResult_TW == "820") ||
                        (ReResult_TW == "821") ||
                        (ReResult_TW == "822") ||
                        (ReResult_TW == "823") ||
                        (ReResult_TW == "824") ||
                        (ReResult_TW == "825") ||
                        (ReResult_TW == "826") ||
                        (ReResult_TW == "827") ||
                        (ReResult_TW == "828") ||
                        (ReResult_TW == "829") ||
                        (ReResult_TW == "830") ||
                        (ReResult_TW == "831") ||
                        (ReResult_TW == "832") ||
                        (ReResult_TW == "833") ||
                        (ReResult_TW == "840") ||
                        (ReResult_TW == "842") ||
                        (ReResult_TW == "843") ||
                        (ReResult_TW == "844") ||
                        (ReResult_TW == "845") ||
                        (ReResult_TW == "846") ||
                        (ReResult_TW == "847") ||
                        (ReResult_TW == "848") ||
                        (ReResult_TW == "849") ||
                        (ReResult_TW == "851") ||
                        (ReResult_TW == "852") ||
                        (ReResult_TW == "880") ||
                        (ReResult_TW == "881") ||
                        (ReResult_TW == "882") ||
                        (ReResult_TW == "883") ||
                        (ReResult_TW == "884") ||
                        (ReResult_TW == "885") ||
                        (ReResult_TW == "890") ||
                        (ReResult_TW == "891") ||
                        (ReResult_TW == "892") ||
                        (ReResult_TW == "893") ||
                        (ReResult_TW == "894") ||
                        (ReResult_TW == "896") ||
                        (ReResult_TW == "900") ||
                        (ReResult_TW == "901") ||
                        (ReResult_TW == "902") ||
                        (ReResult_TW == "903") ||
                        (ReResult_TW == "904") ||
                        (ReResult_TW == "905") ||
                        (ReResult_TW == "906") ||
                        (ReResult_TW == "907") ||
                        (ReResult_TW == "908") ||
                        (ReResult_TW == "909") ||
                        (ReResult_TW == "911") ||
                        (ReResult_TW == "912") ||
                        (ReResult_TW == "913") ||
                        (ReResult_TW == "920") ||
                        (ReResult_TW == "921") ||
                        (ReResult_TW == "922") ||
                        (ReResult_TW == "923") ||
                        (ReResult_TW == "924") ||
                        (ReResult_TW == "925") ||
                        (ReResult_TW == "926") ||
                        (ReResult_TW == "927") ||
                        (ReResult_TW == "928") ||
                        (ReResult_TW == "931") ||
                        (ReResult_TW == "932") ||
                        (ReResult_TW == "940") ||
                        (ReResult_TW == "941") ||
                        (ReResult_TW == "942") ||
                        (ReResult_TW == "943") ||
                        (ReResult_TW == "944") ||
                        (ReResult_TW == "945") ||
                        (ReResult_TW == "946") ||
                        (ReResult_TW == "947") ||
                        (ReResult_TW == "950") ||
                        (ReResult_TW == "951") ||
                        (ReResult_TW == "952") ||
                        (ReResult_TW == "953") ||
                        (ReResult_TW == "954") ||
                        (ReResult_TW == "955") ||
                        (ReResult_TW == "956") ||
                        (ReResult_TW == "957") ||
                        (ReResult_TW == "958") ||
                        (ReResult_TW == "959") ||
                        (ReResult_TW == "961") ||
                        (ReResult_TW == "962") ||
                        (ReResult_TW == "963") ||
                        (ReResult_TW == "964") ||
                        (ReResult_TW == "965") ||
                        (ReResult_TW == "966") ||
                        (ReResult_TW == "970") ||
                        (ReResult_TW == "971") ||
                        (ReResult_TW == "972") ||
                        (ReResult_TW == "973") ||
                        (ReResult_TW == "974") ||
                        (ReResult_TW == "975") ||
                        (ReResult_TW == "976") ||
                        (ReResult_TW == "977") ||
                        (ReResult_TW == "978") ||
                        (ReResult_TW == "979") ||
                        (ReResult_TW == "981") ||
                        (ReResult_TW == "982") ||
                        (ReResult_TW == "983")) ProvincePostalMatch = 1;
                    break;
                case "GD":
                    if (ReResult == "51" || ReResult == "52") ProvincePostalMatch = 1; //GUANGDONG
                    break;
                case "HK":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // XIANGGANG
                    break;
                case "SX":
                    if (ReResult == "03" || ReResult == "04") ProvincePostalMatch = 1; // SHANXI
                    break;
                case "HA":
                    if (ReResult == "45" || ReResult == "46" || ReResult == "47") ProvincePostalMatch = 1; // HENAN
                    break;
                case "FJ":
                    if (ReResult == "35" || ReResult == "36") ProvincePostalMatch = 1; // FUJIAN
                    break;
            }
            return ProvincePostalMatch;
        }
        // GET: RecipientAddresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipientAddress recipientAddress = db.RecipientAddresses.Find(id);
            if (recipientAddress == null)
            {
                return HttpNotFound();
            }
            return View(recipientAddress);
        }

        // POST: RecipientAddresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RecipientAddress recipientAddress = db.RecipientAddresses.Find(id);
            db.RecipientAddresses.Remove(recipientAddress);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
