﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;
using SinExWebApp20256966.ViewModels;
using X.PagedList;
using System.Text.RegularExpressions;

// Add spaces to allow use of the mail sending functionality.
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace SinExWebApp20256966.Controllers
{
    //[Authorize(Roles ="Customer, Employee")]
    public class ShipmentsController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        [Authorize(Roles = "Customer")]
        // new CREATE
        public ActionResult CreateShipment(string ReferenceNumber, string OriginCity, string RecipientName, string CompanyName, string DepartmentName, string UseSavedAddress, string RecipientAddressNickName, string Building, string Street, string City, string PostalCode, string PhoneNumber, string EmailAddress, string ServiceType, string ShipmentPayer, string DutiesTaxesPayer, string RecipientAccountNumber, string NotifySender, string NotifyRecipient, int? PackageNumber, List<string> TypeSizes, List<string> ContentDescriptions, List<string> CustomerWeights, List<string> Values, List<string> Currencies, string submit)
        {
            var shipmentcreate = new CreateShipmentViewModel();
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);

            string message = "";

            var typesize = new SelectList(db.PackageTypeSizes.OrderBy(c => c.PackageTypeID).Select(c => c.Size).Distinct());
            var currency = new SelectList(db.Currencies.OrderBy(c => c.CurrencyCode).Select(c => c.CurrencyCode).Distinct());
            shipmentcreate.TypeSizeList = typesize.ToList();
            shipmentcreate.CurrencyList = currency.ToList();

            var origincity = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var usesavedaddress = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var recipientaddressnickname = new SelectList(db.RecipientAddresses.Where(s => s.ShippingAccountId == account.ShippingAccountId).Select(s => s.NickName).Distinct());
            var servicetype = new SelectList(db.ServiceTypes.OrderBy(c => c.ServiceTypeID).Select(c => c.Type).Distinct());
            var city = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var shipmentpayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var dutiestaxespayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var notifysender = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var notifyrecipient = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");


            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name);
            if (person is BusinessShippingAccount)
            {
                ViewBag.AccountType = "BusinessShippingAccount";
                BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name);
                ViewBag.FullName = bperson.ContactPersonName + " " + bperson.CompanyName;
            }
            else if (person is PersonalShippingAccount)
            {
                ViewBag.AccountType = "PersonalShippingAccount";
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name);
                ViewBag.FullName = pperson.FirstName + " " + pperson.LastName;
            }
            ViewBag.AccountNo = account.ShippingAccountId.ToString("000000000000");
            ViewBag.MailingAddress = account.AddressBuilding + " " + account.AddressStreet + " " + account.AddressCity + " " + account.AddressProvinceCode;
            ViewBag.PhoneNumber = account.PhoneNumber;
            ViewBag.EmailAddress = account.Email;
            ViewBag.OriginCity = origincity.ToList();

            if (PackageNumber == null)
            {
                ViewBag.packagenumber = 1;
            }
            else
            {
                ViewBag.packagenumber = (int)PackageNumber;
            }
            if (submit == "Add a package")
            {
                ViewBag.packagenumber = (int)PackageNumber + 1;
            }
            if (submit == "Delete a package")
            {
                ViewBag.packagenumber = (int)PackageNumber - 1;
            }

            ViewBag.UseSavedAddress = usesavedaddress.ToList();
            ViewBag.RecipientAddressNickName = recipientaddressnickname.ToList();
            ViewBag.ServiceType = servicetype.ToList();
            ViewBag.City = city.ToList();
            ViewBag.ShipmentPayer = shipmentpayer.ToList();
            ViewBag.DutiesTaxesPayer = dutiestaxespayer.ToList();
            ViewBag.NotifySender = notifysender.ToList();
            ViewBag.NotifyRecipient = notifyrecipient.ToList();



            if (submit == "Create")
            {
                // Recipient Information
                string Province = "";
                if (UseSavedAddress == "Yes")
                {
                    if (String.IsNullOrEmpty(RecipientAddressNickName))
                    {
                        message = "Pleasse select a saved recipient address";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    else
                    {
                        var addressinfo = db.RecipientAddresses.Where(s => s.ShippingAccountId == account.ShippingAccountId).Where(s => s.NickName == RecipientAddressNickName).Single();
                        Building = addressinfo.Building;
                        Street = addressinfo.Street;
                        City = addressinfo.City;
                        Province = addressinfo.Province;
                        PostalCode = addressinfo.PostalCode;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(Street) || String.IsNullOrEmpty(City))
                    {
                        message = "Pleasse input or select Street and City  for delivery purpose.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    } else
                    {
                        Province = db.Destinations.Where(s => s.City == City).Select(s => s.ProvinceCode).Single();
                        var pc = db.Destinations.SingleOrDefault(c => c.City == City).ProvinceCode;
                        //int ProvincePostalMatch = 0;
                        //string ReResult = Regex.Match(PostalCode, @"^[0-9][0-9]").Value;
                        //string ReResult_TW = Regex.Match(PostalCode, @"^[1-9][0-9][0-9]").Value;

                        int ProvincePostalMatch = 0;
                        if (!String.IsNullOrEmpty(PostalCode)) {
                            ProvincePostalMatch = ProvincePostalMatchFunc(pc, PostalCode);
                        }
                        else
                        {
                            ProvincePostalMatch = 1;
                        }

                        if (ProvincePostalMatch == 0)
                        {
                            message = "The City and PostalCode do not match, please check it carefully.";
                            ViewBag.message = message;
                            return View(shipmentcreate);
                        }
                    }
                }

                // Payer
                if (ShipmentPayer == "Recipient" || DutiesTaxesPayer == "Recipient")
                {
                    if (String.IsNullOrEmpty(RecipientAccountNumber))
                    {
                        message = "Pleasse input the Account Number of recipient";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    int accountNum = Convert.ToInt32(RecipientAccountNumber);
                    int recnum = Convert.ToInt32(RecipientAccountNumber);
                    if (db.ShippingAccounts.Where(s => s.ShippingAccountId == recnum).Count() != 1)
                    {
                        message = "Pleasse input a valid Account Number of recipient";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                }
                int ShipmentPayerID = 0, DutiesTaxesPayerID = 0;
                if (ShipmentPayer == "Recipient")
                {
                    ShipmentPayerID = Convert.ToInt32(RecipientAccountNumber);
                }
                else
                {
                    ShipmentPayerID = account.ShippingAccountId;
                }
                if (DutiesTaxesPayer == "Recipient")
                {
                    DutiesTaxesPayerID = Convert.ToInt32(RecipientAccountNumber);
                }
                else
                {
                    DutiesTaxesPayerID = account.ShippingAccountId;
                }

                // Package
                for (int i=0; i<(int)PackageNumber; i++)
                {
                    string PackagesValidationResult = PackagesValidation(TypeSizes[i], ContentDescriptions[i], CustomerWeights[i], Values[i], Currencies[i]);
                    if (PackagesValidationResult != "correct")
                    {
                        ViewBag.message = PackagesValidationResult;
                        return View(shipmentcreate);
                    }
                }
                
                /*for (int i = 0; i < (int)PackageNumber; i++)
                {
                    string temptypesize = TypeSizes[i], tempcontent = ContentDescriptions[i], tempweight = CustomerWeights[i], tempvalue = Values[i], tempcurrency = Currencies[i];
                    if (String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcontent) || String.IsNullOrEmpty(tempvalue) || String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcurrency))
                    {
                        message = "Pleasse input or select all the package information";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (temptypesize != "Envelope - 250x350mm" && String.IsNullOrEmpty(tempweight))
                    {
                        message = "Pleasse input or select all the package information";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Regex.IsMatch(tempweight, @"^\d+(?:\.\d)?$") == false)
                    {
                        message = "Pleasse input a valid weight(a positive number to one decimal place)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Regex.IsMatch(tempvalue, @"^[0-9]*$") == false)
                    {
                        message = "Pleasse input a valid value(a positive integer)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (temptypesize != "Envelope - 250x350mm" && Convert.ToDouble(tempweight) <= 0)
                    {
                        message = "Pleasse input a valid weight(a positive number to one decimal place)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 100)
                    {
                        message = "For safety reasons, please don't add a package weights larger than 100kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 20 && temptypesize == "Pak - large - 450x550mm")
                    {
                        message = "For safety reasons, please don't add a Pak weights larger than 20kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 20 && temptypesize == "Pak - small - 350x400mm")
                    {
                        message = "For safety reasons, please don't add a Pak weights larger than 20kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 1 && temptypesize == "Envelope - 250x350mm")
                    {
                        message = "For safety reasons, please don't add a Envelop weights larger than 1kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                }*/

                
                // Create and Save Shipment
                Shipment NewShipment = new Shipment();
                if (!String.IsNullOrEmpty(ReferenceNumber))
                {
                    NewShipment.ReferenceNumber = ReferenceNumber;
                }
                NewShipment.ServiceType = ServiceType;
                NewShipment.RecipientName = RecipientName;
                NewShipment.NumberOfPackages = (int)PackageNumber;
                NewShipment.ShippingAccountId = account.ShippingAccountId;
                NewShipment.CompanyName = CompanyName;
                NewShipment.DepartmentName = DepartmentName;
                NewShipment.Building = Building;
                NewShipment.Street = Street;
                NewShipment.City = City;
                NewShipment.Origin = db.ShippingAccounts.SingleOrDefault(s=>s.ShippingAccountId == person.ShippingAccountId).AddressCity;
                NewShipment.Destination = City;
                NewShipment.Province = Province;
                NewShipment.PostalCode = PostalCode;
                NewShipment.PhoneNumber = PhoneNumber;
                NewShipment.EmailAddress = EmailAddress;
                NewShipment.ShipmentPayerID = ShipmentPayerID;
                NewShipment.DutiesTaxesPayerID = DutiesTaxesPayerID;
                //NewShipment.ShipmentCurrency = db.Destinations.Where(s => s.City == City).Select(s => s.CurrencyCode).Single();
                //NewShipment.DutiesTaxesCurrency = db.Destinations.Where(s => s.City == OriginCity).Select(s => s.CurrencyCode).Single();
                ShippingAccount shipmentpayersaccount = db.ShippingAccounts.SingleOrDefault(s => s.ShippingAccountId == ShipmentPayerID);
                string shipmentpayersplace = shipmentpayersaccount.AddressCity;
                NewShipment.ShipmentCurrency = db.Destinations.SingleOrDefault(s => s.City == shipmentpayersplace).CurrencyCode;

                ShippingAccount dutytaxpayersaccount = db.ShippingAccounts.SingleOrDefault(s => s.ShippingAccountId == DutiesTaxesPayerID);
                string dutytaxpayersplace = dutytaxpayersaccount.AddressCity;
                NewShipment.DutiesTaxesCurrency = db.Destinations.SingleOrDefault(s => s.City == dutytaxpayersplace).CurrencyCode;

                NewShipment.ConfirmStatus = false;
                NewShipment.DeleteStatus = false;
                NewShipment.PickupStatus = false;

                NewShipment.ShippedDate = new DateTime(2000, 01, 01);
                NewShipment.DeliveredDate = new DateTime(2000, 01, 01);
                NewShipment.PickupTime = new DateTime(2000, 01, 01, 00, 00, 01);

                if (NotifySender == "Yes")
                {
                    NewShipment.NotifySender = true;
                }
                else
                {
                    NewShipment.NotifySender = false;
                }
                if (NotifyRecipient == "Yes")
                {
                    NewShipment.NotifyRecipient = true;
                }
                else
                {
                    NewShipment.NotifyRecipient = false;
                }

                if (ModelState.IsValid)
                {
                    db.Shipments.Add(NewShipment);
                    db.SaveChanges();
                }

                // Create and Save Packages
                for (int i = 0; i < PackageNumber; i++)
                {
                    Package newpackage = new Package();
                    newpackage.TypeSize = TypeSizes[i];
                    newpackage.ContentDescription = ContentDescriptions[i];
                    if (TypeSizes[i] == "Envelope - 250x350mm" && String.IsNullOrEmpty(CustomerWeights[i]))
                    {
                        newpackage.CustomerWeight = 0;
                    }
                    else
                    {
                        newpackage.CustomerWeight = Convert.ToDouble(CustomerWeights[i]);
                    }
                    newpackage.Value = Convert.ToDouble(Values[i]);
                    newpackage.Currency = Currencies[i];
                    newpackage.WaybillId = db.Shipments.Select(s => s.WaybillId).Max();
                    
                    if (ModelState.IsValid)
                    {
                        db.Packages.Add(newpackage);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            
            return View(shipmentcreate);
        }

        public int ProvincePostalMatchFunc(string pc, string PostalCode)
        {
            int ProvincePostalMatch = 0;
            string ReResult = Regex.Match(PostalCode, @"^[0-9][0-9]").Value;
            string ReResult_TW = Regex.Match(PostalCode, @"^[1-9][0-9][0-9]").Value;

            switch (pc)
            {
                case "BJ":
                    if (ReResult == "10") ProvincePostalMatch = 1; // beijing
                    break;
                case "GZ":
                    if (ReResult == "55" || ReResult == "56") ProvincePostalMatch = 1; // gui zhou
                    break;
                case "NM":
                    if (ReResult == "02" || ReResult == "01") ProvincePostalMatch = 1; // nei meng
                    break;
                case "JS":
                    if (ReResult == "21" || ReResult == "22") ProvincePostalMatch = 1; // jiang su
                    break;
                case "HE":
                    if (ReResult == "30") ProvincePostalMatch = 1; // Tianjin
                    if (ReResult == "05" || ReResult == "06" || ReResult == "07") ProvincePostalMatch = 1; // Heibei district
                    break;
                case "JL":
                    if (ReResult == "13") ProvincePostalMatch = 1; // Jilin
                    break;
                case "HI":
                    if (ReResult == "57") ProvincePostalMatch = 1; // hai nan
                    break;
                case "SD":
                    if (ReResult == "25" || ReResult == "27" || ReResult == "26") ProvincePostalMatch = 1; //Shan dong
                    break;
                case "JX":
                    if (ReResult == "33" || ReResult == "34") ProvincePostalMatch = 1; // Jiangxi
                    break;
                case "GX":
                    if (ReResult == "53" || ReResult == "54") ProvincePostalMatch = 1; // Guangxi
                    break;
                case "XJ": // XIN JIANG
                    if (ReResult == "83") ProvincePostalMatch = 1;
                    if (ReResult == "84") ProvincePostalMatch = 1;
                    break;
                case "HN":
                    if (ReResult == "41" || ReResult == "42") ProvincePostalMatch = 1; // Hunan
                    break;
                case "HB":
                    if (ReResult == "43" || ReResult == "44") ProvincePostalMatch = 1; // Hubei
                    break;
                case "HL":
                    if (ReResult == "16") ProvincePostalMatch = 1;
                    if (ReResult == "15") ProvincePostalMatch = 1; // Heilongjiang
                    break;
                case "SC":
                    if (ReResult == "61" || ReResult == "62" || ReResult == "63" || ReResult == "64") ProvincePostalMatch = 1; // SICHUAN
                    break;
                case "ZJ":
                    if (ReResult == "31" || ReResult == "32") ProvincePostalMatch = 1; // ZHE JIANG
                    break;
                case "YN":
                    if (ReResult == "65" || ReResult == "66" || ReResult == "67") ProvincePostalMatch = 1; // YUNNAN
                    break;
                case "SH":
                    if (ReResult == "20") ProvincePostalMatch = 1; //SHANG HAI
                    break;
                case "SN":
                    if (ReResult == "71" || ReResult == "72") ProvincePostalMatch = 1; //SHANNA XI
                    break;
                case "CQ":
                    if (ReResult == "40") ProvincePostalMatch = 1; //CHONG QING
                    break;
                case "GS":
                    if (ReResult == "73" || ReResult == "74") ProvincePostalMatch = 1;
                    break;
                case "LN":
                    if (ReResult == "11" || ReResult == "12") ProvincePostalMatch = 1; // Liaoning 
                    break;
                case "QH":
                    if (ReResult == "81") ProvincePostalMatch = 1; //QINGHAI
                    break;
                case "AH":
                    if (ReResult == "23" || ReResult == "24") ProvincePostalMatch = 1; // AN HUI
                    break;
                case "XZ":
                    if (ReResult == "85" || ReResult == "86") ProvincePostalMatch = 1; // Tibet
                    break;
                case "NX":
                    if (ReResult == "75") ProvincePostalMatch = 1; //NING XIA
                    break;
                case "MC":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // MACAU
                    break;
                case "TW":
                    if ((ReResult_TW == "100") ||
                        (ReResult_TW == "103") ||
                        (ReResult_TW == "104") ||
                        (ReResult_TW == "105") ||
                        (ReResult_TW == "106") ||
                        (ReResult_TW == "108") ||
                        (ReResult_TW == "110") ||
                        (ReResult_TW == "111") ||
                        (ReResult_TW == "112") ||
                        (ReResult_TW == "114") ||
                        (ReResult_TW == "115") ||
                        (ReResult_TW == "116") ||
                        (ReResult_TW == "200") ||
                        (ReResult_TW == "201") ||
                        (ReResult_TW == "202") ||
                        (ReResult_TW == "203") ||
                        (ReResult_TW == "204") ||
                        (ReResult_TW == "205") ||
                        (ReResult_TW == "206") ||
                        (ReResult_TW == "209") ||
                        (ReResult_TW == "210") ||
                        (ReResult_TW == "211") ||
                        (ReResult_TW == "212") ||
                        (ReResult_TW == "207") ||
                        (ReResult_TW == "208") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "221") ||
                        (ReResult_TW == "222") ||
                        (ReResult_TW == "223") ||
                        (ReResult_TW == "224") ||
                        (ReResult_TW == "226") ||
                        (ReResult_TW == "227") ||
                        (ReResult_TW == "228") ||
                        (ReResult_TW == "231") ||
                        (ReResult_TW == "232") ||
                        (ReResult_TW == "233") ||
                        (ReResult_TW == "234") ||
                        (ReResult_TW == "235") ||
                        (ReResult_TW == "236") ||
                        (ReResult_TW == "237") ||
                        (ReResult_TW == "238") ||
                        (ReResult_TW == "239") ||
                        (ReResult_TW == "241") ||
                        (ReResult_TW == "242") ||
                        (ReResult_TW == "243") ||
                        (ReResult_TW == "244") ||
                        (ReResult_TW == "245") ||
                        (ReResult_TW == "248") ||
                        (ReResult_TW == "249") ||
                        (ReResult_TW == "251") ||
                        (ReResult_TW == "252") ||
                        (ReResult_TW == "253") ||
                        (ReResult_TW == "260") ||
                        (ReResult_TW == "261") ||
                        (ReResult_TW == "262") ||
                        (ReResult_TW == "263") ||
                        (ReResult_TW == "264") ||
                        (ReResult_TW == "265") ||
                        (ReResult_TW == "266") ||
                        (ReResult_TW == "267") ||
                        (ReResult_TW == "268") ||
                        (ReResult_TW == "269") ||
                        (ReResult_TW == "270") ||
                        (ReResult_TW == "272") ||
                        (ReResult_TW == "290") ||
                        (ReResult_TW == "300") ||
                        (ReResult_TW == "301") ||
                        (ReResult_TW == "302") ||
                        (ReResult_TW == "303") ||
                        (ReResult_TW == "304") ||
                        (ReResult_TW == "305") ||
                        (ReResult_TW == "306") ||
                        (ReResult_TW == "307") ||
                        (ReResult_TW == "308") ||
                        (ReResult_TW == "310") ||
                        (ReResult_TW == "311") ||
                        (ReResult_TW == "312") ||
                        (ReResult_TW == "313") ||
                        (ReResult_TW == "314") ||
                        (ReResult_TW == "315") ||
                        (ReResult_TW == "320") ||
                        (ReResult_TW == "324") ||
                        (ReResult_TW == "325") ||
                        (ReResult_TW == "326") ||
                        (ReResult_TW == "327") ||
                        (ReResult_TW == "328") ||
                        (ReResult_TW == "330") ||
                        (ReResult_TW == "333") ||
                        (ReResult_TW == "334") ||
                        (ReResult_TW == "335") ||
                        (ReResult_TW == "336") ||
                        (ReResult_TW == "337") ||
                        (ReResult_TW == "338") ||
                        (ReResult_TW == "350") ||
                        (ReResult_TW == "351") ||
                        (ReResult_TW == "352") ||
                        (ReResult_TW == "353") ||
                        (ReResult_TW == "354") ||
                        (ReResult_TW == "356") ||
                        (ReResult_TW == "357") ||
                        (ReResult_TW == "358") ||
                        (ReResult_TW == "360") ||
                        (ReResult_TW == "361") ||
                        (ReResult_TW == "362") ||
                        (ReResult_TW == "363") ||
                        (ReResult_TW == "364") ||
                        (ReResult_TW == "365") ||
                        (ReResult_TW == "366") ||
                        (ReResult_TW == "367") ||
                        (ReResult_TW == "368") ||
                        (ReResult_TW == "369") ||
                        (ReResult_TW == "400") ||
                        (ReResult_TW == "401") ||
                        (ReResult_TW == "402") ||
                        (ReResult_TW == "403") ||
                        (ReResult_TW == "404") ||
                        (ReResult_TW == "407") ||
                        (ReResult_TW == "408") ||
                        (ReResult_TW == "411") ||
                        (ReResult_TW == "412") ||
                        (ReResult_TW == "413") ||
                        (ReResult_TW == "414") ||
                        (ReResult_TW == "420") ||
                        (ReResult_TW == "421") ||
                        (ReResult_TW == "422") ||
                        (ReResult_TW == "423") ||
                        (ReResult_TW == "424") ||
                        (ReResult_TW == "426") ||
                        (ReResult_TW == "427") ||
                        (ReResult_TW == "428") ||
                        (ReResult_TW == "429") ||
                        (ReResult_TW == "432") ||
                        (ReResult_TW == "433") ||
                        (ReResult_TW == "434") ||
                        (ReResult_TW == "435") ||
                        (ReResult_TW == "436") ||
                        (ReResult_TW == "437") ||
                        (ReResult_TW == "438") ||
                        (ReResult_TW == "439") ||
                        (ReResult_TW == "500") ||
                        (ReResult_TW == "502") ||
                        (ReResult_TW == "503") ||
                        (ReResult_TW == "504") ||
                        (ReResult_TW == "505") ||
                        (ReResult_TW == "506") ||
                        (ReResult_TW == "507") ||
                        (ReResult_TW == "508") ||
                        (ReResult_TW == "509") ||
                        (ReResult_TW == "510") ||
                        (ReResult_TW == "511") ||
                        (ReResult_TW == "512") ||
                        (ReResult_TW == "513") ||
                        (ReResult_TW == "514") ||
                        (ReResult_TW == "515") ||
                        (ReResult_TW == "516") ||
                        (ReResult_TW == "520") ||
                        (ReResult_TW == "521") ||
                        (ReResult_TW == "522") ||
                        (ReResult_TW == "523") ||
                        (ReResult_TW == "524") ||
                        (ReResult_TW == "525") ||
                        (ReResult_TW == "526") ||
                        (ReResult_TW == "527") ||
                        (ReResult_TW == "528") ||
                        (ReResult_TW == "530") ||
                        (ReResult_TW == "540") ||
                        (ReResult_TW == "541") ||
                        (ReResult_TW == "542") ||
                        (ReResult_TW == "544") ||
                        (ReResult_TW == "545") ||
                        (ReResult_TW == "546") ||
                        (ReResult_TW == "551") ||
                        (ReResult_TW == "552") ||
                        (ReResult_TW == "553") ||
                        (ReResult_TW == "555") ||
                        (ReResult_TW == "556") ||
                        (ReResult_TW == "557") ||
                        (ReResult_TW == "558") ||
                        (ReResult_TW == "600") ||
                        (ReResult_TW == "602") ||
                        (ReResult_TW == "604") ||
                        (ReResult_TW == "605") ||
                        (ReResult_TW == "606") ||
                        (ReResult_TW == "607") ||
                        (ReResult_TW == "608") ||
                        (ReResult_TW == "611") ||
                        (ReResult_TW == "612") ||
                        (ReResult_TW == "613") ||
                        (ReResult_TW == "614") ||
                        (ReResult_TW == "615") ||
                        (ReResult_TW == "616") ||
                        (ReResult_TW == "621") ||
                        (ReResult_TW == "622") ||
                        (ReResult_TW == "623") ||
                        (ReResult_TW == "624") ||
                        (ReResult_TW == "625") ||
                        (ReResult_TW == "630") ||
                        (ReResult_TW == "631") ||
                        (ReResult_TW == "632") ||
                        (ReResult_TW == "633") ||
                        (ReResult_TW == "634") ||
                        (ReResult_TW == "635") ||
                        (ReResult_TW == "637") ||
                        (ReResult_TW == "638") ||
                        (ReResult_TW == "640") ||
                        (ReResult_TW == "643") ||
                        (ReResult_TW == "654") ||
                        (ReResult_TW == "636") ||
                        (ReResult_TW == "646") ||
                        (ReResult_TW == "647") ||
                        (ReResult_TW == "648") ||
                        (ReResult_TW == "649") ||
                        (ReResult_TW == "651") ||
                        (ReResult_TW == "652") ||
                        (ReResult_TW == "653") ||
                        (ReResult_TW == "655") ||
                        (ReResult_TW == "700") ||
                        (ReResult_TW == "701") ||
                        (ReResult_TW == "702") ||
                        (ReResult_TW == "704") ||
                        (ReResult_TW == "708") ||
                        (ReResult_TW == "709") ||
                        (ReResult_TW == "710") ||
                        (ReResult_TW == "711") ||
                        (ReResult_TW == "712") ||
                        (ReResult_TW == "713") ||
                        (ReResult_TW == "714") ||
                        (ReResult_TW == "715") ||
                        (ReResult_TW == "716") ||
                        (ReResult_TW == "717") ||
                        (ReResult_TW == "718") ||
                        (ReResult_TW == "719") ||
                        (ReResult_TW == "720") ||
                        (ReResult_TW == "721") ||
                        (ReResult_TW == "722") ||
                        (ReResult_TW == "723") ||
                        (ReResult_TW == "724") ||
                        (ReResult_TW == "725") ||
                        (ReResult_TW == "726") ||
                        (ReResult_TW == "727") ||
                        (ReResult_TW == "730") ||
                        (ReResult_TW == "731") ||
                        (ReResult_TW == "732") ||
                        (ReResult_TW == "733") ||
                        (ReResult_TW == "734") ||
                        (ReResult_TW == "735") ||
                        (ReResult_TW == "736") ||
                        (ReResult_TW == "737") ||
                        (ReResult_TW == "741") ||
                        (ReResult_TW == "742") ||
                        (ReResult_TW == "743") ||
                        (ReResult_TW == "744") ||
                        (ReResult_TW == "800") ||
                        (ReResult_TW == "801") ||
                        (ReResult_TW == "802") ||
                        (ReResult_TW == "803") ||
                        (ReResult_TW == "804") ||
                        (ReResult_TW == "805") ||
                        (ReResult_TW == "806") ||
                        (ReResult_TW == "807") ||
                        (ReResult_TW == "811") ||
                        (ReResult_TW == "812") ||
                        (ReResult_TW == "813") ||
                        (ReResult_TW == "814") ||
                        (ReResult_TW == "815") ||
                        (ReResult_TW == "817") ||
                        (ReResult_TW == "819") ||
                        (ReResult_TW == "820") ||
                        (ReResult_TW == "821") ||
                        (ReResult_TW == "822") ||
                        (ReResult_TW == "823") ||
                        (ReResult_TW == "824") ||
                        (ReResult_TW == "825") ||
                        (ReResult_TW == "826") ||
                        (ReResult_TW == "827") ||
                        (ReResult_TW == "828") ||
                        (ReResult_TW == "829") ||
                        (ReResult_TW == "830") ||
                        (ReResult_TW == "831") ||
                        (ReResult_TW == "832") ||
                        (ReResult_TW == "833") ||
                        (ReResult_TW == "840") ||
                        (ReResult_TW == "842") ||
                        (ReResult_TW == "843") ||
                        (ReResult_TW == "844") ||
                        (ReResult_TW == "845") ||
                        (ReResult_TW == "846") ||
                        (ReResult_TW == "847") ||
                        (ReResult_TW == "848") ||
                        (ReResult_TW == "849") ||
                        (ReResult_TW == "851") ||
                        (ReResult_TW == "852") ||
                        (ReResult_TW == "880") ||
                        (ReResult_TW == "881") ||
                        (ReResult_TW == "882") ||
                        (ReResult_TW == "883") ||
                        (ReResult_TW == "884") ||
                        (ReResult_TW == "885") ||
                        (ReResult_TW == "890") ||
                        (ReResult_TW == "891") ||
                        (ReResult_TW == "892") ||
                        (ReResult_TW == "893") ||
                        (ReResult_TW == "894") ||
                        (ReResult_TW == "896") ||
                        (ReResult_TW == "900") ||
                        (ReResult_TW == "901") ||
                        (ReResult_TW == "902") ||
                        (ReResult_TW == "903") ||
                        (ReResult_TW == "904") ||
                        (ReResult_TW == "905") ||
                        (ReResult_TW == "906") ||
                        (ReResult_TW == "907") ||
                        (ReResult_TW == "908") ||
                        (ReResult_TW == "909") ||
                        (ReResult_TW == "911") ||
                        (ReResult_TW == "912") ||
                        (ReResult_TW == "913") ||
                        (ReResult_TW == "920") ||
                        (ReResult_TW == "921") ||
                        (ReResult_TW == "922") ||
                        (ReResult_TW == "923") ||
                        (ReResult_TW == "924") ||
                        (ReResult_TW == "925") ||
                        (ReResult_TW == "926") ||
                        (ReResult_TW == "927") ||
                        (ReResult_TW == "928") ||
                        (ReResult_TW == "931") ||
                        (ReResult_TW == "932") ||
                        (ReResult_TW == "940") ||
                        (ReResult_TW == "941") ||
                        (ReResult_TW == "942") ||
                        (ReResult_TW == "943") ||
                        (ReResult_TW == "944") ||
                        (ReResult_TW == "945") ||
                        (ReResult_TW == "946") ||
                        (ReResult_TW == "947") ||
                        (ReResult_TW == "950") ||
                        (ReResult_TW == "951") ||
                        (ReResult_TW == "952") ||
                        (ReResult_TW == "953") ||
                        (ReResult_TW == "954") ||
                        (ReResult_TW == "955") ||
                        (ReResult_TW == "956") ||
                        (ReResult_TW == "957") ||
                        (ReResult_TW == "958") ||
                        (ReResult_TW == "959") ||
                        (ReResult_TW == "961") ||
                        (ReResult_TW == "962") ||
                        (ReResult_TW == "963") ||
                        (ReResult_TW == "964") ||
                        (ReResult_TW == "965") ||
                        (ReResult_TW == "966") ||
                        (ReResult_TW == "970") ||
                        (ReResult_TW == "971") ||
                        (ReResult_TW == "972") ||
                        (ReResult_TW == "973") ||
                        (ReResult_TW == "974") ||
                        (ReResult_TW == "975") ||
                        (ReResult_TW == "976") ||
                        (ReResult_TW == "977") ||
                        (ReResult_TW == "978") ||
                        (ReResult_TW == "979") ||
                        (ReResult_TW == "981") ||
                        (ReResult_TW == "982") ||
                        (ReResult_TW == "983")) ProvincePostalMatch = 1;
                    break;
                case "GD":
                    if (ReResult == "51" || ReResult == "52") ProvincePostalMatch = 1; //GUANGDONG
                    break;
                case "HK":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // XIANGGANG
                    break;
                case "SX":
                    if (ReResult == "03" || ReResult == "04") ProvincePostalMatch = 1; // SHANXI
                    break;
                case "HA":
                    if (ReResult == "45" || ReResult == "46" || ReResult == "47") ProvincePostalMatch = 1; // HENAN
                    break;
                case "FJ":
                    if (ReResult == "35" || ReResult == "36") ProvincePostalMatch = 1; // FUJIAN
                    break;
            }
            return ProvincePostalMatch;
        }

    public string PackagesValidation(string temptypesize, string tempcontent, string tempweight, string tempvalue, string tempcurrency)
        {
            string result = "correct";
            if (String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcontent) || String.IsNullOrEmpty(tempvalue) || String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcurrency))
            {
                result = "Pleasse input or select all the package information";
            }
            if (temptypesize != "Envelope - 250x350mm" && String.IsNullOrEmpty(tempweight))
            {
                result = "Pleasse input or select all the package information";
            }
            if ((!String.IsNullOrEmpty(tempweight)) && Regex.IsMatch(tempweight, @"^\d+(?:\.\d)?$") == false)
            {
                result = "Pleasse input a valid weight(a positive number to one decimal place)";
            }
            if (Regex.IsMatch(tempvalue, @"^[0-9]*$") == false)
            {
                result = "Pleasse input a valid value(a positive integer)";
            }
            
            if (!String.IsNullOrEmpty(tempweight))
            {
                if (temptypesize != "Envelope - 250x350mm" && Convert.ToDouble(tempweight) <= 0)
                {
                    result = "Pleasse input a valid weight(a positive number to one decimal place)";
                }
                if (Convert.ToDouble(tempweight) >= 100)
                {
                    result = "For safety reasons, please don't add a package weights larger than 100kg.";
                }
                if (Convert.ToDouble(tempweight) >= 20 && (temptypesize == "Pak - large - 450x550mm" || temptypesize == "Pak - small - 350x400mm"))
                {
                    result = "For safety reasons, please don't add a Pak weights larger than 20kg.";
                }
                if (Convert.ToDouble(tempweight) >= 1 && temptypesize == "Envelope - 250x350mm")
                {
                    result = "For safety reasons, please don't add a Envelop weights larger than 1kg.";
                }
            }
            return result;
        }

        // GET: Shipments
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult Index()
        {
            if (User.IsInRole("Customer"))
            {
                string username = System.Web.HttpContext.Current.User.Identity.Name;
                ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
                return View(db.Shipments.Where(s => s.ShippingAccountId == account.ShippingAccountId).Where(s => s.DeleteStatus == false).ToList());
            }
            return View(db.Shipments.ToList());

        }

        // GET: Shipments/Details/5
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment shipment = db.Shipments.Find(id);
            if (shipment == null)
            {
                return HttpNotFound();
            }

            bool temp = shipment.NotifyRecipient;
            if (temp == true)
            {
                ViewBag.NotifyRecipient = true;
            }
            else
            {
                ViewBag.NotifyRecipient = false;
            }

            temp = shipment.NotifySender;
            if (temp == true)
            {
                ViewBag.NotifySender = true;
            }
            else
            {
                ViewBag.NotifySender = false;
            }

            int sender = shipment.ShippingAccountId;
            int sp = shipment.ShipmentPayerID;
            int dp = shipment.DutiesTaxesPayerID;
            ViewBag.waybill = shipment.WaybillId.ToString("0000000000000000");
            ViewBag.ReferenceNumber = shipment.ReferenceNumber;

            if (sender == sp)
            {
                ViewBag.shipmentpayer = "Sender";
            }
            else
            {
                ViewBag.shipmentpayer = "Recipient";
            }

            if (sender == dp)
            {
                ViewBag.dutypayer = "Sender";
            }
            else
            {
                ViewBag.dutypayer = "Recipient";
            }

            //Package Estimated Cost
            int num = 0;
            string services = shipment.ServiceType;
            string currencyc = "CNY";
            double totalcost = 0;
            double totalactualcost = 0;
            double[] packagecost = new double[20];
            double[] actualpackagecost = new double[20];
            

            foreach (Package package in shipment.Packages)
            {
                decimal penaltyfee = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
                decimal cost = 0;
                decimal actualcost = 0;

                string types = package.TypeSize;
                double weight = package.CustomerWeight;
                double? actualweight = package.ActualWeight;
                
                var PID = db.PackageTypeSizes.Where(s => s.Size == types).Select(s => s.PackageTypeID).Distinct();
                int packagetypeid = PID.Max();

                var SID = db.ServiceTypes.Where(s => s.Type == services).Select(s => s.ServiceTypeID).Distinct();
                int servicetypeid = SID.Max();
                decimal Fee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.Fee).Max());
                decimal MinimunFee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.MinimumFee).Max());

                var ER = db.Currencies.Where(s => s.CurrencyCode == currencyc).Select(s => s.ExchangeRate).Distinct();                
                double exchangerate = ER.Max();
                decimal rate = Convert.ToDecimal(exchangerate);

                decimal we = 0;
                decimal actualwe = 0;
                if (packagetypeid == 1)
                {
                }
                else
                {
                    we = Convert.ToDecimal(weight);
                    if (actualweight != null)
                    {
                        actualwe = Convert.ToDecimal(actualweight);
                    }
                }

                if (types == "Envelope - 250x350mm")
                {
                    cost = Fee * rate;
                    if (actualweight != null)
                    {
                        actualcost = Fee * rate;
                    }
                }
                else
                {
                    decimal weightlimit = 0;
                    decimal te = 0;
                    decimal actualte = 0;

                    if (types == "Pak - small - 350x400mm" || types == "Pak - large - 450x550mm")
                    {
                        weightlimit = 5;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - small - 300x250x150mm")
                    {
                        weightlimit = 10;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - medium - 400x350x250mm")
                    {
                        weightlimit = 20;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - large - 500x450x350mm")
                    {
                        weightlimit = 30;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else
                    {
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                    }

                    if (te < MinimunFee)
                    {
                        te = MinimunFee;
                        if (actualweight != null)
                        {
                            actualte = MinimunFee;
                        }
                    }

                    cost = te * rate;
                    if (actualweight != null)
                    {
                        actualcost = actualte * rate;
                    }

                }
                packagecost[num] = Convert.ToDouble(cost);
                if (actualweight != null)
                {
                    actualpackagecost[num] = Convert.ToDouble(actualcost);
                }
                
                num++;
                totalcost = totalcost + Convert.ToDouble(cost);
                if (actualweight != null)
                {
                    totalactualcost += Convert.ToDouble(actualcost);
                }
            }


            ViewBag.currency = "CNY";
            ViewBag.costmsg = "Estimated Cost: ";
            ViewBag.shipmentcost = totalcost;
            ViewBag.acutalshipmentcost = totalactualcost;
            ViewBag.actualcostmsg = "Actual Cost: ";
            ViewBag.packagecost = packagecost;
            ViewBag.actualpackagecost = actualpackagecost;
            int accountid = shipment.ShippingAccountId;
            ShippingAccount account = db.ShippingAccounts.Find(accountid);

            
            if (account is BusinessShippingAccount)
            {
                BusinessShippingAccount bperson = (BusinessShippingAccount)account;
                ViewBag.FullName = bperson.ContactPersonName + " " + bperson.CompanyName;
            }
            else if (account is PersonalShippingAccount)
            {
                
                PersonalShippingAccount pperson = (PersonalShippingAccount)account;
                ViewBag.FullName = pperson.FirstName + " " + pperson.LastName;
            }

            ViewBag.AccountNo = account.ShippingAccountId.ToString("000000000000");
            ViewBag.MailingAddress = account.AddressBuilding + " " + account.AddressStreet + " " + account.AddressCity + " " + account.AddressProvinceCode;
            ViewBag.PhoneNumber = account.PhoneNumber;
            ViewBag.EmailAddress = account.Email;
            
            ViewBag.username = account.UserName;

            int wid = shipment.WaybillId;
            var pl = db.Packages.Where(s => s.WaybillId == wid);
            ViewBag.Packageslist = pl;

            return View(shipment);
        }

        // Invoice Details
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult InvoiceDetails(int? id)
        {

            /*
             * The invoice should show 
             * the shipping account number of the invoice payer, 
             * shipment waybill number,
             * ship (pickup) date, 
             * service type, 
             * sender’s reference number,
             * sender and recipient information (full name and mailing/delivery address), 
             * credit card type, 
             * credit card number (last four digits only), 
             * the authorization code, 
             * total amount payable and, 
             * for each package, the package type, 
             * the actual weight and the cost.
             * The monetary amounts of an invoice should be in the currency of the city in which the payer is located
             */

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment shipment = db.Shipments.Find(id); // id = waybill id
            if (shipment == null)
            {
                return HttpNotFound();
            } // can not find the shipment according to the waybill id

            bool temp = shipment.NotifyRecipient;
            if (temp == true)
            {
                ViewBag.NotifyRecipient = true;
            }
            else
            {
                ViewBag.NotifyRecipient = false;
            }

            temp = shipment.NotifySender;
            if (temp == true)
            {
                ViewBag.NotifySender = true;
            }
            else
            {
                ViewBag.NotifySender = false;
            }

            int sender = shipment.ShippingAccountId;
            int sp = shipment.ShipmentPayerID;
            int dp = shipment.DutiesTaxesPayerID;

            if (sender == sp)
            {
                ViewBag.shipmentpayer = "Sender";
            }
            else
            {
                ViewBag.shipmentpayer = "Recipient";
            }

            if (sender == dp)
            {
                ViewBag.dutypayer = "Sender";
            }
            else
            {
                ViewBag.dutypayer = "Recipient";
            }
            ViewBag.shipmentpayerID = shipment.ShipmentPayerID.ToString("000000000000");
            ViewBag.dutytaxpayerID = shipment.DutiesTaxesPayerID.ToString("000000000000");
            ViewBag.waybillID = shipment.WaybillId.ToString("000000000000000");

            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShippingAccountId);
            if (person is BusinessShippingAccount)
            {
                ViewBag.AccountType = "BusinessShippingAccount";
                BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShippingAccountId);
                ViewBag.SenderFullName = bperson.ContactPersonName + " " + bperson.CompanyName;
            }
            else if (person is PersonalShippingAccount)
            {
                ViewBag.AccountType = "PersonalShippingAccount";
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShippingAccountId);
                ViewBag.SenderFullName = pperson.FirstName + " " + pperson.LastName;
            }
            ViewBag.SenderMailing = person.AddressBuilding + " " + person.AddressStreet + " " + person.AddressCity + " " + person.AddressProvinceCode;
            ViewBag.RecipientFullName = shipment.RecipientName;
            ViewBag.RecipientDeliver = shipment.Building + " " + shipment.Street + " " + shipment.City + " " + shipment.Province;

            var CurID = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShippingAccountId).ShippingAccountId;
            if (User.IsInRole("Customer"))
            {
                CurID = db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name).ShippingAccountId;
            } else
            {
                string ShipmentcreditCardNumber = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShipmentPayerID).CardNumber.ToString();
                ViewBag.ShipmentCreditCardNumber = ShipmentcreditCardNumber.Substring(ShipmentcreditCardNumber.Length - 4, 4);
                ViewBag.ShipmentCreditCardType = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShipmentPayerID).CardType;
                ViewBag.ShipmentAuthorizationCode = shipment.ShipemtnPaymentAuthorizationCode;

                string DutyTaxCardNumber = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.DutiesTaxesPayerID).CardNumber.ToString();
                ViewBag.DutyTaxCreditCardNumber = DutyTaxCardNumber.Substring(DutyTaxCardNumber.Length - 4, 4);
                ViewBag.DutyTaxCreditCardType = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.DutiesTaxesPayerID).CardType;
                ViewBag.DutyTaxAuthorizationCode = shipment.DutiesTaxesPaymentAuthorizationCode;
            }
            
            if (sp == dp && sp == CurID)
            {
                string CreditCardNum = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardNumber.ToString();
                ViewBag.creditCardNumber = CreditCardNum.Substring(CreditCardNum.Length - 4, 4);
                ViewBag.creditCardType = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardType;
                ViewBag.AuthorizationCode = shipment.ShipemtnPaymentAuthorizationCode;
            } else if (sp != dp && sp == CurID)
            {
                string CreditCardNum = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardNumber.ToString();
                ViewBag.creditCardNumber = CreditCardNum.Substring(CreditCardNum.Length - 4, 4);
                ViewBag.creditCardType = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardType;
                ViewBag.AuthorizationCode = shipment.ShipemtnPaymentAuthorizationCode;
            } else if (sp != dp && dp == CurID)
            {
                string CreditCardNum = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardNumber.ToString();
                ViewBag.creditCardNumber = CreditCardNum.Substring(CreditCardNum.Length - 4, 4);
                ViewBag.creditCardType = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).CardType;
                ViewBag.AuthorizationCode = shipment.DutiesTaxesPaymentAuthorizationCode;
            }


            //Package Estimated Cost
            int num = 0;
            string services = shipment.ServiceType;
            string currencyc = "CNY";
            double totalcost = 0;
            double totalactualcost = 0;
            double[] packagecost = new double[20];
            double[] actualpackagecost = new double[20];


            foreach (Package package in shipment.Packages)
            {
                decimal penaltyfee = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
                decimal cost = 0;
                decimal actualcost = 0;

                string types = package.TypeSize;
                double weight = package.CustomerWeight;
                double? actualweight = package.ActualWeight;

                var PID = db.PackageTypeSizes.Where(s => s.Size == types).Select(s => s.PackageTypeID).Distinct();
                int packagetypeid = PID.Max();

                var SID = db.ServiceTypes.Where(s => s.Type == services).Select(s => s.ServiceTypeID).Distinct();
                int servicetypeid = SID.Max();
                decimal Fee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.Fee).Max());
                decimal MinimunFee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.MinimumFee).Max());

                var ER = db.Currencies.Where(s => s.CurrencyCode == currencyc).Select(s => s.ExchangeRate).Distinct();
                double exchangerate = ER.Max();
                decimal rate = Convert.ToDecimal(exchangerate);

                decimal we = 0;
                decimal actualwe = 0;
                if (packagetypeid == 1)
                {
                }
                else
                {
                    we = Convert.ToDecimal(weight);
                    if (actualweight != null)
                    {
                        actualwe = Convert.ToDecimal(actualweight);
                    }
                }

                if (types == "Envelope - 250x350mm")
                {
                    cost = Fee * rate;
                    if (actualweight != null)
                    {
                        actualcost = Fee * rate;
                    }
                }
                else
                {
                    decimal weightlimit = 0;
                    decimal te = 0;
                    decimal actualte = 0;

                    if (types == "Pak - small - 350x400mm" || types == "Pak - large - 450x550mm")
                    {
                        weightlimit = 5;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - small - 300x250x150mm")
                    {
                        weightlimit = 10;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - medium - 400x350x250mm")
                    {
                        weightlimit = 20;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else if (types == "Box - large - 500x450x350mm")
                    {
                        weightlimit = 30;
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                            if (actualweight != null)
                            {
                                actualte = Fee * actualwe + penaltyfee;
                            }
                        }
                    }
                    else
                    {
                        te = Fee * we;
                        if (actualweight != null)
                        {
                            actualte = Fee * actualwe;
                        }
                    }

                    if (te < MinimunFee)
                    {
                        te = MinimunFee;
                        if (actualweight != null)
                        {
                            actualte = MinimunFee;
                        }
                    }

                    cost = te * rate;
                    if (actualweight != null)
                    {
                        actualcost = actualte * rate;
                    }

                }
                packagecost[num] = Convert.ToDouble(cost);
                if (actualweight != null)
                {
                    actualpackagecost[num] = Convert.ToDouble(actualcost);
                }

                num++;
                totalcost = totalcost + Convert.ToDouble(cost);
                if (actualweight != null)
                {
                    totalactualcost += Convert.ToDouble(actualcost);
                }
            }


            ViewBag.currency = "CNY";
            ViewBag.costmsg = "Estimated Cost: ";
            ViewBag.shipmentcost = totalcost;
            ViewBag.acutalshipmentcost = totalactualcost;
            ViewBag.actualcostmsg = "Actual Cost: ";
            ViewBag.packagecost = packagecost;
            ViewBag.actualpackagecost = actualpackagecost;
            ViewBag.shippeddate = shipment.ShippedDate;

            string pncode = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == CurID).AddressProvinceCode;
            switch(pncode)
            {
                case "HK":
                    ViewBag.PayCurrency = "HKD";
                    break;
                case "TW":
                    ViewBag.PayCurrency = "TWD";
                    break;
                case "MC":
                    ViewBag.PayCurrency = "MOP";
                    break;
                default:
                    ViewBag.PayCurrency = "CNY";
                    break;
            }
            
            if ((shipment.ShipmentPayerID == CurID) && (shipment.DutiesTaxesPayerID == CurID))
            {         
                ViewBag.PayAmount = shipment.TotalInvoiceAmount;
            } else
            {
                if (sp == CurID)
                {            
                    ViewBag.PayAmount = totalactualcost;
                } else if (dp == CurID)
                {
                    ViewBag.PayAmount = shipment.DutiesAmount + shipment.TaxesAmount;
                }
            }

            ViewBag.username = System.Web.HttpContext.Current.User.Identity.Name;
            var wid = shipment.WaybillId;
            var pl = db.Packages.Where(s => s.WaybillId == wid);
            ViewBag.Packageslist = pl;

            if (User.IsInRole("Employee"))
            {
                ViewBag.DutyTaxPayCurrency = shipment.DutiesTaxesCurrency;
                ViewBag.DutyTaxPayAmount = shipment.DutiesAmount + shipment.TaxesAmount;
                ViewBag.ShipmentPayCurrency = shipment.ShipmentCurrency;
                ViewBag.ShipmentPayAmount = shipment.ShipmentAmount;
            }

            return View(shipment);
        }

        // GET: Shipments/Edit/5
        [Authorize(Roles = "Customer")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment shipment = db.Shipments.Find(id);
            if (shipment == null)
            {
                return HttpNotFound();
            }
            Session["Waybill"] = id;
            ViewBag.listnumber = shipment.NumberOfPackages;
            var shipmentcreate = new CreateShipmentViewModel();
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);
            
            var typesize = new SelectList(db.PackageTypeSizes.OrderBy(c => c.PackageTypeID).Select(c => c.Size).Distinct());
            var currency = new SelectList(db.Currencies.OrderBy(c => c.CurrencyCode).Select(c => c.CurrencyCode).Distinct());
            shipmentcreate.TypeSizeList = typesize.ToList();
            shipmentcreate.CurrencyList = currency.ToList();

            var origincity = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var usesavedaddress = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var recipientaddressnickname = new SelectList(db.RecipientAddresses.Where(s => s.ShippingAccountId == account.ShippingAccountId).Select(s => s.NickName).Distinct());
            var servicetype = new SelectList(db.ServiceTypes.OrderBy(c => c.ServiceTypeID).Select(c => c.Type).Distinct());
            var city = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var shipmentpayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var dutiestaxespayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var notifysender = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var notifyrecipient = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");

            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(c => c.UserName == username);
            if (person is BusinessShippingAccount)
            {
                ViewBag.AccountType = "BusinessShippingAccount";
                BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.UserName == username);
                ViewBag.FullName = bperson.ContactPersonName + " " + bperson.CompanyName;
            }
            else if (person is PersonalShippingAccount)
            {
                ViewBag.AccountType = "PersonalShippingAccount";
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.UserName == username);
                ViewBag.FullName = pperson.FirstName + " " + pperson.LastName;
            }

            ViewBag.AccountNo = account.ShippingAccountId.ToString("000000000000");
            ViewBag.MailingAddress = account.AddressBuilding + " " + account.AddressStreet + " " + account.AddressCity + " " + account.AddressProvinceCode;
            ViewBag.PhoneNumber = account.PhoneNumber;
            ViewBag.EmailAddress = account.Email;
            ViewBag.OriginCity = origincity.ToList();

            ViewBag.packagenumber = shipment.NumberOfPackages;
            

            ViewBag.UseSavedAddress = usesavedaddress.ToList();
            ViewBag.RecipientAddressNickName = recipientaddressnickname.ToList();
            ViewBag.ServiceType = servicetype.ToList();
            ViewBag.City = city.ToList();
            ViewBag.ShipmentPayer = shipmentpayer.ToList();
            ViewBag.DutiesTaxesPayer = dutiestaxespayer.ToList();
            ViewBag.NotifySender = notifysender.ToList();
            ViewBag.NotifyRecipient = notifyrecipient.ToList();





            //View Model set up
            shipmentcreate.ReferenceNumber = shipment.ReferenceNumber;
            shipmentcreate.OriginCity = shipment.Origin;
            shipmentcreate.RecipientName = shipment.RecipientName;
            shipmentcreate.CompanyName = shipment.CompanyName;
            shipmentcreate.DepartmentName = shipment.DepartmentName;

            shipmentcreate.UseSavedAddress = "No";
            shipmentcreate.RecipientAddressNickName = "Please Select: ";
            string rbuilding = shipment.Building;
            string rcity = shipment.City;
            string rstreet = shipment.Street;
            string rprovince = shipment.Province;
            string rpostal = shipment.PostalCode;
            var query = from s in db.RecipientAddresses
                        where s.Building == rbuilding && s.Building == rcity && s.Street == rstreet && s.Province == rprovince && s.PostalCode == rpostal
                        select s.NickName;
            string nick = query.SingleOrDefault();
            if (nick != null)
            {
                shipmentcreate.RecipientAddressNickName = nick;
                shipmentcreate.UseSavedAddress = "Yes";
            }
            

            shipmentcreate.Building = shipment.Building;
            shipmentcreate.Street = shipment.Street;
            shipmentcreate.City = shipment.City;
            shipmentcreate.PostalCode = shipment.PostalCode;
            shipmentcreate.PhoneNumber = shipment.PhoneNumber;
            shipmentcreate.EmailAddress = shipment.EmailAddress;
            shipmentcreate.PackageNumber = shipment.NumberOfPackages;
            shipmentcreate.ServiceType = shipment.ServiceType;
            if (Convert.ToInt16(shipment.ShipmentPayerID) == shipment.ShippingAccountId)
            {
                shipmentcreate.ShipmentPayer = "Sender";
            }
            else
            {
                shipmentcreate.ShipmentPayer = "Recipient";
                shipmentcreate.RecipientAccountNumber = shipment.ShipmentPayerID.ToString("000000000000");
            }
            if (Convert.ToInt16(shipment.DutiesTaxesPayerID) == shipment.ShippingAccountId)
            {
                shipmentcreate.DutiesTaxesPayer = "Sender";
            }
            else
            {
                shipmentcreate.DutiesTaxesPayer = "Recipient";
                shipmentcreate.RecipientAccountNumber = shipment.DutiesTaxesPayerID.ToString("000000000000");
            }
            if (shipment.NotifySender == true)
            {
                shipmentcreate.NotifySender = "Yes";
            }
            else
            {
                shipmentcreate.NotifySender = "No";
            }
            if (shipment.NotifyRecipient == true)
            {
                shipmentcreate.NotifyRecipient = "Yes";
            }
            else
            {
                shipmentcreate.NotifyRecipient = "No";
            }

            shipmentcreate.TypeSizes = new List<string>();
            shipmentcreate.ContentDescriptions = new List<string>();
            shipmentcreate.CustomerWeights = new List<string>();
            shipmentcreate.Values = new List<string>();
            shipmentcreate.Currencies = new List<string>();

            int wid = shipment.WaybillId;
            var packages = db.Packages.Where(s => s.WaybillId == wid);

            foreach (var package in packages)
            {
                shipmentcreate.TypeSizes.Add(package.TypeSize);
                shipmentcreate.ContentDescriptions.Add(package.ContentDescription);
                shipmentcreate.CustomerWeights.Add(Convert.ToString(package.CustomerWeight));
                shipmentcreate.Values.Add(Convert.ToString(package.Value));
                shipmentcreate.Currencies.Add(package.Currency);

            } 

            return View(shipmentcreate);
        }

        // POST: Shipments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult Edit([Bind(Include = "ReferenceNumber, OriginCity, RecipientName, CompanyName, DepartmentName, UseSavedAddress, RecipientAddressNickName, Building, Street, City, PostalCode, PhoneNumber, EmailAddress, PackageNumber, TypeSizes, TypeSizeList, ContentDescriptions, CustomerWeights, Values, Currencies, CurrencyList, ServiceType, ShipmentPayer, DutiesTaxesPayer, RecipientAccountNumber, NotifySender, NotifyRecipient")] CreateShipmentViewModel shipmentcreate, string submit)
        {
            int? id = Convert.ToInt16(Session["Waybill"]);
            Shipment shipment = db.Shipments.Find(id);

            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);

            ViewBag.listnumber = shipmentcreate.PackageNumber;

            var typesize = new SelectList(db.PackageTypeSizes.OrderBy(c => c.PackageTypeID).Select(c => c.Size).Distinct());
            var currency = new SelectList(db.Currencies.OrderBy(c => c.CurrencyCode).Select(c => c.CurrencyCode).Distinct());
            shipmentcreate.TypeSizeList = typesize.ToList();
            shipmentcreate.CurrencyList = currency.ToList();

            var origincity = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var usesavedaddress = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var recipientaddressnickname = new SelectList(db.RecipientAddresses.Where(s => s.ShippingAccountId == account.ShippingAccountId).Select(s => s.NickName).Distinct());
            var servicetype = new SelectList(db.ServiceTypes.OrderBy(c => c.ServiceTypeID).Select(c => c.Type).Distinct());
            var city = new SelectList(db.Destinations.OrderBy(c => c.City).Select(c => c.City).Distinct());
            var shipmentpayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var dutiestaxespayer = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Sender", Value = "Sender"},
                new SelectListItem { Text = "Recipient", Value = "Recipient"},
            }, "Value", "Text");
            var notifysender = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");
            var notifyrecipient = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes", Value = "Yes"},
                new SelectListItem { Text = "No", Value = "No"},
            }, "Value", "Text");

            ViewBag.AccountNo = account.ShippingAccountId.ToString("000000000000");
            
            ShippingAccount person = db.ShippingAccounts.Find(account.ShippingAccountId);
            if (person is PersonalShippingAccount)
            {
                PersonalShippingAccount pperson = (PersonalShippingAccount) db.ShippingAccounts.Find(account.ShippingAccountId);
                ViewBag.FullName = pperson.FirstName + " " + pperson.LastName;
            }   
            else if (person is BusinessShippingAccount)
            {
                BusinessShippingAccount bus = (BusinessShippingAccount)db.ShippingAccounts.Find(account.ShippingAccountId);
                ViewBag.FullName = bus.ContactPersonName + " " + bus.CompanyName;
            }
            ViewBag.MailingAddress = account.AddressBuilding + ", " + account.AddressCity + ", " + account.AddressProvinceCode;
            ViewBag.PhoneNumber = account.PhoneNumber;
            ViewBag.EmailAddress = account.Email;
            ViewBag.OriginCity = origincity.ToList();

            ViewBag.packagenumber = shipment.NumberOfPackages;


            ViewBag.UseSavedAddress = usesavedaddress.ToList();
            ViewBag.RecipientAddressNickName = recipientaddressnickname.ToList();
            ViewBag.ServiceType = servicetype.ToList();
            ViewBag.City = city.ToList();
            ViewBag.ShipmentPayer = shipmentpayer.ToList();
            ViewBag.DutiesTaxesPayer = dutiestaxespayer.ToList();
            ViewBag.NotifySender = notifysender.ToList();
            ViewBag.NotifyRecipient = notifyrecipient.ToList();


            if (submit == "Add a package")
            {
                ViewBag.packagenumber = (int)shipmentcreate.PackageNumber + 1;                
                shipmentcreate.ContentDescriptions.Add("");
                shipmentcreate.CustomerWeights.Add("");
                shipmentcreate.Values.Add("");
                shipmentcreate.Currencies.Add("");
            }
            if (submit == "Delete a package")
            {
                ViewBag.packagenumber = (int)shipmentcreate.PackageNumber - 1;
            }
            if (submit == "Save")
            {

                string message;
                
                if (shipmentcreate.UseSavedAddress == "Yes")
                {
                    if (String.IsNullOrEmpty(shipmentcreate.RecipientAddressNickName))
                    {
                        message = "Please select a saved recipient address";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    
                }
                else
                {
                    if (String.IsNullOrEmpty(shipmentcreate.Street) || String.IsNullOrEmpty(shipmentcreate.City))
                    {
                        message = "Please input or select Street and City";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    
                    if (!String.IsNullOrEmpty(shipmentcreate.PostalCode))
                    {
                        var pc = db.Destinations.SingleOrDefault(c => c.City == shipmentcreate.City).ProvinceCode;
                        if (ProvincePostalMatchFunc(pc, shipmentcreate.PostalCode) == 0)
                        {
                            ViewBag.message = "Please match the city and postal code.";
                            return View(shipmentcreate);
                        }
                    }
                }

                // Payer
                if (shipmentcreate.ShipmentPayer == "Recipient" || shipmentcreate.DutiesTaxesPayer == "Recipient")
                {
                    int accountNum = Convert.ToInt16(shipmentcreate.RecipientAccountNumber);
                    if (String.IsNullOrEmpty(shipmentcreate.RecipientAccountNumber))
                    {
                        message = "Pleasse input the Account Number of recipient";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    
                }


                // Package
                for (int i = 0; i < (int)shipmentcreate.PackageNumber; i++)
                {
                    string PackagesValidationResult = PackagesValidation(shipmentcreate.TypeSizes[i], shipmentcreate.ContentDescriptions[i], shipmentcreate.CustomerWeights[i], shipmentcreate.Values[i], shipmentcreate.Currencies[i]);
                    if (PackagesValidationResult != "correct")
                    {
                        ViewBag.message = PackagesValidationResult;
                        return View(shipmentcreate);
                    }
                }
                /*for (int i = 0; i < (int)shipmentcreate.PackageNumber; i++)
                {
                    string temptypesize = shipmentcreate.TypeSizes[i], tempcontent = shipmentcreate.ContentDescriptions[i], tempweight = shipmentcreate.CustomerWeights[i], tempvalue = shipmentcreate.Values[i], tempcurrency = shipmentcreate.Currencies[i];
                    
                    if (String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcontent) || String.IsNullOrEmpty(tempvalue) || String.IsNullOrEmpty(temptypesize) || String.IsNullOrEmpty(tempcurrency))
                    {
                        message = "Pleasse input or select all the package information";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (temptypesize != "Envelope - 250x350mm" && String.IsNullOrEmpty(tempweight))
                    {
                        message = "Pleasse input or select all the package information";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Regex.IsMatch(tempweight, @"^\d+(?:\.\d)?$") == false)
                    {
                        message = "Pleasse input a valid weight(a positive number to one decimal place)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Regex.IsMatch(tempvalue, @"^[0-9]*$") == false)
                    {
                        message = "Pleasse input a valid value(a positive integer)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (temptypesize != "Envelope - 250x350mm" && Convert.ToDouble(tempweight) <= 0)
                    {
                        message = "Pleasse input a valid weight(a positive number to one decimal place)";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 100)
                    {
                        message = "For safety reasons, please don't add a package weights larger than 100kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 20 && temptypesize == "Pak - 450x550mm")
                    {
                        message = "For safety reasons, please don't add a Pak weights larger than 20kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 20 && temptypesize == "Pak - 350x400mm")
                    {
                        message = "For safety reasons, please don't add a Pak weights larger than 20kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                    if (Convert.ToDouble(tempweight) >= 1 && temptypesize == "Envelope - 250x350mm")
                    {
                        message = "For safety reasons, please don't add a Envelop weights larger than 1kg.";
                        ViewBag.message = message;
                        return View(shipmentcreate);
                    }
                }*/

                




                shipment.Building = shipmentcreate.Building;
                shipment.City = shipmentcreate.City;
                shipment.Province = db.Destinations.SingleOrDefault(c => c.City == shipmentcreate.City).ProvinceCode;
                shipment.CompanyName = shipmentcreate.CompanyName;
                shipment.DepartmentName = shipmentcreate.DepartmentName;
                shipment.EmailAddress = shipmentcreate.EmailAddress;
                shipment.ReferenceNumber = shipmentcreate.ReferenceNumber;
                shipment.Origin = shipmentcreate.OriginCity;
                shipment.PostalCode = shipmentcreate.PostalCode;
                shipment.Street = shipmentcreate.Street;
                shipment.ServiceType = shipmentcreate.ServiceType;
                shipment.PhoneNumber = shipmentcreate.PhoneNumber;
                shipment.NumberOfPackages = Convert.ToInt16(shipmentcreate.PackageNumber);
                shipment.RecipientName = shipmentcreate.RecipientName;

                string City = shipmentcreate.City;
                string OriginCity = db.ShippingAccounts.SingleOrDefault(s => s.ShippingAccountId == person.ShippingAccountId).AddressCity;
                shipment.ShipmentCurrency = db.Destinations.Where(s => s.City == City).Select(s => s.CurrencyCode).Single();
                shipment.DutiesTaxesCurrency = db.Destinations.Where(s => s.City == OriginCity).Select(s => s.CurrencyCode).Single();

                shipment.ConfirmStatus = false;
                shipment.DeleteStatus = false;
                shipment.PickupStatus = false;

                shipment.ShippedDate = new DateTime(2000, 01, 01);
                shipment.DeliveredDate = new DateTime(2000, 01, 01);
                shipment.PickupTime = new DateTime(2000, 01, 01, 00, 00, 01);

                if (shipmentcreate.NotifySender == "Yes")
                {
                    shipment.NotifySender = true;
                }
                else
                {
                    shipment.NotifySender = false;
                }
                if (shipmentcreate.NotifyRecipient == "Yes")
                {
                    shipment.NotifyRecipient = true;
                }
                else
                {
                    shipment.NotifyRecipient = false;
                }

                
                int ShipmentPayerID = 0, DutiesTaxesPayerID = 0;
                if (shipmentcreate.ShipmentPayer == "Recipient")
                {
                    ShipmentPayerID = Convert.ToInt32(Regex.Match(shipmentcreate.RecipientAccountNumber, @"^[1-9][0-9]*").Value);
                }
                else
                {
                    ShipmentPayerID = account.ShippingAccountId;
                }
                if (shipmentcreate.DutiesTaxesPayer == "Recipient")
                {
                    DutiesTaxesPayerID = Convert.ToInt32(Regex.Match(shipmentcreate.RecipientAccountNumber, @"^[1-9][0-9]*").Value);
                }
                else
                {
                    DutiesTaxesPayerID = account.ShippingAccountId;
                }

                shipment.ShipmentPayerID = ShipmentPayerID;
                shipment.DutiesTaxesPayerID = DutiesTaxesPayerID;

                int waybill = shipment.WaybillId;
                var query = db.Packages.Where(s => s.WaybillId == waybill).Select(s => s.PackageID);

                foreach (int i in query)
                {
                    Package p = db.Packages.Find(i);
                    if (p != null)
                    {
                        db.Packages.Remove(p);
                    }

                }


                //Package
                for (int i = 0; i < shipmentcreate.PackageNumber; i++)
                {
                    Package newpackage = new Package();
                    newpackage.TypeSize = shipmentcreate.TypeSizes[i];
                    newpackage.ContentDescription = shipmentcreate.ContentDescriptions[i];
                    if (shipmentcreate.TypeSizes[i] == "Envelope - 250x350mm" && String.IsNullOrEmpty(shipmentcreate.CustomerWeights[i]))
                    {
                        newpackage.CustomerWeight = 0;
                    }
                    else
                    {
                        newpackage.CustomerWeight = Convert.ToDouble(shipmentcreate.CustomerWeights[i]);
                    }

                    if( String.IsNullOrEmpty(shipmentcreate.CustomerWeights[i]) )
                    {
                        shipmentcreate.CustomerWeights[i] = "0";
                    }

                    newpackage.CustomerWeight = Convert.ToDouble(shipmentcreate.CustomerWeights[i]);
                    newpackage.Value = Convert.ToDouble(shipmentcreate.Values[i]);
                    newpackage.Currency = shipmentcreate.Currencies[i];
                    newpackage.WaybillId = shipment.WaybillId;


                    if (ModelState.IsValid)
                    {
                        db.Packages.Add(newpackage);
                        db.SaveChanges();
                    }
                }



                if (ModelState.IsValid)
                {
                    db.Entry(shipment).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            
            return View(shipmentcreate);
            
        }



        // GET: Shipments/Delete/5
        [Authorize(Roles = "Customer")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shipment shipment = db.Shipments.Find(id);
            if (shipment == null)
            {
                return HttpNotFound();
            }
            bool temp = shipment.NotifyRecipient;
            if (temp == true)
            {
                ViewBag.NotifyRecipient = true;
            }
            else
            {
                ViewBag.NotifyRecipient = false;
            }

            temp = shipment.NotifySender;
            if (temp == true)
            {
                ViewBag.NotifySender = true;
            }
            else
            {
                ViewBag.NotifySender = false;
            }

            ViewBag.ReferenceNumber = shipment.ReferenceNumber;

            int sender = shipment.ShippingAccountId;
            int sp = Convert.ToInt32(shipment.ShipmentPayerID);
            int dp = Convert.ToInt32(shipment.DutiesTaxesPayerID);

            if (sender == sp)
            {
                ViewBag.shipmentpayer = "Sender";
            }
            else
            {
                ViewBag.shipmentpayer = "Recipient";
            }

            if (sender == dp)
            {
                ViewBag.dutypayer = "Sender";
            }
            else
            {
                ViewBag.dutypayer = "Recipient";
            }

            //Package Estimated Cost
            int num = 0;
            string services = shipment.ServiceType;
            string currencyc = shipment.ShipmentCurrency;
            double totalcost = 0;
            double[] packagecost = new double[20];


            foreach (Package package in shipment.Packages)
            {
                decimal penaltyfee = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
                decimal cost = 0;

                string types = package.TypeSize;
                double weight = package.CustomerWeight;

                var PID = db.PackageTypeSizes.Where(s => s.Size == types).Select(s => s.PackageTypeID).Distinct();
                int packagetypeid = PID.Max();

                var SID = db.ServiceTypes.Where(s => s.Type == services).Select(s => s.ServiceTypeID).Distinct();
                int servicetypeid = SID.Max();
                decimal Fee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.Fee).Max());
                decimal MinimunFee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.MinimumFee).Max());

                var ER = db.Currencies.Where(s => s.CurrencyCode == currencyc).Select(s => s.ExchangeRate).Distinct();
                double exchangerate = ER.Max();
                decimal rate = Convert.ToDecimal(exchangerate);

                decimal we = 0;
                if (packagetypeid == 1)
                {
                }
                else
                {
                    we = Convert.ToDecimal(weight);
                }

                if (types == "Envelope - 250x350mm")
                {
                    cost = Fee * rate;
                }
                else
                {
                    decimal weightlimit = 0;
                    decimal te = 0;

                    if (types == "Pak - small - 350x400mm" || types == "Pak - large - 450x550mm")
                    {
                        weightlimit = 5;
                        te = Fee * we;
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                        }
                    }
                    else if (types == "Box - small - 300x250x150mm")
                    {
                        weightlimit = 10;
                        te = Fee * we;
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                        }
                    }
                    else if (types == "Box - medium - 400x350x250mm")
                    {
                        weightlimit = 20;
                        te = Fee * we;
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                        }
                    }
                    else if (types == "Box - large - 500x450x350mm")
                    {
                        weightlimit = 30;
                        te = Fee * we;
                        if (we > weightlimit)
                        {
                            te = Fee * we + penaltyfee;
                        }
                    }
                    else
                    {
                        te = Fee * we;
                    }

                    if (te < MinimunFee)
                    {
                        te = MinimunFee;
                    }

                    cost = te * rate;

                }
                packagecost[num] = Convert.ToDouble(cost);

                num++;
                totalcost = totalcost + Convert.ToDouble(cost);
            }


            ViewBag.currency = shipment.ShipmentCurrency;
            ViewBag.costmsg = "Estimated Cost: ";
            ViewBag.shipmentcost = totalcost;
            ViewBag.packagecost = packagecost;

            ViewBag.username = System.Web.HttpContext.Current.User.Identity.Name;

            return View(shipment);
        }

        // POST: Shipments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Customer")]
        public ActionResult DeleteConfirmed(int id)
        {
            Shipment shipment = db.Shipments.Find(id);
            shipment.DeleteStatus = true;
            //db.Shipments.Remove(shipment);

            if (ModelState.IsValid)
            {
                db.Entry(shipment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shipment);
        }

        // GET: Shipments/EmployeeEdit
        // [Authorize(Roles = "Employee")]
        public ActionResult EmployeeEdit(string waybillId, double? dutiesAmount, double? taxesAmount, List<double?> actualWeight)
        {
            if(waybillId == null || waybillId == "0000000000000000")
            {
                EmployeeEditViewModel empty = new EmployeeEditViewModel();
                ViewBag.firstVisit = true;
                empty.waybillId = "0000000000000000";
                ViewBag.Tracking = "The shipment does not exist, Please input valid waybill ID";

                return View(empty);
            }

            int id = Convert.ToInt32(waybillId);
            Shipment shipment = db.Shipments.Find(id);

            if (shipment == null)
            {
                EmployeeEditViewModel empty = new EmployeeEditViewModel();
                ViewBag.firstVisit = true;
                empty.waybillId = "0000000000000000";
                ViewBag.Tracking = "The shipment does not exist, Please input valid waybill ID";

                return View(empty);
            }


            if (shipment.ShipemtnPaymentAuthorizationCode != null && shipment.DutiesTaxesPaymentAuthorizationCode != null)
            {
                EmployeeEditViewModel empty = new EmployeeEditViewModel();
                ViewBag.firstVisit = true;
                empty.waybillId = waybillId;
                ViewBag.Tracking = "The shipment has been invoiced already. Don't ask customers pay twice";

                return View(empty); // Return the search page
            }

            int facilitycount = 0;

            var shipmentTrack = db.ShipmentTrackings.Where(c => c.WaybillId == id);
            string shipmentLastActivity = "";
            foreach (var item in shipmentTrack)
            {
                if (item.Activity == "At local sort facility")
                {
                    facilitycount += 1;
                }
                shipmentLastActivity = item.Activity;
            }
            if (facilitycount == 1 && shipmentLastActivity == "At local sort facility")
            {
                if (dutiesAmount == null && taxesAmount == null && actualWeight == null)
                {
                    ViewBag.firstVisit = false;

                    int senderId = shipment.ShippingAccountId;
                    ShippingAccount sender = db.ShippingAccounts.Where(s => s.ShippingAccountId == senderId).Single();
                    ViewBag.userName = sender.UserName;
                    ViewBag.numOfPackages = shipment.NumberOfPackages;

                    EmployeeEditViewModel employEdit = new EmployeeEditViewModel();
                    employEdit.waybillId = id.ToString("0000000000000000");
                    ViewBag.DutyTaxCurrency = shipment.DutiesTaxesCurrency;
                    ViewBag.ShipmentCurrency = shipment.ShipmentCurrency;
                    employEdit.ServiceType = shipment.ServiceType;
                    employEdit.RecipientName = shipment.RecipientName;
                    employEdit.Building = shipment.Building;
                    employEdit.Street = shipment.Street;
                    employEdit.City = shipment.City;
                    employEdit.Province = shipment.Province;
                    employEdit.PostalCode = shipment.PostalCode;
                    employEdit.PhoneNumber = shipment.PhoneNumber;
                    employEdit.EmailAddress = shipment.EmailAddress;
                    employEdit.Packages = db.Packages.Where(p => p.Shipment.WaybillId == id).ToList();
                    employEdit.TaxesAmount = shipment.TaxesAmount;
                    employEdit.DutiesAmount = shipment.DutiesAmount;
                    employEdit.ActualWeight = new List<double?>();
                    for (int i = 0; i < shipment.NumberOfPackages; i++)
                    {
                        employEdit.ActualWeight.Add(employEdit.Packages[i].ActualWeight);
                    }

                    return View(employEdit);
                }

                if (dutiesAmount != null)
                {
                    shipment.DutiesAmount = dutiesAmount;
                }

                if (taxesAmount != null)
                {
                    shipment.TaxesAmount = taxesAmount;
                }

                if (actualWeight != null)
                {
                    string servicetype = shipment.ServiceType;
                    string shipmentcurrency = shipment.ShipmentCurrency;
                    double shipmentcost = 0;

                    List<Package> packages = db.Packages.Where(p => p.WaybillId == id).ToList();

                    for (int i = 0; i < shipment.NumberOfPackages; i++)
                    {
                        packages[i].ActualWeight = actualWeight[i];

                        //******** Recalculate the cost ************//
                        decimal penalty = db.penaltyfees.Where(s => s.penaltyfeeID == 1).Select(s => s.fee).Single();
                        decimal packagecost = 0;

                        string packagetype = packages[i].TypeSize;
                        double weight = (double)actualWeight[i];

                        var PID = db.PackageTypeSizes.Where(s => s.Size == packagetype).Select(s => s.PackageTypeID).Distinct();
                        int packagetypeid = PID.Max();

                        var SID = db.ServiceTypes.Where(s => s.Type == servicetype).Select(s => s.ServiceTypeID).Distinct();
                        int servicetypeid = SID.Max();
                        decimal Fee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.Fee).Max());
                        decimal MinimunFee = Convert.ToDecimal(db.ServicePackageFees.Where(s => s.PackageTypeID == packagetypeid).Where(s => s.ServiceTypeID == servicetypeid).Select(s => s.MinimumFee).Max());

                        var ER = db.Currencies.Where(s => s.CurrencyCode == shipmentcurrency).Select(s => s.ExchangeRate).Distinct();
                        double exchangerate = ER.Max();
                        decimal rate = Convert.ToDecimal(exchangerate);

                        decimal we = 0; // weight to be stored in package

                        if (!(packagetypeid == 1))
                        {
                            we = Convert.ToDecimal(weight);
                        }

                        if (packagetype == "Envelope - 250x350mm")
                        {
                            packagecost = Fee * rate;
                        }
                        else
                        {
                            decimal weightlimit = 0;
                            decimal te = 0;

                            if (packagetype == "Pak - small - 350x400mm" || packagetype == "Pak - large - 450x550mm")
                            {
                                weightlimit = 5;
                                te = Fee * we;
                                if (we > weightlimit)
                                {
                                    te += penalty;
                                }
                            }
                            else if (packagetype == "Box - small - 300x250x150mm")
                            {
                                weightlimit = 10;
                                te = Fee * we;
                                if (we > weightlimit)
                                {
                                    te += penalty;
                                }
                            }
                            else if (packagetype == "Box - medium - 400x350x250mm")
                            {
                                weightlimit = 20;
                                te = Fee * we;
                                if (we > weightlimit)
                                {
                                    te += penalty;
                                }
                            }
                            else if (packagetype == "Box - large - 500x450x350mm")
                            {
                                weightlimit = 30;
                                te = Fee * we;
                                if (we > weightlimit)
                                {
                                    te += penalty;
                                }
                            }
                            else
                            {
                                te = Fee * we;
                            }

                            if (te < MinimunFee)
                            {
                                te = MinimunFee;
                            }

                            packagecost = te * rate;

                        }

                        packages[i].Cost = Convert.ToDouble(packagecost);

                        shipmentcost += Convert.ToDouble(packagecost);
                    }
                    if (shipment.DutiesTaxesCurrency == shipment.ShipmentCurrency)
                    {
                        shipment.ShipmentAmount = shipmentcost;
                        shipment.TotalInvoiceAmount = shipmentcost + shipment.DutiesAmount + shipment.TaxesAmount;
                    }
                    else
                    {
                        shipment.ShipmentAmount = shipmentcost;
                        shipment.TotalInvoiceAmount = shipmentcost;
                    }

                    if (shipment.ShipmentPayerID != shipment.DutiesTaxesPayerID) // pay separately
                    {
                        // Send Invoice Notification to shipment payer
                        string ShipmentCreditCardType = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardType;
                        string ShipmentCreditCardNumber = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardNumber;
                        string ShipmentCreditCardSecurityNum = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardSecurityNumber;
                        string ShipmentCreditCardHolderName = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardHolderName;
                        string shipmentAuthorizationCode = GenerateAuthorizationCode(ShipmentCreditCardType, ShipmentCreditCardNumber, ShipmentCreditCardSecurityNum, ShipmentCreditCardHolderName);
                        shipment.ShipemtnPaymentAuthorizationCode = shipmentAuthorizationCode;
                        var shipmentPayerEmail = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShipmentPayerID).Email;
                        SendShipmentPayerEmail(shipmentPayerEmail, shipment.WaybillId, shipment.ShipmentPayerID, shipment.DutiesTaxesPayerID, shipment, packages, ShipmentCreditCardType, ShipmentCreditCardNumber, shipmentAuthorizationCode);

                        string DutyTaxCreditCardType = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.DutiesTaxesPayerID).CardType;
                        string DutyTaxCreditCardNumber = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.DutiesTaxesPayerID).CardNumber;
                        string DutyTaxCreditCardSecurityNum = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.DutiesTaxesPayerID).CardSecurityNumber;
                        string DutyTaxCreditCardHolderName = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.DutiesTaxesPayerID).CardHolderName;
                        string dutyTaxAuthorizationCode = GenerateAuthorizationCode(ShipmentCreditCardType, ShipmentCreditCardNumber, ShipmentCreditCardSecurityNum, ShipmentCreditCardHolderName);
                        shipment.DutiesTaxesPaymentAuthorizationCode = dutyTaxAuthorizationCode;
                        // Send Invoice Notification to duties and taxes payer
                        var dutyandtaxPayerEmail = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.DutiesTaxesPayerID).Email;
                        SendDutyandTaxPayerEmail(dutyandtaxPayerEmail, shipment.WaybillId, shipment.ShipmentPayerID, shipment.DutiesTaxesPayerID, shipment, packages, DutyTaxCreditCardType, DutyTaxCreditCardNumber, dutyTaxAuthorizationCode);
                    }
                    else
                    {
                        string ShipmentCreditCardType = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardType;
                        string ShipmentCreditCardNumber = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardNumber;
                        string ShipmentCreditCardSecurityNum = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardSecurityNumber;
                        string ShipmentCreditCardHolderName = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShipmentPayerID).CardHolderName;
                        string shipmentAuthorizationCode = GenerateAuthorizationCode(ShipmentCreditCardType, ShipmentCreditCardNumber, ShipmentCreditCardSecurityNum, ShipmentCreditCardHolderName);
                        shipment.ShipemtnPaymentAuthorizationCode = shipmentAuthorizationCode;
                        shipment.DutiesTaxesPaymentAuthorizationCode = shipmentAuthorizationCode;
                        var shipmentPayerEmail = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == shipment.ShipmentPayerID).Email;
                        SendPayerEmail(shipmentPayerEmail, shipment.WaybillId, shipment.ShipmentPayerID, shipment.DutiesTaxesPayerID, shipment, packages, ShipmentCreditCardType, ShipmentCreditCardNumber, shipmentAuthorizationCode);
                    }
                }

            } else
            {
                EmployeeEditViewModel empty = new EmployeeEditViewModel();
                ViewBag.firstVisit = true;
                empty.waybillId = waybillId;
                ViewBag.Tracking = "The shipment has been already weighted or hasn't arrived at the local sort facility";

                return View(empty); // Return the search page
            }

            db.Entry(shipment).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        Random _rdm = new Random();
        public string GenerateAuthorizationCode(string CreditType, string CreditCardNumber, string CreditCardSecurityNumber, string CreditCardHolderName)
        {
            string AuthorizationCode = _rdm.Next(0, 9999).ToString("0000");
            return AuthorizationCode;
        }

        public void SendPayerEmail(string email, int waybillId, int shipmentpayerID, int dutytaxpayerID, Shipment shipment, List<Package> packages, string ShipmentCreditCardType, string ShipmentCreditCardNumber, string shipmentAuthorizationCode)
        {
            // Create an instance of MailMessage named mail.
            MailDefinition md = new MailDefinition();
            md.From = "comp3111_team106@cse.ust.hk";
            md.IsBodyHtml = true;
            md.Subject = "Invoice of your Shipment is Ready!";

            ListDictionary replacements = new ListDictionary();
            string body = @"<h4>Invoice Details</h4><br /><div><hr/><h2><strong><u>Invoice Information</u></strong></h2><br/><br /><hr/><h4><strong><u>Invoice Payer Information</u></strong></h4><br /><dt>Shipment WaibillID: </dt><dd>";
            string waybillID = waybillId.ToString("0000000000000000");
            body += waybillID;
            body += "<br /></dd><dt>Shipment Payer ID: </dt><dd>";
            string shipmentPayerID = shipmentpayerID.ToString("000000000000");
            body += shipmentPayerID;
            body += "<br /></dd><dt>Duty & Tax Payer ID: </dt><dd>";
            string dutytaxPayerID = dutytaxpayerID.ToString("000000000000");
            body += dutytaxPayerID;
            body += "<br /></dd><br /><hr /><h4><strong><u>Shipment Information</u></strong></h4>";
            body += "<br /><dt>Shipment WaybillID: </dt><dd>";
            body += waybillID;
            body += "<br /></dd><dt>Ship (Pickup) Date: </dt><dd>";
            string shippeddate = shipment.ShippedDate.ToString();
            body += shippeddate;
            body += "<br /></dd><dt>Service Type: </dt><dd>";
            string sertiveType = shipment.ServiceType;
            body += sertiveType;
            body += "<br /></dd><dt>Reference Number: </dt><dd>";
            string referencenum = shipment.ReferenceNumber;
            body += referencenum;
            body += "<br /></dd><br /><hr /><h4><strong><u>Sender's Information</u></strong></h4><br /><dt>Sender FullName: </dt><dd>";
            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
            if (person is BusinessShippingAccount)
            {
                BusinessShippingAccount bperson = (BusinessShippingAccount) db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += bperson.ContactPersonName;
            } else if (person is PersonalShippingAccount)
            {
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += pperson.FirstName + " " + pperson.LastName;
            }
            body += "<br /></dd><dt>Sender's Mailing Address: </dt><dd>";
            body += person.AddressBuilding + " " + person.AddressStreet + " " + person.AddressCity + " " + person.AddressProvinceCode;
            body += "<br /></dd><br /><hr /><h4><strong><u>Recipient's Information</u></strong></h4><br /><dt>Recipient Name: </dt><dd>";
            body += shipment.RecipientName;
            body += "<br /></dd><dt>Recipient's Delivery Address: </dt><dd>";
            body += shipment.Building + " " + shipment.Street + " " + shipment.City + " " + shipment.Province;
            body += "<br /></dd><br /><hr /><h4><strong><u>Package Information</u></strong></h4><div>";
            foreach (var item in packages)
            {
                body += "<br />Package Type: <td>" + item.TypeSize + "</td>";
                body += "<br />Actual Weight: <td>" + item.ActualWeight.ToString() + "</td>";
                body += "<br />Cost: <td>" + shipment.ShipmentCurrency + " " + item.Cost.ToString() + "</td>";
            }
            body += "<br /></div><br /><hr /><h4><strong><u>Payment Information</u></strong></h4><br /><dt>Credit Card Type: </dt><dd>";
            body += ShipmentCreditCardType;
            body += "<br /></dd><dt>Credit Card Number: </dt><dd>";
            body += ShipmentCreditCardNumber.Substring(ShipmentCreditCardNumber.Length - 4, 4);
            body += "<br /></dd><dt>Authorization Code: </dt><dd>";
            body += shipmentAuthorizationCode;
            body += "<br /><br /></dd><dt></dt><dd>";
            body += "Payable Amount: " + shipment.ShipmentCurrency + " " + shipment.TotalInvoiceAmount.ToString();
            body += "<br /></dd></div>";

            MailMessage mail = md.CreateMailMessage(email, replacements, body, new System.Web.UI.Control());
            mail.IsBodyHtml = true;

            // Set the sender (From), receiver (To), subject and 
            // message body fields of the mail message.

            // Create an instance of SmtpClient named emailServer.
            // Set the mail server to use as "smtp.ust.hk".
            SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

            // Send the message.
            emailServer.Send(mail);
        }

        public void SendShipmentPayerEmail(string email, int waybillId, int shipmentpayerID, int dutytaxpayerID, Shipment shipment, List<Package> packages, string ShipmentCreditCardType, string ShipmentCreditCardNumber, string shipmentAuthorizationCode)
        {
            MailDefinition md = new MailDefinition();
            md.From = "comp3111_team106@cse.ust.hk";
            md.IsBodyHtml = true;
            md.Subject = "Invoice of your Shipment is Ready!";

            ListDictionary replacements = new ListDictionary();
            string body = @"<h4>Invoice Details</h4><div><br /><hr/><h2><strong><u>Invoice Information</u></strong></h2><br/><br /><hr/><h4><strong><u>Invoice Payer Information</u></strong></h4><br /><dt>Shipment WaibillID: </dt><dd>";
            string waybillID = waybillId.ToString("0000000000000000");
            body += waybillID;
            body += "<br /></dd><dt>Shipment Payer ID: </dt><dd>";
            string shipmentPayerID = shipmentpayerID.ToString("000000000000");
            body += shipmentPayerID;
            body += "<br /></dd><dt>Duty & Tax Payer ID: </dt><dd>";
            string dutytaxPayerID = dutytaxpayerID.ToString("000000000000");
            body += dutytaxPayerID;
            body += "<br /></dd><br /><hr /><h4><strong><u>Shipment Information</u></strong></h4>";
            body += "<br /><dt>Shipment WaybillID: </dt><dd>";
            body += waybillID;
            body += "<br /></dd><dt>Ship (Pickup) Date: </dt><dd>";
            string shippeddate = shipment.ShippedDate.ToString();
            body += shippeddate;
            body += "<br /></dd><dt>Service Type: </dt><dd>";
            string sertiveType = shipment.ServiceType;
            body += sertiveType;
            body += "<br /></dd><dt>Reference Number: </dt><dd>";
            string referencenum = shipment.ReferenceNumber;
            body += referencenum;
            body += "<br /></dd><br /><hr /><h4><strong><u>Sender's Information</u></strong></h4><br /><dt>Sender FullName: </dt><dd>";
            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
            if (person is BusinessShippingAccount)
            {
                BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += bperson.ContactPersonName;
            }
            else if (person is PersonalShippingAccount)
            {
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += pperson.FirstName + " " + pperson.LastName;
            }
            body += "<br /></dd><dt>Sender's Mailing Address: </dt><dd>";
            body += person.AddressBuilding + " " + person.AddressStreet + " " + person.AddressCity + " " + person.AddressProvinceCode;
            body += "<br /></dd><br /><hr /><h4><strong><u>Recipient's Information</u></strong></h4><br /><dt>Recipient Name: </dt><dd>";
            body += shipment.RecipientName;
            body += "<br /></dd><dt>Recipient's Delivery Address: </dt><dd>";
            body += shipment.Building + " " + shipment.Street + " " + shipment.City + " " + shipment.Province;
            body += "<br /></dd><br /><hr /><h4><strong><u>Package Information</u></strong></h4><div>";
            foreach (var item in packages)
            {
                body += "<br />Package Type: <td>" + item.TypeSize + "</td>";
                body += "<br />Actual Weight: <td>" + item.ActualWeight.ToString() + "</td>";
                body += "<br />Cost: <td>" + shipment.ShipmentCurrency + " " + item.Cost.ToString() + "</td>";
            }
            body += "<br /></div><br /><hr /><h4><strong><u>Payment Information</u></strong></h4><br /><dt>Credit Card Type: </dt><dd>";
            body += ShipmentCreditCardType;
            body += "<br /></dd><dt>Credit Card Number: </dt><dd>";
            body += ShipmentCreditCardNumber.Substring(ShipmentCreditCardNumber.Length - 4, 4);
            body += "<br /></dd><dt>Authorization Code: </dt><dd>";
            body += shipmentAuthorizationCode;
            body += "<br /></dd><dt></dt><dd>";
            body += "Payable Amount: " + shipment.ShipmentCurrency + " " + shipment.ShipmentAmount.ToString();
            body += "<br /></dd></div>";

            MailMessage mail = md.CreateMailMessage(email, replacements, body, new System.Web.UI.Control());

            // Set the sender (From), receiver (To), subject and 
            // message body fields of the mail message.

            // Create an instance of SmtpClient named emailServer.
            // Set the mail server to use as "smtp.ust.hk".
            SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

            // Send the message.
            emailServer.Send(mail);
        }
        public void SendDutyandTaxPayerEmail(string email, int waybillId, int shipmentpayerID, int dutytaxpayerID, Shipment shipment, List<Package> packages, string DutyTaxCreditCardType, string DutyTaxCreditCardNumber, string dutyTaxAuthorizationCode)
        {
            MailDefinition md = new MailDefinition();
            md.From = "comp3111_team106@cse.ust.hk";
            md.IsBodyHtml = true;
            md.Subject = "Invoice of your Shipment is Ready!";

            ListDictionary replacements = new ListDictionary();
            string body = @"<h4>Invoice Details</h4><br /><div><hr/><h2><strong><u>Invoice Information</u></strong></h2><br /><br/><hr/><h4><strong><u>Invoice Payer Information</u></strong></h4><br /><dt>Shipment WaibillID: </dt><dd>";
            string waybillID = waybillId.ToString("0000000000000000");
            body += waybillID;
            body += "<br /></dd><dt>Shipment Payer ID: </dt><dd>";
            string shipmentPayerID = shipmentpayerID.ToString("000000000000");
            body += shipmentPayerID;
            body += "<br /></dd><dt>Duty & Tax Payer ID: </dt><dd>";
            string dutytaxPayerID = dutytaxpayerID.ToString("000000000000");
            body += dutytaxPayerID;
            body += "<br /></dd><br /><hr /><h4><strong><u>Shipment Information</u></strong></h4>";
            body += "<br /><dt>Shipment WaybillID: </dt><dd>";
            body += waybillID;
            body += "<br /></dd><dt>Ship (Pickup) Date: </dt><dd>";
            string shippeddate = shipment.ShippedDate.ToString();
            body += shippeddate;
            body += "<br /></dd><dt>Service Type: </dt><dd>";
            string sertiveType = shipment.ServiceType;
            body += sertiveType;
            body += "<br /></dd><dt>Reference Number: </dt><dd>";
            string referencenum = shipment.ReferenceNumber;
            body += referencenum;
            body += "<br /></dd><br /><hr /><h4><strong><u>Sender's Information</u></strong></h4><br /><dt>Sender FullName: </dt><dd>";
            ShippingAccount person = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
            if (person is BusinessShippingAccount)
            {
                BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += bperson.ContactPersonName;
            }
            else if (person is PersonalShippingAccount)
            {
                PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == shipment.ShippingAccountId);
                body += pperson.FirstName + " " + pperson.LastName;
            }
            body += "<br /></dd><dt>Sender's Mailing Address: </dt><dd>";
            body += person.AddressBuilding + " " + person.AddressStreet + " " + person.AddressCity + " " + person.AddressProvinceCode;
            body += "<br /></dd><br /><hr /><h4><strong><u>Recipient's Information</u></strong></h4><dt>Recipient Name: </dt><dd>";
            body += shipment.RecipientName;
            body += "<br /></dd><dt>Recipient's Delivery Address: </dt><dd>";
            body += shipment.Building + " " + shipment.Street + " " + shipment.City + " " + shipment.Province;
            body += "<br /></dd><br /><hr /><h4><strong><u>Package Information</u></strong></h4><br /><div>";
            foreach (var item in packages)
            {
                body += "<br />Package Type: <td>" + item.TypeSize + "</td>";
                body += "<br />Actual Weight: <td>" + item.ActualWeight.ToString() + "</td>";
                body += "<br />Cost: <td>" + shipment.ShipmentCurrency + " " + item.Cost.ToString() + "</td>";
            }
            body += "<br /></div><br /><hr /><h4><strong><u>Payment Information</u></strong></h4><dt>Credit Card Type: </dt><dd>";
            body += DutyTaxCreditCardType;
            body += "<br /></dd><dt>Credit Card Number: </dt><dd>";
            body += DutyTaxCreditCardNumber.Substring(DutyTaxCreditCardNumber.Length - 4, 4);
            body += "<br /></dd><dt>Authorization Code: </dt><dd>";
            body += dutyTaxAuthorizationCode;
            body += "<br /></dd><dt></dt><dd>";
            double shipmentDutyTax = (double)shipment.DutiesAmount + (double)shipment.TaxesAmount;
            body += "Payable Amount: " + shipment.DutiesTaxesCurrency + " " + shipmentDutyTax.ToString();
            body += "<br /></dd></div>";

            MailMessage mail = md.CreateMailMessage(email, replacements, body, new System.Web.UI.Control());

            // Set the sender (From), receiver (To), subject and 
            // message body fields of the mail message.

            // Create an instance of SmtpClient named emailServer.
            // Set the mail server to use as "smtp.ust.hk".
            SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

            // Send the message.
            emailServer.Send(mail);
        }

        [Authorize(Roles = "Customer")]
        public ActionResult Confirm(int id, string pickupLocationNickname, string newPickupLocation, string pickupType, DateTime? pickupDate, TimeSpan? pickupTime)
        {
            var shipmentConfirm = new ShipmentConfirmationViewModel();
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            ShippingAccount account = db.ShippingAccounts.SingleOrDefault(s => s.UserName == username);

            var pickuptype = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "immediate", Value = "immediate"},
                new SelectListItem { Text = "prearranged", Value = "prearranged"},
            }, "Value", "Text");
            var pickuplocationnickname = new SelectList(db.PickupLocations.Where(s => s.ShippingAccountId == account.ShippingAccountId).Select(s => s.NickName).Distinct());

            ViewBag.PickUpType = pickuptype.ToList();
            ViewBag.PickUpLocationNickname = pickuplocationnickname.ToList();


            // Validation
            if (String.IsNullOrEmpty(pickupLocationNickname) && String.IsNullOrEmpty(newPickupLocation))
            {
                ViewBag.message = "Please Select or Input Pickup Location.";
                return View(shipmentConfirm);
            }

            if (pickupType == "")
            {
                ViewBag.message = "Please Select a Pickup Type.";
                return View(shipmentConfirm);
            }

            if (pickupType == "prearranged")
            {
                DateTime now = DateTime.Now;
                // pickupTime existence and correctness
                if (pickupDate == null)
                {
                    ViewBag.message = "Please Input Pickup Date.";
                    return View(shipmentConfirm);
                }
                if (pickupDate < now)
                {
                    ViewBag.message = "The Pickup Can Only be Prearranged In a Later Time.";
                    return View(shipmentConfirm);
                }
                if (pickupTime == null)
                {
                    ViewBag.message = "Please Input Correct Pickup Time as HH:mm.";
                    return View(shipmentConfirm);
                }
           
                DateTime date = (DateTime)pickupDate;
                TimeSpan time = (TimeSpan)pickupTime;
                DateTime inputTime = date.Date + time;

                DateTime validTime = now.AddDays(5);
                if (inputTime > validTime)
                {
                    ViewBag.message = "The Pickup Can Only be Prearranged Up To 5 Days.";
                    return View(shipmentConfirm);
                }

            }

            Shipment shipment = db.Shipments.Find(id);
            if (pickupType == "prearranged")
            {
                DateTime date = (DateTime)pickupDate;
                TimeSpan time = (TimeSpan)pickupTime;
                DateTime inputTime = date.Date + time;
                shipment.PickupTime = inputTime;
            }
            else if (pickupType == "immediate")
            {
                shipment.PickupTime = DateTime.Now;
            }

            if (newPickupLocation == "")
            {
                PickupLocation pickUpLocation = db.PickupLocations.Where(s => s.NickName == pickupLocationNickname).Single();
                shipment.PickupLocation = pickUpLocation.Location;
            }
            else
            {
                shipment.PickupLocation = newPickupLocation;
            }
            shipment.PickupType = pickupType;
            shipment.ConfirmStatus = true;

            db.Entry(shipment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Shipments/GenerateHistoryReport
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult GenerateHistoryReport(int? ShippingAccountId, DateTime? ShippedStartDate, DateTime? ShippedEndDate, 
            string sortOrder, int? page)
        {

            if (ShippedStartDate == null && ShippedEndDate == null)
            {
                ShippedStartDate = new DateTime(1900, 01, 01);
                ShippedEndDate = new DateTime(2100, 01, 01);
            }

           


            // Instantiate an instance of the ShipmentsReportViewModel and the ShipmentsSearchViewModel.
            var shipmentSearch = new ShipmentsReportViewModel();
            shipmentSearch.Shipment = new ShipmentsSearchViewModel();



            if (User.IsInRole("Employee"))
            {
                shipmentSearch.Shipment.ShippingAccounts = PopulateShippingAccountsDropdownList().ToList();
            }

            if (User.IsInRole("Customer"))
            {
                ShippingAccount TshippingAccount = db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name);
                ShippingAccountId = TshippingAccount.ShippingAccountId;
            }

            // Populate the ShippingAccountId dropdown list.
            shipmentSearch.Shipment.ShippedStartDates = PopulateShippingDatesDropdownList().ToList();
            shipmentSearch.Shipment.ShippedEndDates = PopulateShippingDatesDropdownList().ToList();

            


            // Initialize the query to retrieve shipments using the ShipmentsListViewModel.
            var shipmentQuery = from s in db.Shipments
                                select new ShipmentsListViewModel
                                {
                                    WaybillId = s.WaybillId,
                                    ServiceType = s.ServiceType,
                                    ShippedDate = s.ShippedDate,
                                    DeliveredDate = s.DeliveredDate,
                                    RecipientName = s.RecipientName,
                                    NumberOfPackages = s.NumberOfPackages,
                                    Origin = s.Origin,
                                    Destination = s.Destination,
                                    ShippingAccountId = s.ShippingAccountId
                                };
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ShippingAccountId = ShippingAccountId;
            ViewBag.ShippedStartDate = ShippedStartDate;
            ViewBag.ShippedEndDate = ShippedEndDate;
            int pageSize = 5;
            int pageNumber = (page ?? 1);


            if (ShippedStartDate > ShippedEndDate)
            {
                shipmentSearch.Shipments = new ShipmentsListViewModel[0].ToPagedList(pageNumber, pageSize);
                ViewBag.errmsg = "Start Date must be before End Date.";
                return View(shipmentSearch);
            }


            // Add the condition to select a spefic shipping account if shipping account id is not null.
            if (ShippingAccountId != null)
            {
                // TODO: Construct the LINQ query to retrive only the shipments for the specified shipping account id.
                shipmentQuery = shipmentQuery.Where(c => c.ShippingAccountId == ShippingAccountId);
                if ((ShippedStartDate != null) && (ShippedEndDate != null))
                {
                    if (ShippedStartDate <= ShippedEndDate)
                    {
                        shipmentQuery = shipmentQuery.Where(c => ((c.ShippedDate >= ShippedStartDate) && (c.ShippedDate <= ShippedEndDate)));
                    }
                } else if ((ShippedStartDate != null) && (ShippedEndDate == null))
                {
                    shipmentQuery = shipmentQuery.Where(c => (c.ShippedDate >= ShippedStartDate));
                } else if ((ShippedStartDate == null) && (ShippedEndDate != null))
                {
                    shipmentQuery = shipmentQuery.Where(c => (c.ShippedDate <= ShippedEndDate));
                }

                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "delivereddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.DeliveredDate);
                        break;
                    case "delivereddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                        break;
                    case "origin_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                shipmentSearch.Shipments = shipmentQuery.ToPagedList(pageNumber, pageSize);
            }
            else if ((ShippedStartDate != null) && (ShippedEndDate != null))
            {
                if (ShippedStartDate <= ShippedEndDate)
                {
                    shipmentQuery = shipmentQuery.Where(c => (c.ShippedDate >= ShippedStartDate) && (c.ShippedDate <= ShippedEndDate));
                    ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                    ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                    ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                    ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                    ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                    ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                    switch (sortOrder)
                    {
                        case "servicetype_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.ServiceType);
                            break;
                        case "servicetype":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.ServiceType);
                            break;
                        case "shippeddate_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.ShippedDate);
                            break;
                        case "shippeddate":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.ShippedDate);
                            break;
                        case "recipientname_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.RecipientName);
                            break;
                        case "recipientname":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.RecipientName);
                            break;
                        case "delivereddate_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.DeliveredDate);
                            break;
                        case "delivereddate":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                            break;
                        case "origin_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.Origin);
                            break;
                        case "origin":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.Origin);
                            break;
                        case "destination_desc":
                            shipmentQuery = shipmentQuery.OrderByDescending(c => c.Destination);
                            break;
                        case "destination":
                            shipmentQuery = shipmentQuery.OrderBy(c => c.Destination);
                            break;
                        default:
                            shipmentQuery = shipmentQuery.OrderBy(c => c.ShippingAccountId);
                            break;
                    }
                    shipmentSearch.Shipments = shipmentQuery.ToPagedList(pageNumber, pageSize);
                }
            }
            else if ((ShippedStartDate != null) && (ShippedEndDate == null)) {
                shipmentQuery = shipmentQuery.Where(c => c.ShippedDate >= ShippedStartDate);
                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "delivereddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.DeliveredDate);
                        break;
                    case "delivereddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                        break;
                    case "origin_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                shipmentSearch.Shipments = shipmentQuery.ToPagedList(pageNumber, pageSize);
            }
            else if ((ShippedStartDate == null) && (ShippedEndDate != null)) {
                shipmentQuery = shipmentQuery.Where(c => c.ShippedDate <= ShippedEndDate);
                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "delivereddate_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.DeliveredDate);
                        break;
                    case "delivereddate":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                        break;
                    case "origin_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        shipmentQuery = shipmentQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        shipmentQuery = shipmentQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        shipmentQuery = shipmentQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                shipmentSearch.Shipments = shipmentQuery.ToPagedList(pageNumber, pageSize);
            }
            else
            {
                
                // Return an empty result if no shipping account id has been selected.
                shipmentSearch.Shipments = new ShipmentsListViewModel[0].ToPagedList(pageNumber, pageSize);
            }
            var aa = new DateTime(1900, 01, 01);
            var bb = new DateTime(2100, 01, 01);

            if (ShippedStartDate == aa && ShippedEndDate == bb)
            {
                shipmentSearch.Shipment.ShippedStartDate = DateTime.Now.Date;
                shipmentSearch.Shipment.ShippedEndDate = DateTime.Now.Date;
            }
            return View(shipmentSearch);
        }

        // 8(b) invoice history Generation
        [Authorize(Roles = "Customer, Employee")]
        public ActionResult GenerateInvoiceReport(int? ShippingAccountId, DateTime? ShippedStartDate, DateTime? ShippedEndDate,
            string sortOrder, int? page)
        {
            var invoiceSearch = new ShipmentsReportViewModel();
            invoiceSearch.Shipment = new ShipmentsSearchViewModel();
           

            if (ShippedStartDate == null && ShippedEndDate == null)
            {
                ShippedStartDate = new DateTime(1900, 01, 01);
                ShippedEndDate = new DateTime(2100, 01, 01);
            }
            /*if (ShippedStartDate == null && ShippedEndDate == null && ShippingAccountId == null)
            {
                ShippedStartDate = DateTime.Now.Date;
                ShippedEndDate = DateTime.Now.Date;
            }*/

            if (User.IsInRole("Employee"))
            {
                invoiceSearch.Shipment.ShippingAccounts = PopulateShippingAccountsDropdownList().ToList();
            }

            if (User.IsInRole("Customer"))
            {
                ShippingAccount TshippingAccount = db.ShippingAccounts.SingleOrDefault(c => c.UserName == User.Identity.Name);
                ShippingAccountId = TshippingAccount.ShippingAccountId;
            }

            // Populate the ShippingAccountId dropdown list.
            invoiceSearch.Shipment.ShippedStartDates = PopulateShippingDatesDropdownList().ToList();
            invoiceSearch.Shipment.ShippedEndDates = PopulateShippingDatesDropdownList().ToList();

            // Initialize the query to retrieve shipments using the ShipmentsListViewModel.
            var invoiceQuery = from s in db.Shipments
                               select new ShipmentsListViewModel
                               {
                                   WaybillId = s.WaybillId,
                                   ServiceType = s.ServiceType,
                                   ShippedDate = s.ShippedDate,
                                   // DeliveredDate = s.DeliveredDate,
                                   RecipientName = s.RecipientName,
                                   // NumberOfPackages = s.NumberOfPackages,
                                   Origin = s.Origin,
                                   Destination = s.Destination,
                                   TotalInvoiceAmount = s.TotalInvoiceAmount >= s.ShipmentAmount ? s.TotalInvoiceAmount : (s.DutiesAmount + s.TaxesAmount),
                                   ShipmentAmount = s.ShipmentAmount,
                                   DutiesAndTaxesAmount = s.TaxesAmount + s.DutiesAmount,
                                   DutiesTaxesCurrency = s.DutiesTaxesCurrency,
                                   ShipmentCurrency = s.ShipmentCurrency,
                                   ShippingAccountId = s.ShippingAccountId,
                                   TotalAmountCurrency = s.ShipmentCurrency,
                                   ShipmentPayerId = s.ShipmentPayerID,
                                   DutiesAndTaxesPayerId = s.DutiesTaxesPayerID
                               };

            ViewBag.CurrentSort = sortOrder;
            ViewBag.ShippingAccountId = ShippingAccountId;
            ViewBag.ShippedStartDate = ShippedStartDate;
            ViewBag.ShippedEndDate = ShippedEndDate;
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            if (ShippingAccountId != null)
            {
                // TODO: Construct the LINQ query to retrive only the shipments for the specified shipping account id. 
                foreach (var item in invoiceQuery)
                {
                    if ((item.ShipmentPayerId == ShippingAccountId) && (item.DutiesAndTaxesPayerId == ShippingAccountId))
                    {
                        item.TotalInvoiceAmount = item.ShipmentAmount + item.DutiesAndTaxesAmount;
                        string pncode = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == ShippingAccountId).AddressProvinceCode;
                        switch (pncode)
                        {
                            case "HK":
                                ViewBag.TotalAmountCurrency = "HKD";
                                break;
                            case "TW":
                                ViewBag.TotalAmountCurrency = "TWD";
                                break;
                            case "MC":
                                ViewBag.TotalAmountCurrency = "MOP";
                                break;
                            default:
                                ViewBag.TotalAmountCurrency = "CNY";
                                break;
                        }
                        
                    }
                    else if ((item.ShipmentPayerId == ShippingAccountId) && (item.DutiesAndTaxesPayerId != ShippingAccountId))
                    {
                        item.TotalInvoiceAmount = item.ShipmentAmount;
                        string pncode = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == ShippingAccountId).AddressProvinceCode;
                        switch (pncode)
                        {
                            case "HK":
                                ViewBag.TotalAmountCurrency = "HKD";
                                break;
                            case "TW":
                                ViewBag.TotalAmountCurrency = "TWD";
                                break;
                            case "MC":
                                ViewBag.TotalAmountCurrency = "MOP";
                                break;
                            default:
                                ViewBag.TotalAmountCurrency = "CNY";
                                break;
                        }
                    }
                    else if ((item.ShipmentPayerId != ShippingAccountId) && (item.DutiesAndTaxesPayerId == ShippingAccountId))
                    {
                        item.TotalInvoiceAmount = item.DutiesAndTaxesAmount;
                        string pncode = db.ShippingAccounts.SingleOrDefault(p => p.ShippingAccountId == ShippingAccountId).AddressProvinceCode;
                        switch (pncode)
                        {
                            case "HK":
                                ViewBag.TotalAmountCurrency = "HKD";
                                break;
                            case "TW":
                                ViewBag.TotalAmountCurrency = "TWD";
                                break;
                            case "MC":
                                ViewBag.TotalAmountCurrency = "MOP";
                                break;
                            default:
                                ViewBag.TotalAmountCurrency = "CNY";
                                break;
                        }
                    }
                }

                invoiceQuery = invoiceQuery.Where(c => ((c.ShipmentPayerId == ShippingAccountId) || (c.DutiesAndTaxesPayerId == ShippingAccountId && c.DutiesAndTaxesAmount > 0)) && (c.DutiesAndTaxesAmount != null));
                
                if ((ShippedStartDate != null) && (ShippedEndDate != null))
                {
                    if (ShippedStartDate <= ShippedEndDate)
                    {
                        invoiceQuery = invoiceQuery.Where(c => ((c.ShippedDate >= ShippedStartDate) && (c.ShippedDate <= ShippedEndDate)));
                    }
                    else
                    {
                        ViewBag.errormsg = "The end date should be after the start date";
                        invoiceSearch.Shipments = new ShipmentsListViewModel[0].ToPagedList(pageNumber, pageSize);

                    }
                }
                else if ((ShippedStartDate != null) && (ShippedEndDate == null))
                {
                    invoiceQuery = invoiceQuery.Where(c => (c.ShippedDate >= ShippedStartDate));
                }
                else if ((ShippedStartDate == null) && (ShippedEndDate != null))
                {
                    invoiceQuery = invoiceQuery.Where(c => (c.ShippedDate <= ShippedEndDate));
                }

                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.TotalInvoiceAmountSortParm = sortOrder == "totalinvoiceamount" ? "totalinvoiceamount_desc" : "totalinvoiceamount";
                // ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "totalinvoiceamount_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.TotalInvoiceAmount);
                        break;
                    case "totalinvoiceamount":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.TotalInvoiceAmount);
                        break;
                    // case "delivereddate_desc":
                    //     invoiceQuery = invoiceQuery.OrderByDescending(c => c.DeliveredDate);
                    //     break;
                    // case "delivereddate":
                    //     invoiceQuery = invoiceQuery.OrderBy(c => c.DeliveredDate);
                    //     break;
                    case "origin_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                invoiceSearch.Shipments = invoiceQuery.ToPagedList(pageNumber, pageSize);
            }
            else if ((ShippedStartDate != null) && (ShippedEndDate != null))
            {
                if (ShippedStartDate <= ShippedEndDate)
                {
                    invoiceQuery = invoiceQuery.Where(c => (c.ShippedDate >= ShippedStartDate) && (c.ShippedDate <= ShippedEndDate));
                    ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                    ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                    ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                    ViewBag.TotalInvoiceAmountSortParm = sortOrder == "totalinvoiceamount" ? "totalinvoiceamount_desc" : "totalinvoiceamount";
                    // ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                    ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                    ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                    switch (sortOrder)
                    {
                        case "servicetype_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.ServiceType);
                            break;
                        case "servicetype":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.ServiceType);
                            break;
                        case "shippeddate_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.ShippedDate);
                            break;
                        case "shippeddate":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.ShippedDate);
                            break;
                        case "recipientname_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.RecipientName);
                            break;
                        case "recipientname":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.RecipientName);
                            break;
                        case "totalinvoiceamount_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.TotalInvoiceAmount);
                            break;
                        case "totalinvoiceamount":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.TotalInvoiceAmount);
                            break;
                        // case "delivereddate_desc":
                        //    invoiceQuery = invoiceQuery.OrderByDescending(c => c.DeliveredDate);
                        //    break;
                        // case "delivereddate":
                        //    shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                        //    break;
                        case "origin_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.Origin);
                            break;
                        case "origin":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.Origin);
                            break;
                        case "destination_desc":
                            invoiceQuery = invoiceQuery.OrderByDescending(c => c.Destination);
                            break;
                        case "destination":
                            invoiceQuery = invoiceQuery.OrderBy(c => c.Destination);
                            break;
                        default:
                            invoiceQuery = invoiceQuery.OrderBy(c => c.ShippingAccountId);
                            break;
                    }
                    invoiceSearch.Shipments = invoiceQuery.ToPagedList(pageNumber, pageSize);
                }
                else
                {
                    ViewBag.errormsg = "The end date should be after the start date";
                    invoiceSearch.Shipments = new ShipmentsListViewModel[0].ToPagedList(pageNumber, pageSize);

                }
            }
            else if ((ShippedStartDate != null) && (ShippedEndDate == null))
            {
                invoiceQuery = invoiceQuery.Where(c => c.ShippedDate >= ShippedStartDate);
                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.TotalInvoiceAmountSortParm = sortOrder == "totalinvoiceamount" ? "totalinvoiceamount_desc" : "totalinvoiceamount";
                // ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "totalinvoiceamount_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.TotalInvoiceAmount);
                        break;
                    case "totalinvoiceamount":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.TotalInvoiceAmount);
                        break;
                    // case "delivereddate_desc":
                    //    shipmentQuery = shipmentQuery.OrderByDescending(c => c.DeliveredDate);
                    //    break;
                    // case "delivereddate":
                    //    shipmentQuery = shipmentQuery.OrderBy(c => c.DeliveredDate);
                    //    break;
                    case "origin_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                invoiceSearch.Shipments = invoiceQuery.ToPagedList(pageNumber, pageSize);
            }
            else if ((ShippedStartDate == null) && (ShippedEndDate != null))
            {
                invoiceQuery = invoiceQuery.Where(c => c.ShippedDate <= ShippedEndDate);
                ViewBag.ServiceTypeSortParm = sortOrder == "servicetype" ? "servicetype_desc" : "servicetype";
                ViewBag.ShippedDateSortParm = sortOrder == "shippeddate" ? "shippeddate_desc" : "shippeddate";
                ViewBag.RecipientNameSortParm = sortOrder == "recipientname" ? "recipientname_desc" : "recipientname";
                ViewBag.TotalInvoiceAmountSortParm = sortOrder == "totalinvoiceamount" ? "totalinvoiceamount_desc" : "totalinvoiceamount";
                // ViewBag.DeliveredDateSortParm = sortOrder == "delivereddate" ? "delivereddate_desc" : "delivereddate";
                ViewBag.OriginSortParm = sortOrder == "origin" ? "origin_desc" : "Origin";
                ViewBag.DestinationSortParm = sortOrder == "destination" ? "destination_desc" : "destination";

                switch (sortOrder)
                {
                    case "servicetype_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ServiceType);
                        break;
                    case "servicetype":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ServiceType);
                        break;
                    case "shippeddate_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.ShippedDate);
                        break;
                    case "shippeddate":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippedDate);
                        break;
                    case "recipientname_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.RecipientName);
                        break;
                    case "recipientname":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.RecipientName);
                        break;
                    case "totalinvoiceamount_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.TotalInvoiceAmount);
                        break;
                    case "totalinvoiceamount":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.TotalInvoiceAmount);
                        break;
                    // case "delivereddate_desc":
                    //    invoiceQuery = invoiceQuery.OrderByDescending(c => c.DeliveredDate);
                    //    break;
                    // case "delivereddate":
                    //    invoiceQuery = invoiceQuery.OrderBy(c => c.DeliveredDate);
                    //    break;
                    case "origin_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Origin);
                        break;
                    case "origin":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Origin);
                        break;
                    case "destination_desc":
                        invoiceQuery = invoiceQuery.OrderByDescending(c => c.Destination);
                        break;
                    case "destination":
                        invoiceQuery = invoiceQuery.OrderBy(c => c.Destination);
                        break;
                    default:
                        invoiceQuery = invoiceQuery.OrderBy(c => c.ShippingAccountId);
                        break;
                }
                invoiceSearch.Shipments = invoiceQuery.ToPagedList(pageNumber, pageSize);
            }
            else
            {
                // Return an empty result if no shipping account id has been selected.
                
                invoiceSearch.Shipments = new ShipmentsListViewModel[0].ToPagedList(pageNumber, pageSize);
            }
            var aa = new DateTime(1900, 01, 01);
            var bb = new DateTime(2100, 01, 01);

            if (ShippedStartDate == aa && ShippedEndDate == bb)
            {
                invoiceSearch.Shipment.ShippedStartDate = DateTime.Now.Date;
                invoiceSearch.Shipment.ShippedEndDate = DateTime.Now.Date;
            }
            return View(invoiceSearch);
        }


        private SelectList PopulateShippingDatesDropdownList()
        {
            var shippingDateQuery = db.Shipments.Select(c => c.ShippedDate).Distinct().OrderBy(c => c);
                      
            
            return new SelectList(shippingDateQuery);
        }

        private SelectList PopulateShippingAccountsDropdownList()
        {
            // TODO: Construct the LINQ query to retrieve the unique list of shipping account ids.
            var shippingAccountQuery = db.ShippingAccounts.Select(c => c.ShippingAccountId).Distinct().OrderBy(c => c);
            return new SelectList(shippingAccountQuery);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
        [HttpPost]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None, NoStore = true)]
        public ActionResult doesNumberExist([Bind(Prefix = "RecipientAccountNumber")]string AccountNumber)
        {
            
           
            int num =  Convert.ToInt32(AccountNumber);
            var user = db.ShippingAccounts.SingleOrDefault(s => s.ShippingAccountId == num);
            bool ifAccountExist = true;

            if (user == null)
            {
                ifAccountExist = false;
            }

            return Json(ifAccountExist, JsonRequestBehavior.AllowGet);            

            
        }

       

    }
}
