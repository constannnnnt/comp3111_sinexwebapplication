﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SinExWebApp20256966.Models;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace SinExWebApp20256966.Controllers
{
    public class ShipmentTrackingsController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();

        public ActionResult Shipmenttracking(string WaybillNumber)
        {
            if(!validWaybillNumber(WaybillNumber))
            {
                return View();
            }
            int waybillnumber = Convert.ToInt16(WaybillNumber);
            int find = db.Shipments.Where(s => s.WaybillId == waybillnumber).Count();
            if (find != 1)
            {
                return View();
            }
            else
            {
                var shipment = db.Shipments.SingleOrDefault(s => s.WaybillId == waybillnumber);
                ViewBag.Servicetype = shipment.ServiceType;
                ViewBag.Waybillnumber = shipment.WaybillId.ToString("00000000000000000000");
                ViewBag.WaybillId = shipment.WaybillId;

                var packages = db.Packages.Where(s => s.WaybillId == waybillnumber).ToList();
                string packagetypes = "", weights = "";
                for (int i = 0; i < packages.Count; i++)
                {
                    string temptypesize = packages[i].TypeSize;
                    int tempid = db.PackageTypeSizes.Where(s => s.Size == temptypesize).Select(s => s.PackageTypeID).Single();
                    string temppackagetype = db.PackageTypes.Where(s => s.PackageTypeID == tempid).Select(s => s.Type).Single();
                    packagetypes = packagetypes + temppackagetype + "  ";
                    if (temppackagetype == "Envelope")
                    {
                        weights = weights + "N/A" + "    ";
                    }
                    else
                    {
                        weights = weights + packages[i].CustomerWeight + "\t";
                    }
                }
                ViewBag.Packagetype = packagetypes;
                ViewBag.Weight = weights;

                var specialtracking = db.ShipmentTrackings.Where(s => s.WaybillId == waybillnumber).SingleOrDefault(s => s.Status != null);
                if (specialtracking != null)
                {
                    ViewBag.Deliveredto = specialtracking.DeliveredTo;
                    ViewBag.Deliveredat = specialtracking.DeliveredAt;
                    ViewBag.Status = specialtracking.Status;
                }

                var shipmenttrackings = db.ShipmentTrackings.Where(s => s.WaybillId == waybillnumber);
                return View(shipmenttrackings.ToList());
            }
        }

        public bool validWaybillNumber(string WaybillNumber)
        {
            if (String.IsNullOrEmpty(WaybillNumber))
            {
                return false;
            }
            else if (!Regex.IsMatch(WaybillNumber, @"^[0-9]*$"))
            {
                return false;
            }
            else
            {
                int length = WaybillNumber.Length;
                if (length != 16) { return false; }
                return true;
            }
        }

        // GET: ShipmentTrackings
        public ActionResult Index()
        {
            var shipmentTrackings = db.ShipmentTrackings.Include(s => s.Shipment);
            return View(shipmentTrackings.ToList());
        }

        // GET: ShipmentTrackings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShipmentTracking shipmentTracking = db.ShipmentTrackings.Find(id);
            if (shipmentTracking == null)
            {
                return HttpNotFound();
            }
            return View(shipmentTracking);
        }

        // GET: ShipmentTrackings/Create
        public ActionResult Create(DateTime? Date, TimeSpan? Time, string Activity, string Location, string Remarks, string DeliveredTo, string DeliveredAt, string WaybillNumber, string submit)
        {
            /*var WaybillIds = db.Shipments.Where(s => s.Status == null).Where(s => s.ConfirmStatus == true).Where(s => s.DeleteStatus == false).Select(s => s.WaybillId).Distinct().ToList();
            List<SelectListItem> waybillIdList = new List<SelectListItem>();
            for (int i=0; i<WaybillIds.Count; i++)
            {
                int tempint = WaybillIds[i];
                string temp = tempint.ToString("0000000000000000");
                SelectListItem selListItem = new SelectListItem() { Value = temp, Text = temp };
                waybillIdList.Add(selListItem);
            }
            ViewBag.WaybillNumber = waybillIdList;*/

            var activities = new SelectList(new List<SelectListItem>
            {
                new SelectListItem { Text = "Picked up", Value = "Picked up"},
                new SelectListItem { Text = "At local sort facility", Value = "At local sort facility"},
                new SelectListItem { Text = "Left origin", Value = "Left origin"},
                new SelectListItem { Text = "On vehicle for delivery", Value = "On vehicle for delivery"},
                new SelectListItem { Text = "Delivered", Value = "Delivered"},
                new SelectListItem { Text = "Returned", Value = "Returned"},
                new SelectListItem { Text = "Lost", Value = "Lost"}
            }, "Value", "Text");
            ViewBag.Activity = activities;

            if (submit == "Create")
            {
                // Check validation
                if (!validWaybillNumber(WaybillNumber))
                {
                    ViewBag.message = "Please fill in a valid waybill number.";
                    return View();
                }
                int WaybillId = Convert.ToInt16(WaybillNumber);
                int i = db.Shipments.Where(s => s.Status == null).Where(s => s.ConfirmStatus == true).Where(s => s.WaybillId == WaybillId).Where(s => s.DeleteStatus == false).Count();
                if (i != 1)
                {
                    ViewBag.message = "Please fill in a valid waybill number.";
                    return View();
                }

                if (Time > new TimeSpan(23, 59, 59) || Time < new TimeSpan(00, 00, 00))
                {
                    ViewBag.message = "Please fill in a valid time.";
                    return View();
                }

                if (Date > new DateTime(2020, 12, 12) || Date < new DateTime(2017, 05, 01))
                {
                    ViewBag.message = "Please fill in a valid date.";
                    return View();
                }

                Shipment shipment = db.Shipments.Find(WaybillId);
                List<ShipmentTracking> TrackingHistory = db.ShipmentTrackings.Where(s => s.WaybillId == WaybillId).ToList();
                if (Date == null || Time == null || String.IsNullOrEmpty(Activity) || String.IsNullOrEmpty(Location))
                {
                    ViewBag.message = "Please fill in or select Date, Time, Activity and Location.";
                    return View();
                }
                if (Activity != "Delivered")
                {
                    if ((!String.IsNullOrEmpty(DeliveredTo)) || (!String.IsNullOrEmpty(DeliveredAt)))
                    {
                        ViewBag.message = "It's not delivered yet. How could you know whom the shipment is delivered to or where the shipment is delivered at?";
                        return View();
                    }
                }

                if (TrackingHistory.Count == 0)
                {
                    if (Activity != "Picked up")
                    {
                        ViewBag.message = "The shipment should be picked up first.";
                        return View();
                    }
                    DateTime temp = (DateTime)Date + (TimeSpan)Time;
                    DateTime pickuptime = (DateTime)shipment.PickupTime;
                    if (temp.AddHours(1) < pickuptime)
                    {
                        ViewBag.message = "The pick up date and time is too early!";
                        return View();
                    }
                }
                else
                {
                    List<DateTime> dates = db.ShipmentTrackings.Where(s => s.WaybillId == WaybillId).Select(s => s.Date).ToList();
                    List<TimeSpan> times = db.ShipmentTrackings.Where(s => s.WaybillId == WaybillId).Select(s => s.Time).ToList();
                    if (Date < dates.Max() || (Date == dates.Max() && Time <= times.Max()))
                    {
                        ViewBag.message = "Time is moving forward!";
                        return View();
                    }
                    if (TrackingHistory.Count == 1)
                    {
                        if (Activity != "At local sort facility" && Activity != "Returned" && Activity != "Lost")
                        {
                            ViewBag.message = "The shipment should be delivered to local sort facility(or it might be returned or lost).";
                            return View();
                        }
                    }
                    if (Activity == "Picked up")
                    {
                        ViewBag.message = "The shipment cannot be picked up again!";
                        return View();
                    }
                    if (Activity == "At local sort facility")
                    {
                        if (TrackingHistory.Count != 1 && TrackingHistory.Count != 3)
                        {
                            ViewBag.message = "It's not a correct activity.";
                            return View();
                        }
                    }
                    if (Activity == "Left origin")
                    {
                        if (String.IsNullOrEmpty(shipment.ShipemtnPaymentAuthorizationCode))
                        {
                            ViewBag.message = "The shipment will not go anywhere else before payment finished.";
                            return View();
                        }
                        if (TrackingHistory.Count != 2)
                        {
                            ViewBag.message = "It's not a correct activity.";
                            return View();
                        }
                    }
                    if (Activity == "On vehicle for delivery")
                    {
                        if (TrackingHistory.Count != 4)
                        {
                            ViewBag.message = "It's not a correct activity.";
                            return View();
                        }
                    }
                    if (Activity == "Delivered")
                    {
                        if (TrackingHistory.Count != 5)
                        {
                            ViewBag.message = "It's not a correct activity.";
                            return View();
                        }
                        if (String.IsNullOrEmpty(DeliveredTo) || String.IsNullOrEmpty(DeliveredAt))
                        {
                            ViewBag.message = "Please fill in or select DeliveredTo and DeliveredAt.";
                            return View();
                        }
                    }
                }

                // Create record and trigger special events
                if (Activity == "Picked up")
                {
                    shipment.PickupTime = Date + Time;
                    shipment.ShippedDate = (DateTime)Date;
                    shipment.PickupStatus = true;
                }

                if (Activity == "Delivered" || Activity == "Returned" || Activity == "Lost")
                {
                    shipment.Status = Activity;
                }

                ShipmentTracking shipmentTracking = new ShipmentTracking();
                shipmentTracking.Date = (DateTime)Date;
                shipmentTracking.Time = (TimeSpan)Time;
                shipmentTracking.Activity = Activity;
                shipmentTracking.Location = Location;
                shipmentTracking.Remarks = Remarks;
                shipmentTracking.DeliveredTo = DeliveredTo;
                shipmentTracking.DeliveredAt = DeliveredAt;
                if (Activity == "Delivered" || Activity == "Returned" || Activity == "Lost")
                {
                    shipmentTracking.Status = Activity;
                }
                shipmentTracking.WaybillId = WaybillId;

                //Shipment newshipment = db.Shipments.Where(s => s.WaybillId == WaybillId).Single();
                if (Activity == "Delivered")
                {
                    shipment.DeliveredDate = (DateTime)Date;
                }
                
                db.Entry(shipment).State = EntityState.Modified;
                db.SaveChanges();
                db.ShipmentTrackings.Add(shipmentTracking);
                db.SaveChanges();

                if (Activity == "Picked up")
                {
                    InformRecipient(WaybillId);
                }
                if (Activity == "Delivered")
                {
                    InformSender(WaybillId);
                }
                return RedirectToAction("Shipmenttracking");
            }

            return View();
            
        }

        public void InformRecipient(int WaybillId)
        {
            bool need = db.Shipments.Where(s => s.WaybillId == WaybillId).Select(s => s.NotifyRecipient).Single();
            if (need)
            {
                // Send email
                MailMessage mail = new MailMessage();
                Shipment theshipment = db.Shipments.Where(s => s.WaybillId == WaybillId).Single();
                int senderid = theshipment.ShippingAccountId;
                string email = theshipment.EmailAddress;
                string waybillnumber = WaybillId.ToString("0000000000000000");
                string sendername = "  ";
                ShippingAccount person = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                string senderaddress = "  ";
                if (String.IsNullOrEmpty(person.AddressBuilding))
                {
                    senderaddress = person.AddressStreet + ", " + person.AddressCity + ", " + person.AddressProvinceCode ;
                }
                else
                {
                    senderaddress = person.AddressBuilding + ", " + person.AddressStreet + ", " + person.AddressCity + ", " + person.AddressProvinceCode;
                }
                if (!String.IsNullOrEmpty(person.AddressPostalCode))
                {
                    senderaddress = senderaddress + ", " + person.AddressPostalCode;
                }

                if (person is BusinessShippingAccount)
                {
                    BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                    sendername = bperson.ContactPersonName;
                }
                else if (person is PersonalShippingAccount)
                {
                    PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                    sendername = pperson.FirstName + " " + pperson.LastName;
                }
                DateTime pickupdate = (DateTime) theshipment.PickupTime;
                string recipientname = theshipment.RecipientName;

                // Set the sender (From), receiver (To), subject and 
                // message body fields of the mail message.
                mail.From = new MailAddress("comp3111_team106@cse.ust.hk",
                     "team 106 CLLW");
                mail.To.Add(email);
                mail.Subject = "Successful Pickup";
                mail.Body = "Dear " + recipientname + ",\n\n" + sendername + " who lives at " + senderaddress + " has sent you a shipment with waybill number: " + waybillnumber + "! It is picked up at " + pickupdate + ".\nYou can go to our website to track this shipment.\n\nRegards,\nCLLW Team";

                // Create an instance of SmtpClient named emailServer.
                // Set the mail server to use as "smtp.ust.hk".
                SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

                // Send the message.
                emailServer.Send(mail);
            }
        }

        public void InformSender(int WaybillId)
        {
            bool need = db.Shipments.Where(s => s.WaybillId == WaybillId).Select(s => s.NotifySender).Single();
            if (need)
            {
                // Send email
                MailMessage mail = new MailMessage();
                Shipment theshipment = db.Shipments.Where(s => s.WaybillId == WaybillId).Single();
                int senderid = theshipment.ShippingAccountId;
                string sendername = "  ";
                ShippingAccount person = db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                if (person is BusinessShippingAccount)
                {
                    BusinessShippingAccount bperson = (BusinessShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                    sendername = bperson.ContactPersonName;
                }
                else if (person is PersonalShippingAccount)
                {
                    PersonalShippingAccount pperson = (PersonalShippingAccount)db.ShippingAccounts.SingleOrDefault(c => c.ShippingAccountId == senderid);
                    sendername = pperson.FirstName + " " + pperson.LastName;
                }
                string email = theshipment.EmailAddress;
                string waybillnumber = WaybillId.ToString("0000000000000000");
                string recipientname = theshipment.RecipientName;
                string recipientstreet = theshipment.Street;
                string recipientbuilding = theshipment.Building;
                string recipientcity = theshipment.City;
                string recipientprovince = theshipment.Province;
                string recipientpostalcode = theshipment.PostalCode;
                string recipient_address;
                if (String.IsNullOrEmpty(recipientbuilding))
                {
                    recipient_address = recipientstreet + ", " + recipientcity + ", " + recipientprovince;
                }
                else
                {
                    recipient_address = recipientbuilding + ", " + recipientstreet + ", " + recipientcity + ", " + recipientprovince;
                }
                if (String.IsNullOrEmpty(recipientpostalcode))
                {
                    recipient_address = recipient_address + ", " + recipientpostalcode;
                }
                DateTime deliverydate = theshipment.DeliveredDate;

                // Set the sender (From), receiver (To), subject and 
                // message body fields of the mail message.
                mail.From = new MailAddress("comp3111_team106@cse.ust.hk",
                     "team 106 CLLW");
                mail.To.Add(email);
                mail.Subject = "Successful Delivery";
                mail.Body = "Dear " + sendername + ",\n\nOn " + deliverydate + ", your shipment with waybill number: " + waybillnumber + " has been delivered to " + recipientname + " at " + recipient_address + " successfully!\nThank you for choosing our service.\n\nRegards,\nCLLW Team";

                // Create an instance of SmtpClient named emailServer.
                // Set the mail server to use as "smtp.ust.hk".
                SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

                // Send the message.
                emailServer.Send(mail);
            }
        }

        // POST: ShipmentTrackings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        /*public ActionResult Create([Bind(Include = "ShipmentTrackingID,Date,Time,Activity,Location,Remarks,DeliveredTo,DeliveredAt,Status,WaybillId")] ShipmentTracking shipmentTracking)
        {
            if (ModelState.IsValid)
            {
                db.ShipmentTrackings.Add(shipmentTracking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WaybillId = new SelectList(db.Shipments, "WaybillId", "ReferenceNumber", shipmentTracking.WaybillId);
            return View(shipmentTracking);
        }*/

        // GET: ShipmentTrackings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShipmentTracking shipmentTracking = db.ShipmentTrackings.Find(id);
            if (shipmentTracking == null)
            {
                return HttpNotFound();
            }
            ViewBag.WaybillId = new SelectList(db.Shipments, "WaybillId", "ReferenceNumber", shipmentTracking.WaybillId);
            return View(shipmentTracking);
        }

        // POST: ShipmentTrackings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShipmentTrackingID,Date,Time,Activity,Location,Remarks,DeliveredTo,DeliveredAt,Status,WaybillId")] ShipmentTracking shipmentTracking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shipmentTracking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WaybillId = new SelectList(db.Shipments, "WaybillId", "ReferenceNumber", shipmentTracking.WaybillId);
            return View(shipmentTracking);
        }

        // GET: ShipmentTrackings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShipmentTracking shipmentTracking = db.ShipmentTrackings.Find(id);
            if (shipmentTracking == null)
            {
                return HttpNotFound();
            }
            return View(shipmentTracking);
        }

        // POST: ShipmentTrackings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ShipmentTracking shipmentTracking = db.ShipmentTrackings.Find(id);
            db.ShipmentTrackings.Remove(shipmentTracking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
