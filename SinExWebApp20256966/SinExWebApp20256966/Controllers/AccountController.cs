﻿
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SinExWebApp20256966.Models;
using SinExWebApp20256966.ViewModels;
using System.Text.RegularExpressions;

// Add spaces to allow use of the mail sending functionality.
using System.Net;
using System.Net.Mail;

namespace SinExWebApp20256966.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private SinExWebDatabaseContext db = new SinExWebDatabaseContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string accountType)
        {
            ViewBag.AccountType = accountType;
            return View(new RegisterCustomerViewModel());
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterCustomerViewModel model)
        {
            int CityProvinceMatch = 0;
            int ProvincePostalMatch = 0;
            int CardTypeNumberMatch = 0;
            if (model.PersonalInformation != null)
            {
                if (model.PersonalInformation.CardExpireYear == "2017" && Convert.ToInt16(model.PersonalInformation.CardExpireMonth) <= 4)
                {
                    ViewBag.CardExpire = "The card is already expired.";
                    return View(model);
                }

                var pCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
                foreach (var item in pCityProvince)
                {
                    if (item.City == model.PersonalInformation.AddressCity && (item.ProvinceCode == model.PersonalInformation.AddressProvinceCode))
                    {
                        CityProvinceMatch = 1;
                    }
                }


                if (CityProvinceMatch == 1 && model.PersonalInformation.AddressPostalCode != null)
                {
                    ProvincePostalMatch = ValidateCityPostalCode(model.PersonalInformation.AddressPostalCode, model.PersonalInformation.AddressCity);
                    if (ProvincePostalMatch == 0)
                    {
                        ProvincePostalMatch = ValidateProvincePostalCode(model.PersonalInformation.AddressPostalCode, model.PersonalInformation.AddressProvinceCode);
                    }
                }
                if (model.PersonalInformation.AddressPostalCode == null)
                {
                    ProvincePostalMatch = 1;
                }
                CardTypeNumberMatch = ValidateCard(model.PersonalInformation.CardType, model.PersonalInformation.CardNumber, model.PersonalInformation.CardSecurityNumber);
            }
            else if (model.BusinessInformation != null)
            {
                if (model.BusinessInformation.CardExpireYear == "2017" && Convert.ToInt16(model.BusinessInformation.CardExpireMonth) <= 4)
                {
                    ViewBag.CardExpire = "The card is already expired.";
                    return View(model);
                }

                var bCityProvince = db.Destinations.Select(c => new { c.City, c.ProvinceCode }).ToList();
                foreach (var item in bCityProvince)
                {
                    if (item.City == model.BusinessInformation.AddressCity && (item.ProvinceCode == model.BusinessInformation.AddressProvinceCode))
                    {
                        CityProvinceMatch = 1;
                    }
                }

                if (CityProvinceMatch == 1 && model.BusinessInformation.AddressPostalCode != null)
                {
                    ProvincePostalMatch = ValidateCityPostalCode(model.BusinessInformation.AddressPostalCode, model.BusinessInformation.AddressCity);
                    if (ProvincePostalMatch == 0)
                    {
                        ProvincePostalMatch = ValidateProvincePostalCode(model.BusinessInformation.AddressPostalCode, model.BusinessInformation.AddressProvinceCode);
                    }
                }
                if (model.BusinessInformation.AddressPostalCode == null)
                {
                    ProvincePostalMatch = 1;
                }

                CardTypeNumberMatch = ValidateCard(model.BusinessInformation.CardType, model.BusinessInformation.CardNumber, model.BusinessInformation.CardSecurityNumber);

            }

            if (CityProvinceMatch == 0) ViewBag.CityPronvince = "Your City and Province Code do not match.";
            if (ProvincePostalMatch == 0) ViewBag.ProvincePostal = "Your City/Province Code and Postal Code do not match.";
            if (CardTypeNumberMatch == 0) ViewBag.CardTypeNumber = "Your Card Type and Number do not match or your security code don't match with cardtype.";

            if (ModelState.IsValid && (CityProvinceMatch == 1) && (ProvincePostalMatch == 1) && (CardTypeNumberMatch == 1))
            {
                if (model.PersonalInformation != null)
                {
                    model.LoginInformation.Email = model.PersonalInformation.Email;
                }
                else // AccountType = "Business"
                {
                    model.LoginInformation.Email = model.BusinessInformation.Email;
                }
                var user = new ApplicationUser { UserName = model.LoginInformation.UserName, Email = model.LoginInformation.Email };
                var result = await UserManager.CreateAsync(user, model.LoginInformation.Password);
                if (result.Succeeded)
                {
                    // Assign user to Customer role.
                    var roleResult = await UserManager.AddToRolesAsync(user.Id, "Customer");
                    if (roleResult.Succeeded)
                    {
                        // Create a shipping account for the customer.
                        if (model.PersonalInformation != null)
                        {
                            model.PersonalInformation.UserName = user.UserName;
                            db.ShippingAccounts.Add(model.PersonalInformation);
                        }
                        else
                        {
                            model.BusinessInformation.UserName = user.UserName;
                            db.ShippingAccounts.Add(model.BusinessInformation);
                        }
                        db.SaveChanges();
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");


                        SendEmail(user.UserName,user.Email); // Send a confirmation email

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        AddErrors(roleResult);
                    }
                }

                AddErrors(result);
            }
            if (model.PersonalInformation != null)
            {
                ViewBag.AccountType = "Personal";
            }
            else
            {
                ViewBag.AccountType = "Business";
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public int ValidateCityPostalCode(string postalCode, string City)
        {
            int ProvincePostalMatch = 0;
            string ReResult = Regex.Match(postalCode, @"^[0-9][0-9]").Value;
            string ReResult_TW = Regex.Match(postalCode, @"^[1-9][0-9][0-9]").Value;
            string ReResult_SameProvince = Regex.Match(postalCode, @"^[0-9][0-9][0-9][0-9]").Value;

            switch (City)
            {
                case "Beijing":
                    if (ReResult == "10") ProvincePostalMatch = 1;
                    break;
                case "Guiyang":
                    if (ReResult == "55") ProvincePostalMatch = 1;
                    break;
                case "Hulun Buir":
                    if (ReResult == "02") ProvincePostalMatch = 1;
                    break;
                case "Nanjing":
                    if (ReResult == "21") ProvincePostalMatch = 1;
                    break;
                case "Tianjin":
                    if (ReResult == "30") ProvincePostalMatch = 1;
                    break;
                case "Changchun":
                    if (ReResult == "13") ProvincePostalMatch = 1;
                    break;
                case "Haikou":
                    if (ReResult == "57") ProvincePostalMatch = 1;
                    break;
                case "Jinan":
                    if (ReResult == "25") ProvincePostalMatch = 1;
                    break;
                case "Nanning":
                    if (ReResult == "53") ProvincePostalMatch = 1;
                    break;
                case "Urumqi":
                    if (ReResult == "83") ProvincePostalMatch = 1;
                    break;
                case "Changsha":
                    if (ReResult == "41") ProvincePostalMatch = 1;
                    break;
                case "Hailar":
                    if (ReResult == "02") ProvincePostalMatch = 1;
                    break;
                case "Wuhan":
                    if (ReResult == "43") ProvincePostalMatch = 1;
                    break;
                case "Qiqihar":
                    if (ReResult == "16") ProvincePostalMatch = 1;
                    break;
                case "Chengdu":
                    if (ReResult == "61") ProvincePostalMatch = 1;
                    break;
                case "Kashi":
                    if (ReResult == "84") ProvincePostalMatch = 1;
                    break;
                case "Hangzhou":
                    if (ReResult == "31") ProvincePostalMatch = 1;
                    break;
                case "Kunming":
                    if (ReResult == "65") ProvincePostalMatch = 1;
                    break;
                case "Shanghai":
                    if (ReResult == "20") ProvincePostalMatch = 1;
                    break;
                case "Xi'an":
                    if (ReResult == "71") ProvincePostalMatch = 1;
                    break;
                case "Chongqing":
                    if (ReResult == "40") ProvincePostalMatch = 1;
                    break;
                case "Harbin":
                    if (ReResult == "15") ProvincePostalMatch = 1;
                    break;
                case "Lanzhou":
                    if (ReResult == "73") ProvincePostalMatch = 1;
                    break;
                case "Shenyang":
                    if (ReResult == "11") ProvincePostalMatch = 1;
                    break;
                case "Xining":
                    if (ReResult == "81") ProvincePostalMatch = 1;
                    break;
                case "Fuzhou":
                    if (ReResult == "35") ProvincePostalMatch = 1;
                    break;
                case "Hefei":
                    if (ReResult == "23") ProvincePostalMatch = 1;
                    break;
                case "Lhasa":
                    if (ReResult == "85") ProvincePostalMatch = 1;
                    break;
                case "Shijiazhuang":
                    if (ReResult == "05") ProvincePostalMatch = 1;
                    break;
                case "Yinchuan":
                    if (ReResult == "75") ProvincePostalMatch = 1;
                    break;
                case "Golmud":
                    if (ReResult == "81") ProvincePostalMatch = 1;
                    break;
                case "Hohhot":
                    if (ReResult == "01") ProvincePostalMatch = 1;
                    break;
                case "Macau":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1;
                    break;
                case "Taipei":
                    if ((ReResult_TW == "100") ||
                        (ReResult_TW == "103") ||
                        (ReResult_TW == "104") ||
                        (ReResult_TW == "105") ||
                        (ReResult_TW == "106") ||
                        (ReResult_TW == "108") ||
                        (ReResult_TW == "110") ||
                        (ReResult_TW == "111") ||
                        (ReResult_TW == "112") ||
                        (ReResult_TW == "114") ||
                        (ReResult_TW == "115") ||
                        (ReResult_TW == "116")) ProvincePostalMatch = 1;
                    break;
                case "Yumen":
                    if (ReResult_SameProvince == "7352") ProvincePostalMatch = 1;
                    break;
                case "Guangzhou":
                    if (ReResult == "51") ProvincePostalMatch = 1;
                    break;
                case "Hong Kong":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1;
                    break;
                case "Nanchang":
                    if (ReResult == "33") ProvincePostalMatch = 1;
                    break;
                case "Taiyuan":
                    if (ReResult == "03") ProvincePostalMatch = 1;
                    break;
                case "Zhengzhou":
                    if (ReResult == "45") ProvincePostalMatch = 1;
                    break;
            }
            return ProvincePostalMatch;
        }

        public int ValidateProvincePostalCode(string postalCode, string province)
        {
            int ProvincePostalMatch = 0;
            string ReResult = Regex.Match(postalCode, @"^[0-9][0-9]").Value;
            string ReResult_TW = Regex.Match(postalCode, @"^[1-9][0-9][0-9]").Value;
            string ReResult_SameProvince = Regex.Match(postalCode, @"^[0-9][0-9][0-9][0-9]").Value;

            switch (province)
            {
                case "BJ":
                    if (ReResult == "10") ProvincePostalMatch = 1; // beijing
                    break;
                case "GZ":
                    if (ReResult == "55" || ReResult == "56") ProvincePostalMatch = 1; // gui zhou
                    break;
                case "NM":
                    if (ReResult == "02" || ReResult == "01") ProvincePostalMatch = 1; // nei meng
                    break;
                case "JS":
                    if (ReResult == "21" || ReResult == "22") ProvincePostalMatch = 1; // jiang su
                    break;
                case "HE":
                    if (ReResult == "30") ProvincePostalMatch = 1; // Tianjin
                    if (ReResult == "05" || ReResult == "06" || ReResult == "07") ProvincePostalMatch = 1; // Heibei district
                    break;
                case "JL":
                    if (ReResult == "13") ProvincePostalMatch = 1; // Jilin
                    break;
                case "HI":
                    if (ReResult == "57") ProvincePostalMatch = 1; // hai nan
                    break;
                case "SD":
                    if (ReResult == "25" || ReResult == "27" || ReResult == "26") ProvincePostalMatch = 1; //Shan dong
                    break;
                case "JX":
                    if (ReResult == "33" || ReResult == "34") ProvincePostalMatch = 1; // Jiangxi
                    break;
                case "GX":
                    if (ReResult == "53" || ReResult == "54") ProvincePostalMatch = 1; // Guangxi
                    break;
                case "XJ": // XIN JIANG
                    if (ReResult == "83") ProvincePostalMatch = 1;
                    if (ReResult == "84") ProvincePostalMatch = 1;
                    break;
                case "HN":
                    if (ReResult == "41" || ReResult == "42") ProvincePostalMatch = 1; // Hunan
                    break;
                case "HB":
                    if (ReResult == "43" || ReResult == "44") ProvincePostalMatch = 1; // Hubei
                    break;
                case "HL":
                    if (ReResult == "16") ProvincePostalMatch = 1;
                    if (ReResult == "15") ProvincePostalMatch = 1; // Heilongjiang
                    break;
                case "SC":
                    if (ReResult == "61" || ReResult == "62" || ReResult == "63" || ReResult == "64") ProvincePostalMatch = 1; // SICHUAN
                    break;
                case "ZJ":
                    if (ReResult == "31" || ReResult == "32") ProvincePostalMatch = 1; // ZHE JIANG
                    break;
                case "YN":
                    if (ReResult == "65" || ReResult == "66" || ReResult == "67") ProvincePostalMatch = 1; // YUNNAN
                    break;
                case "SH":
                    if (ReResult == "20") ProvincePostalMatch = 1; //SHANG HAI
                    break;
                case "SN":
                    if (ReResult == "71" || ReResult == "72") ProvincePostalMatch = 1; //SHANNA XI
                    break;
                case "CQ":
                    if (ReResult == "40") ProvincePostalMatch = 1; //CHONG QING
                    break;
                case "GS":
                    if (ReResult == "73" || ReResult == "74") ProvincePostalMatch = 1;
                    break;
                case "LN":
                    if (ReResult == "11" || ReResult == "12") ProvincePostalMatch = 1; // Liaoning 
                    break;
                case "QH":
                    if (ReResult == "81") ProvincePostalMatch = 1; //QINGHAI
                    break;
                case "AH":
                    if (ReResult == "23" || ReResult == "24") ProvincePostalMatch = 1; // AN HUI
                    break;
                case "XZ":
                    if (ReResult == "85" || ReResult == "86") ProvincePostalMatch = 1; // Tibet
                    break;
                case "NX":
                    if (ReResult == "75") ProvincePostalMatch = 1; //NING XIA
                    break;
                case "MC":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // MACAU
                    break;
                case "TW":
                    if ((ReResult_TW == "100") ||
                        (ReResult_TW == "103") ||
                        (ReResult_TW == "104") ||
                        (ReResult_TW == "105") ||
                        (ReResult_TW == "106") ||
                        (ReResult_TW == "108") ||
                        (ReResult_TW == "110") ||
                        (ReResult_TW == "111") ||
                        (ReResult_TW == "112") ||
                        (ReResult_TW == "114") ||
                        (ReResult_TW == "115") ||
                        (ReResult_TW == "116") ||
                        (ReResult_TW == "200") ||
                        (ReResult_TW == "201") ||
                        (ReResult_TW == "202") ||
                        (ReResult_TW == "203") ||
                        (ReResult_TW == "204") ||
                        (ReResult_TW == "205") ||
                        (ReResult_TW == "206") ||
                        (ReResult_TW == "209") ||
                        (ReResult_TW == "210") ||
                        (ReResult_TW == "211") ||
                        (ReResult_TW == "212") ||
                        (ReResult_TW == "207") ||
                        (ReResult_TW == "208") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "220") ||
                        (ReResult_TW == "221") ||
                        (ReResult_TW == "222") ||
                        (ReResult_TW == "223") ||
                        (ReResult_TW == "224") ||
                        (ReResult_TW == "226") ||
                        (ReResult_TW == "227") ||
                        (ReResult_TW == "228") ||
                        (ReResult_TW == "231") ||
                        (ReResult_TW == "232") ||
                        (ReResult_TW == "233") ||
                        (ReResult_TW == "234") ||
                        (ReResult_TW == "235") ||
                        (ReResult_TW == "236") ||
                        (ReResult_TW == "237") ||
                        (ReResult_TW == "238") ||
                        (ReResult_TW == "239") ||
                        (ReResult_TW == "241") ||
                        (ReResult_TW == "242") ||
                        (ReResult_TW == "243") ||
                        (ReResult_TW == "244") ||
                        (ReResult_TW == "245") ||
                        (ReResult_TW == "248") ||
                        (ReResult_TW == "249") ||
                        (ReResult_TW == "251") ||
                        (ReResult_TW == "252") ||
                        (ReResult_TW == "253") ||
                        (ReResult_TW == "260") ||
                        (ReResult_TW == "261") ||
                        (ReResult_TW == "262") ||
                        (ReResult_TW == "263") ||
                        (ReResult_TW == "264") ||
                        (ReResult_TW == "265") ||
                        (ReResult_TW == "266") ||
                        (ReResult_TW == "267") ||
                        (ReResult_TW == "268") ||
                        (ReResult_TW == "269") ||
                        (ReResult_TW == "270") ||
                        (ReResult_TW == "272") ||
                        (ReResult_TW == "290") ||
                        (ReResult_TW == "300") ||
                        (ReResult_TW == "301") ||
                        (ReResult_TW == "302") ||
                        (ReResult_TW == "303") ||
                        (ReResult_TW == "304") ||
                        (ReResult_TW == "305") ||
                        (ReResult_TW == "306") ||
                        (ReResult_TW == "307") ||
                        (ReResult_TW == "308") ||
                        (ReResult_TW == "310") ||
                        (ReResult_TW == "311") ||
                        (ReResult_TW == "312") ||
                        (ReResult_TW == "313") ||
                        (ReResult_TW == "314") ||
                        (ReResult_TW == "315") ||
                        (ReResult_TW == "320") ||
                        (ReResult_TW == "324") ||
                        (ReResult_TW == "325") ||
                        (ReResult_TW == "326") ||
                        (ReResult_TW == "327") ||
                        (ReResult_TW == "328") ||
                        (ReResult_TW == "330") ||
                        (ReResult_TW == "333") ||
                        (ReResult_TW == "334") ||
                        (ReResult_TW == "335") ||
                        (ReResult_TW == "336") ||
                        (ReResult_TW == "337") ||
                        (ReResult_TW == "338") ||
                        (ReResult_TW == "350") ||
                        (ReResult_TW == "351") ||
                        (ReResult_TW == "352") ||
                        (ReResult_TW == "353") ||
                        (ReResult_TW == "354") ||
                        (ReResult_TW == "356") ||
                        (ReResult_TW == "357") ||
                        (ReResult_TW == "358") ||
                        (ReResult_TW == "360") ||
                        (ReResult_TW == "361") ||
                        (ReResult_TW == "362") ||
                        (ReResult_TW == "363") ||
                        (ReResult_TW == "364") ||
                        (ReResult_TW == "365") ||
                        (ReResult_TW == "366") ||
                        (ReResult_TW == "367") ||
                        (ReResult_TW == "368") ||
                        (ReResult_TW == "369") ||
                        (ReResult_TW == "400") ||
                        (ReResult_TW == "401") ||
                        (ReResult_TW == "402") ||
                        (ReResult_TW == "403") ||
                        (ReResult_TW == "404") ||
                        (ReResult_TW == "407") ||
                        (ReResult_TW == "408") ||
                        (ReResult_TW == "411") ||
                        (ReResult_TW == "412") ||
                        (ReResult_TW == "413") ||
                        (ReResult_TW == "414") ||
                        (ReResult_TW == "420") ||
                        (ReResult_TW == "421") ||
                        (ReResult_TW == "422") ||
                        (ReResult_TW == "423") ||
                        (ReResult_TW == "424") ||
                        (ReResult_TW == "426") ||
                        (ReResult_TW == "427") ||
                        (ReResult_TW == "428") ||
                        (ReResult_TW == "429") ||
                        (ReResult_TW == "432") ||
                        (ReResult_TW == "433") ||
                        (ReResult_TW == "434") ||
                        (ReResult_TW == "435") ||
                        (ReResult_TW == "436") ||
                        (ReResult_TW == "437") ||
                        (ReResult_TW == "438") ||
                        (ReResult_TW == "439") ||
                        (ReResult_TW == "500") ||
                        (ReResult_TW == "502") ||
                        (ReResult_TW == "503") ||
                        (ReResult_TW == "504") ||
                        (ReResult_TW == "505") ||
                        (ReResult_TW == "506") ||
                        (ReResult_TW == "507") ||
                        (ReResult_TW == "508") ||
                        (ReResult_TW == "509") ||
                        (ReResult_TW == "510") ||
                        (ReResult_TW == "511") ||
                        (ReResult_TW == "512") ||
                        (ReResult_TW == "513") ||
                        (ReResult_TW == "514") ||
                        (ReResult_TW == "515") ||
                        (ReResult_TW == "516") ||
                        (ReResult_TW == "520") ||
                        (ReResult_TW == "521") ||
                        (ReResult_TW == "522") ||
                        (ReResult_TW == "523") ||
                        (ReResult_TW == "524") ||
                        (ReResult_TW == "525") ||
                        (ReResult_TW == "526") ||
                        (ReResult_TW == "527") ||
                        (ReResult_TW == "528") ||
                        (ReResult_TW == "530") ||
                        (ReResult_TW == "540") ||
                        (ReResult_TW == "541") ||
                        (ReResult_TW == "542") ||
                        (ReResult_TW == "544") ||
                        (ReResult_TW == "545") ||
                        (ReResult_TW == "546") ||
                        (ReResult_TW == "551") ||
                        (ReResult_TW == "552") ||
                        (ReResult_TW == "553") ||
                        (ReResult_TW == "555") ||
                        (ReResult_TW == "556") ||
                        (ReResult_TW == "557") ||
                        (ReResult_TW == "558") ||
                        (ReResult_TW == "600") ||
                        (ReResult_TW == "602") ||
                        (ReResult_TW == "604") ||
                        (ReResult_TW == "605") ||
                        (ReResult_TW == "606") ||
                        (ReResult_TW == "607") ||
                        (ReResult_TW == "608") ||
                        (ReResult_TW == "611") ||
                        (ReResult_TW == "612") ||
                        (ReResult_TW == "613") ||
                        (ReResult_TW == "614") ||
                        (ReResult_TW == "615") ||
                        (ReResult_TW == "616") ||
                        (ReResult_TW == "621") ||
                        (ReResult_TW == "622") ||
                        (ReResult_TW == "623") ||
                        (ReResult_TW == "624") ||
                        (ReResult_TW == "625") ||
                        (ReResult_TW == "630") ||
                        (ReResult_TW == "631") ||
                        (ReResult_TW == "632") ||
                        (ReResult_TW == "633") ||
                        (ReResult_TW == "634") ||
                        (ReResult_TW == "635") ||
                        (ReResult_TW == "637") ||
                        (ReResult_TW == "638") ||
                        (ReResult_TW == "640") ||
                        (ReResult_TW == "643") ||
                        (ReResult_TW == "654") ||
                        (ReResult_TW == "636") ||
                        (ReResult_TW == "646") ||
                        (ReResult_TW == "647") ||
                        (ReResult_TW == "648") ||
                        (ReResult_TW == "649") ||
                        (ReResult_TW == "651") ||
                        (ReResult_TW == "652") ||
                        (ReResult_TW == "653") ||
                        (ReResult_TW == "655") ||
                        (ReResult_TW == "700") ||
                        (ReResult_TW == "701") ||
                        (ReResult_TW == "702") ||
                        (ReResult_TW == "704") ||
                        (ReResult_TW == "708") ||
                        (ReResult_TW == "709") ||
                        (ReResult_TW == "710") ||
                        (ReResult_TW == "711") ||
                        (ReResult_TW == "712") ||
                        (ReResult_TW == "713") ||
                        (ReResult_TW == "714") ||
                        (ReResult_TW == "715") ||
                        (ReResult_TW == "716") ||
                        (ReResult_TW == "717") ||
                        (ReResult_TW == "718") ||
                        (ReResult_TW == "719") ||
                        (ReResult_TW == "720") ||
                        (ReResult_TW == "721") ||
                        (ReResult_TW == "722") ||
                        (ReResult_TW == "723") ||
                        (ReResult_TW == "724") ||
                        (ReResult_TW == "725") ||
                        (ReResult_TW == "726") ||
                        (ReResult_TW == "727") ||
                        (ReResult_TW == "730") ||
                        (ReResult_TW == "731") ||
                        (ReResult_TW == "732") ||
                        (ReResult_TW == "733") ||
                        (ReResult_TW == "734") ||
                        (ReResult_TW == "735") ||
                        (ReResult_TW == "736") ||
                        (ReResult_TW == "737") ||
                        (ReResult_TW == "741") ||
                        (ReResult_TW == "742") ||
                        (ReResult_TW == "743") ||
                        (ReResult_TW == "744") ||
                        (ReResult_TW == "800") ||
                        (ReResult_TW == "801") ||
                        (ReResult_TW == "802") ||
                        (ReResult_TW == "803") ||
                        (ReResult_TW == "804") ||
                        (ReResult_TW == "805") ||
                        (ReResult_TW == "806") ||
                        (ReResult_TW == "807") ||
                        (ReResult_TW == "811") ||
                        (ReResult_TW == "812") ||
                        (ReResult_TW == "813") ||
                        (ReResult_TW == "814") ||
                        (ReResult_TW == "815") ||
                        (ReResult_TW == "817") ||
                        (ReResult_TW == "819") ||
                        (ReResult_TW == "820") ||
                        (ReResult_TW == "821") ||
                        (ReResult_TW == "822") ||
                        (ReResult_TW == "823") ||
                        (ReResult_TW == "824") ||
                        (ReResult_TW == "825") ||
                        (ReResult_TW == "826") ||
                        (ReResult_TW == "827") ||
                        (ReResult_TW == "828") ||
                        (ReResult_TW == "829") ||
                        (ReResult_TW == "830") ||
                        (ReResult_TW == "831") ||
                        (ReResult_TW == "832") ||
                        (ReResult_TW == "833") ||
                        (ReResult_TW == "840") ||
                        (ReResult_TW == "842") ||
                        (ReResult_TW == "843") ||
                        (ReResult_TW == "844") ||
                        (ReResult_TW == "845") ||
                        (ReResult_TW == "846") ||
                        (ReResult_TW == "847") ||
                        (ReResult_TW == "848") ||
                        (ReResult_TW == "849") ||
                        (ReResult_TW == "851") ||
                        (ReResult_TW == "852") ||
                        (ReResult_TW == "880") ||
                        (ReResult_TW == "881") ||
                        (ReResult_TW == "882") ||
                        (ReResult_TW == "883") ||
                        (ReResult_TW == "884") ||
                        (ReResult_TW == "885") ||
                        (ReResult_TW == "890") ||
                        (ReResult_TW == "891") ||
                        (ReResult_TW == "892") ||
                        (ReResult_TW == "893") ||
                        (ReResult_TW == "894") ||
                        (ReResult_TW == "896") ||
                        (ReResult_TW == "900") ||
                        (ReResult_TW == "901") ||
                        (ReResult_TW == "902") ||
                        (ReResult_TW == "903") ||
                        (ReResult_TW == "904") ||
                        (ReResult_TW == "905") ||
                        (ReResult_TW == "906") ||
                        (ReResult_TW == "907") ||
                        (ReResult_TW == "908") ||
                        (ReResult_TW == "909") ||
                        (ReResult_TW == "911") ||
                        (ReResult_TW == "912") ||
                        (ReResult_TW == "913") ||
                        (ReResult_TW == "920") ||
                        (ReResult_TW == "921") ||
                        (ReResult_TW == "922") ||
                        (ReResult_TW == "923") ||
                        (ReResult_TW == "924") ||
                        (ReResult_TW == "925") ||
                        (ReResult_TW == "926") ||
                        (ReResult_TW == "927") ||
                        (ReResult_TW == "928") ||
                        (ReResult_TW == "931") ||
                        (ReResult_TW == "932") ||
                        (ReResult_TW == "940") ||
                        (ReResult_TW == "941") ||
                        (ReResult_TW == "942") ||
                        (ReResult_TW == "943") ||
                        (ReResult_TW == "944") ||
                        (ReResult_TW == "945") ||
                        (ReResult_TW == "946") ||
                        (ReResult_TW == "947") ||
                        (ReResult_TW == "950") ||
                        (ReResult_TW == "951") ||
                        (ReResult_TW == "952") ||
                        (ReResult_TW == "953") ||
                        (ReResult_TW == "954") ||
                        (ReResult_TW == "955") ||
                        (ReResult_TW == "956") ||
                        (ReResult_TW == "957") ||
                        (ReResult_TW == "958") ||
                        (ReResult_TW == "959") ||
                        (ReResult_TW == "961") ||
                        (ReResult_TW == "962") ||
                        (ReResult_TW == "963") ||
                        (ReResult_TW == "964") ||
                        (ReResult_TW == "965") ||
                        (ReResult_TW == "966") ||
                        (ReResult_TW == "970") ||
                        (ReResult_TW == "971") ||
                        (ReResult_TW == "972") ||
                        (ReResult_TW == "973") ||
                        (ReResult_TW == "974") ||
                        (ReResult_TW == "975") ||
                        (ReResult_TW == "976") ||
                        (ReResult_TW == "977") ||
                        (ReResult_TW == "978") ||
                        (ReResult_TW == "979") ||
                        (ReResult_TW == "981") ||
                        (ReResult_TW == "982") ||
                        (ReResult_TW == "983")) ProvincePostalMatch = 1;
                    break;
                case "GD":
                    if (ReResult == "51" || ReResult == "52") ProvincePostalMatch = 1; //GUANGDONG
                    break;
                case "HK":
                    if (ReResult == "99" || ReResult == "00") ProvincePostalMatch = 1; // XIANGGANG
                    break;
                case "SX":
                    if (ReResult == "03" || ReResult == "04") ProvincePostalMatch = 1; // SHANXI
                    break;
                case "HA":
                    if (ReResult == "45" || ReResult == "46" || ReResult == "47") ProvincePostalMatch = 1; // HENAN
                    break;
                case "FJ":
                    if (ReResult == "35" || ReResult == "36") ProvincePostalMatch = 1; // FUJIAN
                    break;
            }
            return ProvincePostalMatch;
        }

        public int ValidateCard(string type, string number, string securitynum)
        {
            int TypeNumberMatch = 0;

            switch (type)
            {
                case "American Express":
                    var regexAmex = @"^3[47][0-9]{13}$";
                    var matchAmex = Regex.Match(number, regexAmex, RegexOptions.IgnoreCase);
                    if (matchAmex.Success && securitynum.Length == 4) TypeNumberMatch = 1;
                    break;
                case "Diners Club":
                    var regexDC = @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
                    var matchDC = Regex.Match(number, regexDC, RegexOptions.IgnoreCase);
                    if (matchDC.Success && securitynum.Length == 3) TypeNumberMatch = 1;
                    break;
                case "Discover":
                    var regexDis = @"^6(?:011|5[0-9]{2})[0-9]{12}$";
                    var matchDis = Regex.Match(number, regexDis, RegexOptions.IgnoreCase);
                    if (matchDis.Success && securitynum.Length == 3) TypeNumberMatch = 1;
                    break;
                case "MasterCard":
                    var regexMaster = @"^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$";
                    var matchMaster = Regex.Match(number, regexMaster, RegexOptions.IgnoreCase);
                    if (matchMaster.Success && securitynum.Length == 3) TypeNumberMatch = 1;
                    break;
                case "UnionPay":
                    var regexUP = @"^(62|88)\d+$";
                    var matchUP = Regex.Match(number, regexUP, RegexOptions.IgnoreCase);
                    if (matchUP.Success && securitynum.Length == 3) TypeNumberMatch = 1;
                    break;
                case "Visa":
                    var regexVisa = @"^4[0-9]{12}(?:[0-9]{3})?$";
                    var matchVisa = Regex.Match(number, regexVisa, RegexOptions.IgnoreCase);
                    if (matchVisa.Success && securitynum.Length == 3) TypeNumberMatch = 1;
                    break;
            }

            return TypeNumberMatch;
        }

        public void SendEmail(string username, string email)
        {
            // Create an instance of MailMessage named mail.
            MailMessage mail = new MailMessage();

            // Set the sender (From), receiver (To), subject and 
            // message body fields of the mail message.
            mail.From = new MailAddress("comp3111_team106@cse.ust.hk",
                 "team 106 CLLW");
            mail.To.Add(email);
            mail.Subject = "Successful Creation of a Shipping Account";
            mail.Body = "Dear " + username + ",\n\nYour account has been created successfully!\n\nRegards,\nCLLW Team";

            // Create an instance of SmtpClient named emailServer.
            // Set the mail server to use as "smtp.ust.hk".
            SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

            // Send the message.
            emailServer.Send(mail);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}