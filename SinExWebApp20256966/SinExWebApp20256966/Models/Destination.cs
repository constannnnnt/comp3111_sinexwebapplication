﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SinExWebApp20256966.Models
{
    [Table("Destination")]
    public class Destination
    {
        [Key]
        public string City { get; set; }
        public string ProvinceCode { get; set; }
        // navigation propertyto Currency
        public virtual string CurrencyCode { get; set; }
        public virtual Currency Currencies { get; set; }
    }
}