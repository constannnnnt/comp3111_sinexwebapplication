﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SinExWebApp20256966.Models
{
    [Table("ShipmentTracking")]
    public class ShipmentTracking
    {
        public virtual int ShipmentTrackingID { get; set; }
        [DataType(DataType.Date)]
        public virtual DateTime Date { get; set; }
        [DataType(DataType.Time)]
        public virtual TimeSpan Time { get; set; }
        public virtual string Activity{ get; set; } //description
        public virtual string Location { get; set; }
        public virtual string Remarks { get; set; }

        public virtual string DeliveredTo { get; set; }
        public virtual string DeliveredAt { get; set; }
        public virtual string Status { get; set; } // final status(delivered. returned or lost)

        public virtual Shipment Shipment { get; set; }
        public virtual int WaybillId { get; set; }
    }
}