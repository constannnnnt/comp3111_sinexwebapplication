﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.Models
{
    [Table("PickupLocation")]
    public class PickupLocation
    {
        public virtual int PickupLocationID { get; set; }
        [Required]
        public virtual string NickName { get; set; }
        [Required]
        public virtual string Location { get; set; }


        public virtual int ShippingAccountId { get; set; }
        public virtual ShippingAccount ShippingAccount { get; set; }
    }
}