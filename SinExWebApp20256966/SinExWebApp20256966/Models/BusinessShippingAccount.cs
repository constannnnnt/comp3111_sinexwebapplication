﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SinExWebApp20256966.Models
{

    public class BusinessShippingAccount : ShippingAccount
    {

        [Required]
        [StringLength(70)]
        [Display(Name = "Contact Person Name", Order = 1)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your input contains invalid characters.")]
        public virtual string ContactPersonName { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Company", Order = 2)]
        [RegularExpression(@"^[A-Za-z0-9,!@&#~ _;+£$€¥<>«»“,.'*+-]*$", ErrorMessage = "Your input contains invalid characters.")]
        public virtual string CompanyName { get; set; }

        [StringLength(30)]
        [Display(Name = "Department", Order = 3)]
        public virtual string Department { get; set; }
    }
}