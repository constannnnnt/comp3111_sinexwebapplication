﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.Models
{
    [Table("Package")]
    public class Package
    {
        public virtual int PackageID { get; set; }
        [Required]
        public virtual string TypeSize { get; set; }
        [Required]
        public virtual string ContentDescription { get; set; }
        [Required]
        public virtual double CustomerWeight { get; set; }
        public virtual double? ActualWeight { get; set; }
        [Required]
        public virtual double Value { get; set; }
        [Required]
        public virtual string Currency { get; set; } // The currency of package value, not package cost

        public virtual double? Cost { get; set; } // initialized: in CNY 


        public virtual Shipment Shipment { get; set; }
        public virtual int WaybillId { get; set; }
    }
}
