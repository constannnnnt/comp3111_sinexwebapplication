﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.Models
{
    [Table("penaltyfee")]
    public class penaltyfee
    {
        public virtual int penaltyfeeID { get; set; }
        [Required]
        [Range(0, 5000, ErrorMessage = "The fee should be positive and not too high!")]
        public virtual decimal fee { get; set; }
    }
}