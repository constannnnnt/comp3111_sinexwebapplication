﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SinExWebApp20256966.Models
{
    [Table("PackageTypeSize")]
    public class PackageTypeSize
    {
        [Key]
        public virtual string Size { get; set; }
        public virtual string WeightLimit { get; set; }
        // Foreign key references PackageType
        public virtual int PackageTypeID { get; set; }
        //Foreign key references PackageType
        public virtual PackageType PackageType { get; set; }
    }
}