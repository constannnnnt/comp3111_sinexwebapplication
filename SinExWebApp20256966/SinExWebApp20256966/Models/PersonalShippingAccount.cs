﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace SinExWebApp20256966.Models
{

    public class PersonalShippingAccount : ShippingAccount
    {
        [Required]
        [StringLength(35)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your input contains invalid characters.")]
        [Display(Name = "First name", Order = 1)]
        public virtual string FirstName { get; set; }

        [Required]
        [StringLength(35)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your input contains invalid characters.")]
        [Display(Name = "Last name", Order = 2)]
        public virtual string LastName { get; set; }

    }
}