﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SinExWebApp20256966.Models
{
    public class SinExWebDatabaseContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SinExWebDatabaseContext() : base("name=SinExWebDatabaseContext")
        {
        }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.PackageType> PackageTypes { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.ServicePackageFee> ServicePackageFees { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.ServiceType> ServiceTypes { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.Destination> Destinations { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.Currency> Currencies { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.PackageTypeSize> PackageTypeSizes { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.Shipment> Shipments { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.ShippingAccount> ShippingAccounts { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.RecipientAddress> RecipientAddresses { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.PickupLocation> PickupLocations { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.Package> Packages { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.ShipmentTracking> ShipmentTrackings { get; set; }

        public System.Data.Entity.DbSet<SinExWebApp20256966.Models.penaltyfee> penaltyfees { get; set; }
    }
}
