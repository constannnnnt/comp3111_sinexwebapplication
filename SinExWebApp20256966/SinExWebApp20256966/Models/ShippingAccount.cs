﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SinExWebApp20256966.Models
{
    [Table("ShippingAccount")]
    public abstract class ShippingAccount
    {
        /* --------------------Information------------------*/
        public virtual int ShippingAccountId { get; set; }

        [StringLength(10, MinimumLength = 6)]
        public virtual string UserName { get; set; }

        /* --------------------Mailing Address Information------------------*/

        [StringLength(70)]
        [Display(Name = "Building", Order = 5)]
        public virtual string AddressBuilding { get; set; }

        [Required]
        [StringLength(35)]
        [Display(Name = "Street", Order = 6)]
        public virtual string AddressStreet { get; set; }

        [Required]
        [StringLength(25)]
        [Display(Name = "City", Order = 7)]
        public virtual string AddressCity { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2)]
        [Display(Name = "Province", Order = 8)]
        public virtual string AddressProvinceCode { get; set; }

        [StringLength(6, MinimumLength = 5)]
        [Display(Name = "Postal Code", Order = 9)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "The field Postable Code Number must be a number.")]
        public virtual string AddressPostalCode { get; set; }
        /* --------------------Mailing Address Information------------------*/

        /* --------------------Mailing Credit Card Information------------------*/
        [Required]
        // [Remote("CheckCreditCardType", "PersonalShippingAccounts", "BusinessShippingAccounts",
           // ErrorMessage = "Invalid Type, Please input American Express, Diners Club, Discover, MasterCard, UnionPay and Visa")]
        [Display(Name = "Type", Order = 10)]
        [RegularExpression(@"^American Express|Diners Club|Discover|MasterCard|UnionPay|Visa$",
            ErrorMessage = "Invalid Type, Please input American Express, Diners Club, Discover, MasterCard, UnionPay and Visa")]
        public virtual string CardType { get; set; }

        [Required]
        [StringLength(19, MinimumLength = 14)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "The field Card Number must be a number.")]
        [Display(Name = "Number", Order = 11)]
        public virtual string CardNumber { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 3)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "The field Security Number must be a number.")]
        [Display(Name = "Security Number", Order = 12)]
        public virtual string CardSecurityNumber { get; set; }

        [Required]
        [StringLength(70)]
        [Display(Name = "Cardholder Name", Order = 13)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your input contains invalid characters.")]
        public virtual string CardHolderName { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2)]
        [RegularExpression(@"^(0[1-9]|1[0-2])*$", ErrorMessage = "The field Expiry Month must be a number and in [01, 12].")]
        [Display(Name = "Expire Month", Order = 14)]
        public virtual string CardExpireMonth { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4)]
        [RegularExpression(@"^20([1-9][7-9]|[2-9][0-9])*$", ErrorMessage = "The field Expiry Year must be a number and after 2017.")]
        [Display(Name = "Expire Year", Order = 15)]
        public virtual string CardExpireYear { get; set; }
        /* --------------------Mailing Credit Card Information------------------*/

        [Required]
        [StringLength(14, MinimumLength = 8)]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Phone number must be numeric.")]
        [Display(Name = "Phone", Order = 3)]
        public virtual string PhoneNumber { get; set; }

        [Required]
        [StringLength(30)]
        /*[RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
            + "@"
            + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$",
            ErrorMessage = "Please enter a valid email address.")]*/
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", Order = 4)]
        public virtual string Email { get; set; }
        /* --------------------Information------------------*/

        public virtual ICollection<Shipment> Shippments { get; set; }

        public virtual ICollection<PickupLocation> PickupLocations { get; set; }
        public virtual ICollection<RecipientAddress> RecipientAddresses { get; set; }
    }
}