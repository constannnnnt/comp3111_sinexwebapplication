﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.Models
{
    [Table("RecipientAddress")]
    public class RecipientAddress
    {
        public virtual int RecipientAddressID { get; set; }
        [Required]
        [Display(Name = "Nick Name")]
        public virtual string NickName { get; set; }
        [StringLength(70)]
        public virtual string Building { get; set; }
        [Required]
        [StringLength(35)]
        public virtual string Street { get; set; }
        [Required]
        [StringLength(25)]
        public virtual string City { get; set; }
        public virtual string Province { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "The field Postable Code must be a number.")]
        [StringLength(6, MinimumLength = 5)]
        [Display(Name = "Postal Code")]
        public virtual string PostalCode { get; set; }


        public virtual int ShippingAccountId { get; set; }
        public virtual ShippingAccount ShippingAccount { get; set; }
    }
}