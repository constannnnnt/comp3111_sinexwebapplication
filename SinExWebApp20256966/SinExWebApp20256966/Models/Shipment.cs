﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SinExWebApp20256966.Models
{
    [Table("Shipment")]
    public class Shipment
    {
        [Key]
        public virtual int WaybillId { get; set; }
        [StringLength(10)]
        [RegularExpression(@"^[A-za-z0-9]*$", ErrorMessage = "The reference number must be alphanumeric.")]
        public virtual string ReferenceNumber { get; set; }
        [Required]
        public virtual string ServiceType { get; set; }
        [DataType(DataType.Date)]
        public virtual DateTime ShippedDate { get; set; }
        [DataType(DataType.Date)]
        public virtual DateTime DeliveredDate { get; set; }
        [Required]
        [StringLength(35)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your input contains invalid characters.")]
        public virtual string RecipientName { get; set; }
        public virtual int NumberOfPackages { get; set; }
        public virtual string Origin { get; set; }
        public virtual string Destination { get; set; }
        public virtual string Status { get; set; }  // final status(delivered, returned or lost)
        [Required]
        public virtual int ShippingAccountId { get; set; }




        //New Attributes for A3

        /*For Creation*/

        //Recipient Information
        public virtual string CompanyName { get; set; }
        public virtual string DepartmentName { get; set; }
        //Delivery Address
        public virtual string Building { get; set; }
        public virtual string Street { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        [Required]
        public virtual string PhoneNumber { get; set; }
        [Required]
        public virtual string EmailAddress { get; set; }

        //Shipment Information
        public virtual ICollection<Package> Packages { get; set; }
        [Required]
        public virtual int ShipmentPayerID { get; set; }
        [Required]
        public virtual int DutiesTaxesPayerID { get; set; }
        public virtual string ShipmentCurrency { get; set; }
        public virtual string DutiesTaxesCurrency { get; set; }

        //Email Notification
        [Required]
        public virtual bool NotifySender { get; set; }
        [Required]
        public virtual bool NotifyRecipient { get; set; }

        /*For Confirmation*/
        public virtual bool ConfirmStatus { get; set; }
        public virtual string PickupType { get; set; }
        [DataType(DataType.DateTime)]
        public virtual DateTime? PickupTime { get; set; }
        public virtual string PickupLocation { get; set; }

        /*For Deletion*/
        public virtual bool DeleteStatus { get; set; }
        public virtual bool PickupStatus { get; set; }

        /*For Payment(feedback)*/
        public virtual string ShipemtnPaymentAuthorizationCode { get; set; }
        public virtual string DutiesTaxesPaymentAuthorizationCode { get; set; }

        // Cost of whole shipment, duty and tax
        public virtual double? ShipmentAmount { get; set; }
        public virtual double? DutiesAmount { get; set; }
        public virtual double? TaxesAmount { get; set; }

        // Total invoice amount, used only in generate invoice history if there is only one payer
        public virtual double? TotalInvoiceAmount { get; set; }

        // shipment tracking
        public virtual ICollection<ShipmentTracking> ShipmentTrackingInformation { get; set; }
    }
}
