﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SinExWebApp20256966.ViewModels
{
    public class CostCalculationViewModel
    {
        public virtual string origin { get; set; }
        public virtual string destination { get; set; }
        public virtual int PackageNumber { get; set; }
        public virtual string ServiceType { get; set; }
        public virtual string Currency { get; set; }


        public virtual IList<string> PackageInfo { get; set; }
        public virtual IList<string> WeightInfo { get; set; }

        public virtual List<SelectListItem> Origins { get; set; }
        public virtual List<SelectListItem> Destinations { get; set; }
        public virtual List<SelectListItem> ServiceTypes { get; set; }
        public virtual List<SelectListItem> CurrencyCodes { get; set; }
        public virtual List<SelectListItem> Sizes { get; set; }
    }
}