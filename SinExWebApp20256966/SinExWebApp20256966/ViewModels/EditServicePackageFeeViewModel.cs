﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.ViewModels
{
    public class EditServicePackageFeeViewModel
    {
        [Required]
        public virtual string PackageType { get; set; }
        [Required]
        public virtual string ServiceType { get; set; }
        [Required]
        public virtual double fee { get; set; }
        [Required]
        public virtual double minimunfee { get; set; }
    }
}