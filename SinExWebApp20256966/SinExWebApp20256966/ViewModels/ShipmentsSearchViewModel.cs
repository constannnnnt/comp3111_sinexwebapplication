﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SinExWebApp20256966.ViewModels
{
    public class ShipmentsSearchViewModel
    {
        public virtual int? ShippingAccountId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public virtual DateTime? ShippedStartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public virtual DateTime? ShippedEndDate { get; set; }
        public virtual List<SelectListItem> ShippingAccounts { get; set; }
        public virtual List<SelectListItem> ShippedStartDates { get; set; }
        public virtual List<SelectListItem> ShippedEndDates { get; set; }
    }
}