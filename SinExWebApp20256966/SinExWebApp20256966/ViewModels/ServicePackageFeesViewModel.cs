﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SinExWebApp20256966.Models;
using SinExWebApp20256966.ViewModels;
using System.Web.Mvc;

namespace SinExWebApp20256966.ViewModels
{
    public class ServicePackageFeesViewModel
    {
        public virtual IEnumerable<ServicePackageFee> Fees { get; set; }
        public virtual double Rates { get; set; }
        public virtual string CurrencyCode { get; set;}
        public virtual List<SelectListItem> CurrencyCodes { get; set; }
        public virtual List<string> ServiceTypes { get; set; }
        public virtual List<string> PackageTypes { get; set; }
    }
}