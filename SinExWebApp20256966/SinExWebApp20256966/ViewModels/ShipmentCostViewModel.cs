﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SinExWebApp20256966.Models;
using SinExWebApp20256966.ViewModels;
using System.Web.Mvc;

namespace SinExWebApp20256966.ViewModels
{
    public class ShipmentCostViewModel
    {
        public virtual IEnumerable<ServicePackageFee> Fees { get; set; }
        public virtual double Rates { get; set; }
        public virtual double Cost { get; set; }
        public virtual string CurrencyCode { get; set; }
        public virtual string Package { get; set; }
        public virtual string Service { get; set; }
        public virtual string Destination { get; set; }
        public virtual string Origin { get; set; }
        public virtual double Weight { get; set; }
        public virtual string Size { get; set; }
        public virtual List<SelectListItem> CurrencyCodes { get; set; }
        public virtual List<string> ServiceTypes { get; set; }
        public virtual List<string> PackageTypes { get; set; }
        public virtual List<SelectListItem> PackageTypeList { get; set; }
        public virtual List<SelectListItem> ServiceTypesList { get; set; }
        public virtual List<SelectListItem> Origins { get; set; }
        public virtual List<SelectListItem> Destinations { get; set; } 
        public virtual List<SelectListItem> Sizes { get; set; }
    }
}