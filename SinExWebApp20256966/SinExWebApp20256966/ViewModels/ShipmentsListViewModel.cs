﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SinExWebApp20256966.ViewModels
{
    public class ShipmentsListViewModel
    {
        public virtual int WaybillId { get; set; }
        public virtual string ServiceType { get; set; }
        public virtual DateTime ShippedDate { get; set; }
        public virtual DateTime DeliveredDate { get; set; }
        public virtual string RecipientName { get; set; }
        public virtual int NumberOfPackages { get; set; }
        public virtual string Origin { get; set; }
        public virtual string Destination { get; set; }
        public virtual int ShippingAccountId { get; set; }
        public virtual int ShipmentPayerId { get; set; }
        public virtual int DutiesAndTaxesPayerId { get; set; }
        public virtual double? ShipmentAmount { get; set; }
        public virtual double? DutiesAndTaxesAmount { get; set; }
        public virtual string ShipmentCurrency { get; set; }
        public virtual string DutiesTaxesCurrency { get; set; }
        public virtual double? TotalInvoiceAmount { get; set; }
        public virtual string TotalAmountCurrency { get; set; }
    }
}