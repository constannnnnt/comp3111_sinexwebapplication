﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;                                           

namespace SinExWebApp20256966.ViewModels                            
{
    public class CreateShipmentViewModel
    {
        [Display(Name = "Reference Number")]
        [StringLength(10)]
        [RegularExpression(@"^[A-za-z0-9]*$", ErrorMessage = "The reference number must be alphanumeric")]
        public virtual string ReferenceNumber { get; set; }
        //[Required]
        [StringLength(25)]
        [Display(Name = "Origin City")]
        public virtual string OriginCity { get; set; }


        [Required(ErrorMessage = "Name of the recipient is required")]
        [Display(Name = "Recipient Name")]
        [StringLength(35)]
        [RegularExpression(@"^[A-Za-z ,.'-]*$", ErrorMessage = "Your name contains valid characters.")]
        public virtual string RecipientName { get; set; }

        [Display(Name = "Company Name")]
        [StringLength(40)]
        [RegularExpression(@"^[A-Za-z0-9,!@&#~ _;+£$€¥<>«»“,.'*+-]*$", ErrorMessage = "Your input contains invalid characters.")]
        public virtual string CompanyName { get; set; }

        [StringLength(30)]
        [Display(Name = "Department Name")]
        public virtual string DepartmentName { get; set; }
         
        [Display(Name = "Use Saved Address")]
        public virtual string UseSavedAddress { get; set; }
        [Display(Name = "Saved Recipient Address")]
        public virtual string RecipientAddressNickName { get; set; }
        public virtual string Building { get; set; }
        public virtual string Street { get; set; }
        public virtual string City { get; set; }
        public virtual string PostalCode { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "The field Phone Number must be a number.")]
        [Display(Name = "Phone Number")]
        [StringLength(14, MinimumLength = 8)]
        public virtual string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Email address is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid email address.")]
        [Display(Name = "Email Address")]
        [StringLength(30)]
        public virtual string EmailAddress { get; set; }


        [Display(Name = "Package Number")]
        public virtual int? PackageNumber { get; set; }
        public virtual List<string> TypeSizes { get; set; }
        public virtual List<SelectListItem> TypeSizeList { get; set; }
        public virtual List<string> ContentDescriptions { get; set; }
        
        public virtual List<string> CustomerWeights { get; set; }
        public virtual List<string> Values { get; set; }
        public virtual List<string> Currencies { get; set; }
        public virtual List<SelectListItem> CurrencyList { get; set; }


        [Required(ErrorMessage = "Service type is required")]
        [Display(Name = "Service Type")]
        public virtual string ServiceType { get; set; }
        [Required(ErrorMessage = "Please indicate the shipment payer")]
        [Display(Name = "Shipment Payer")]
        
        public virtual string ShipmentPayer { get; set; }
        [Required(ErrorMessage = "Please indicate the duty/taxes payer")]
        [Display(Name = "Duties and Taxes Payer")]
        
        public virtual string DutiesTaxesPayer { get; set; }
        [Display(Name = "Recipient Account Number")]
        [Remote("doesNumberExist", "Shipments", HttpMethod = "POST", ErrorMessage = "Account Number does not exist. Please enter a valid number.")]
        [StringLength(12, MinimumLength = 12, ErrorMessage = "Invalid Account Number.")]
        public virtual string RecipientAccountNumber { get; set; }


        [Required(ErrorMessage = "Please indicate whether to notify the sender")]
        [Display(Name = "Notify Sender?")]
        public virtual string NotifySender { get; set; }
        [Required(ErrorMessage = "Please indicate whether to notify the recipient")]
        [Display(Name = "Notify Recipient?")]
        public virtual string NotifyRecipient { get; set; }
    }
}