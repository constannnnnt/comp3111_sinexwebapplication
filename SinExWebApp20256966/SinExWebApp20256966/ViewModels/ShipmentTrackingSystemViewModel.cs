﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SinExWebApp20256966.ViewModels
{
    public class ShipmentTrackingSystemViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        public virtual DateTime Date { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public virtual TimeSpan Time { get; set; }
        [Required]
        public virtual string Activity { get; set; } //description
        [Required]
        public virtual string Location { get; set; }
        public virtual string Remarks { get; set; }

        public virtual string DeliveredTo { get; set; }
        public virtual string DeliveredAt { get; set; }

        public virtual string WaybillNumber { get; set; }
    }
}