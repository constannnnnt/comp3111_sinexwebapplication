﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SinExWebApp20256966.ViewModels
{
    public class ShipmentConfirmationViewModel
    {
        public virtual string PickupLocationNickname { get; set; }
        public virtual string NewPickupLocation { get; set; }
        [Required]
        public virtual string PickupType { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]

        public virtual DateTime? PickupDate { get; set; }
        [DataType(DataType.Time,ErrorMessage = "Please input as HH:mm")]
        [Display(Name = "Pickup Time (HH:mm)")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public virtual TimeSpan? PickupTime { get; set; }
    }
}