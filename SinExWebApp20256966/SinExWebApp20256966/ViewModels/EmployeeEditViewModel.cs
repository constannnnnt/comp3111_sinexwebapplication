﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SinExWebApp20256966.Models;

namespace SinExWebApp20256966.ViewModels
{
    public class EmployeeEditViewModel
    {
        public virtual string waybillId { get; set; }
        public virtual string ServiceType { get; set; }
        public virtual string RecipientName { get; set; }
        public virtual string Building { get; set; }
        public virtual string Street { get; set; }
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual List<Package> Packages { get; set; }
        public virtual double? DutiesAmount { get; set; }
        public virtual double? TaxesAmount { get; set; }

        public virtual List<double?> ActualWeight { get; set; }
    }
}