﻿using NUnit.Framework;
using SinExWebApp20256966.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinExWebApp20256966.Controllers.Tests
{
    [TestFixture()]
    public class AccountControllerTests
    {
        AccountController AC = new AccountController();
        // For all the test cases below, it must first pass the model constraint
        
        [TestCase("Zhongshan", "102365", 0)] // 0 means false, 1 means true
        [TestCase("Guangzhou", "510000", 1)] // 0 means false, 1 means true
        [TestCase("Hong Kong", "000000", 1)] // 0 means false, 1 means true
        [TestCase("Beijing", "101300", 1)] // 0 means false, 1 means true
        [TestCase("Macau", "000000", 1)] // 0 means false, 1 means true
        [TestCase("Hong Kong", "999077", 1)] // 0 means false, 1 means true
        [TestCase("Macau", "999078", 1)] // 0 means false, 1 means true
        [TestCase("Taipei", "10000", 1)] // 0 means false, 1 means true
        [TestCase("Macau", "10200", 0)] // 0 means false, 1 means true
        [TestCase("macau", "10200", 0)] // 0 means false, 1 means true
        public void ValidateCityPostalCodeTest(string City, string PostalCode, int expect_result)
        {
            int result = AC.ValidateCityPostalCode(PostalCode, City);
            Assert.That(expect_result, Is.EqualTo(result));
        }

        [TestCase("GD", "523456", 1)] // 0 means false, 1 means true
        [TestCase("GD", "515672", 1)] // 0 means false, 1 means true
        [TestCase("GD", "502346", 0)] // 0 means false, 1 means true
        [TestCase("GD", "537899", 0)] // 0 means false, 1 means true
        [TestCase("HK", "000000", 1)] // 0 means false, 1 means true
        [TestCase("MC", "000000", 1)] // 0 means false, 1 means true
        [TestCase("mc", "000000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "896000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "983000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "970000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "984000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "084000", 0)] // 0 means false, 1 means true
        public void ValidateProvincePostalCodeTest(string province, string PostalCode, int expect_result)
        {
            int result = AC.ValidateProvincePostalCode(PostalCode, province);
            Assert.That(expect_result, Is.EqualTo(result));
        }

        [TestCase("visa", "411111111111111", "233",0)] // 0 means false, 1 means true, because we limit the type to be exactly Visa
        [TestCase("Visa", "4111111111111111", "233", 1)]
        [TestCase("Visa", "41111111111111111", "233", 0)]
        [TestCase("Visa", "4012888888881881", "233", 1)]
        [TestCase("Visa", "4012888888881881", "2333", 0)]
        [TestCase("Visa", "4012888888881881", "23", 0)]
        [TestCase("American Express", "378282246310005", "2333", 1)]
        [TestCase("American Express", "371449635398431", "2333", 1)]
        [TestCase("american express", "371449635398431", "2333", 0)]
        [TestCase("American Express", "371449635398431", "233", 0)]
        [TestCase("American Express", "351449635398431", "2333", 0)]
        [TestCase("Diners Club", "30596309025904", "233", 1)]
        [TestCase("Diners Club", "38520000023237", "233", 1)]
        [TestCase("diners Club", "38520000023237", "233", 0)]
        [TestCase("Diners Club", "38520000023237", "2333", 0)]
        [TestCase("Diners Club", "37520000023237", "233", 0)]
        [TestCase("Discover", "6011111111111117", "233", 1)]
        [TestCase("Discover", "6011000990139424", "233", 1)]
        [TestCase("Discover ", "6011111111111117", "233", 0)]
        [TestCase("Discover", "6012111111111117", "233", 0)]
        [TestCase("MasterCard", "5555555555554444", "233", 1)]
        [TestCase("MasterCard", "5105105105105100", "233", 1)]
        [TestCase("MasterCard", "5105105105105100", "2334", 0)]
        [TestCase("mastercard", "5105105105105100", "233", 0)]
        [TestCase("MasterCard", "5605105105105100", "233", 0)]
        [TestCase("UnionPay", "62125632894652145", "233", 1)]
        [TestCase("UnionPay", "88125413956832145", "233", 1)]
        [TestCase("Unionpay", "88125413956832145", "233", 0)]
        [TestCase("unionPay", "88125413956832145", "233", 0)]
        [TestCase("UnionPay", "62125632894652145", "2332", 0)]
        public void ValidateCardTest(string type, string number, string securitynum, int expect_result)
        {
            int result = AC.ValidateCard(type, number, securitynum);
            Assert.That(expect_result, Is.EqualTo(result));
        }
    }
}