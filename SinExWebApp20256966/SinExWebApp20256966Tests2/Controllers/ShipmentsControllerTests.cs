﻿using NUnit.Framework;
using SinExWebApp20256966.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinExWebApp20256966.Controllers.Tests
{
    [TestFixture()]
    public class ShipmentsControllerTests
    {
        ShipmentsController sc = new ShipmentsController();

        [TestCase("Pak - large - 450x550mm", "Test", "12.35", "2500", "CNY", "Pleasse input a valid weight(a positive number to one decimal place)")]
        [TestCase("Envelope - 250x350mm", "Test", "-1", "2500", "CNY", "Pleasse input a valid weight(a positive number to one decimal place)")]
        [TestCase("Pak - large - 450x550mm", "Test", "12", "not an integer", "HKD", "Pleasse input a valid value(a positive integer)")]
        [TestCase("Envelope - 250x350mm", "Test", "0.5", "2500", "CNY", "correct")]
        [TestCase("Envelope - 250x350mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Envelope - 250x350mm", "Test", "0.9", "2500", "CNY", "correct")]
        [TestCase("Envelope - 250x350mm", "Test", "1.1", "2500", "CNY", "For safety reasons, please don't add a Envelop weights larger than 1kg.")]
        [TestCase("Pak - small - 350x400mm", "Test", "10", "2500", "CNY", "correct")]
        [TestCase("Pak - small - 350x400mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Pak - small - 350x400mm", "Test", "19.9", "2500", "CNY", "correct")]
        [TestCase("Pak - small - 350x400mm", "Test", "20.1", "2500", "CNY", "For safety reasons, please don't add a Pak weights larger than 20kg.")]
        [TestCase("Pak - large - 450x550mm", "Test", "10", "2500", "CNY", "correct")]
        [TestCase("Pak - large - 450x550mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Pak - large - 450x550mm", "Test", "19.9", "2500", "CNY", "correct")]
        [TestCase("Pak - large - 450x550mm", "Test", "20.1", "2500", "CNY", "For safety reasons, please don't add a Pak weights larger than 20kg.")]
        [TestCase("Tube - 1000x80mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Tube - 1000x80mm", "Test", "50", "2500", "CNY", "correct")]
        [TestCase("Tube - 1000x80mm", "Test", "99.9", "2500", "CNY", "correct")]
        [TestCase("Tube - 1000x80mm", "Test", "100.1", "5000", "MOP", "For safety reasons, please don't add a package weights larger than 100kg.")]
        [TestCase("Box - small - 300x250x150mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Box - small - 300x250x150mm", "Test", "50", "2500", "CNY", "correct")]
        [TestCase("Box - small - 300x250x150mm", "Test", "99.9", "2500", "CNY", "correct")]
        [TestCase("Box - small - 300x250x150mm", "Test", "100.1", "5000", "MOP", "For safety reasons, please don't add a package weights larger than 100kg.")]
        [TestCase("Box - medium - 400x350x250mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Box - medium - 400x350x250mm", "Test", "50", "2500", "CNY", "correct")]
        [TestCase("Box - medium - 400x350x250mm", "Test", "99.9", "2500", "CNY", "correct")]
        [TestCase("Box - medium - 400x350x250mm", "Test", "100.1", "5000", "MOP", "For safety reasons, please don't add a package weights larger than 100kg.")]
        [TestCase("Box - large - 500x450x350mm", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Box - large - 500x450x350mm", "Test", "50", "2500", "CNY", "correct")]
        [TestCase("Box - large - 500x450x350mm", "Test", "99.9", "2500", "CNY", "correct")]
        [TestCase("Box - large - 500x450x350mm", "Test", "100.1", "5000", "MOP", "For safety reasons, please don't add a package weights larger than 100kg.")]
        [TestCase("Customer", "Test", "0.1", "2500", "CNY", "correct")]
        [TestCase("Customer", "Test", "50", "2500", "CNY", "correct")]
        [TestCase("Customer", "Test", "99.9", "2500", "CNY", "correct")]
        [TestCase("Customer", "Test", "100.1", "5000", "MOP", "For safety reasons, please don't add a package weights larger than 100kg.")]

        public void PackagesValidationTest(string TypeSize, string ContentDescription, string CustomerWeight, string Value, string Currency, string expected_result)
        {
            string result = sc.PackagesValidation(TypeSize, ContentDescription, CustomerWeight, Value, Currency);
            Assert.That(result, Is.EqualTo(expected_result));
        }

        [TestCase("GD", "523456", 1)] // 0 means false, 1 means true
        [TestCase("GD", "515672", 1)] // 0 means false, 1 means true
        [TestCase("GD", "502346", 0)] // 0 means false, 1 means true
        [TestCase("GD", "537899", 0)] // 0 means false, 1 means true
        [TestCase("HK", "000000", 1)] // 0 means false, 1 means true
        [TestCase("MC", "000000", 1)] // 0 means false, 1 means true
        [TestCase("mc", "000000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "896000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "983000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "970000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "984000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "084000", 0)] // 0 means false, 1 means true
        public void ProvincePostalMatchFuncTest(string pc, string postalcode, int expected_result)
        {
            int result = sc.ProvincePostalMatchFunc(pc, postalcode);
            Assert.That(result, Is.EqualTo(expected_result));
        }
    }
}