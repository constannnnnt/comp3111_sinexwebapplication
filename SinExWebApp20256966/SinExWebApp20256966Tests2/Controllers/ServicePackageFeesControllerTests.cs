﻿using NUnit.Framework;
using SinExWebApp20256966.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinExWebApp20256966.Controllers.Tests
{
    [TestFixture()]
    public class ServicePackageFeesControllerTests
    {
        ServicePackageFeesController con;
        [OneTimeSetUp]
        public void TestSetUp() { con = new ServicePackageFeesController(); }



        [TestCase(2, 0, ExpectedResult = "Weight should be a positive decimal number in kg to one decimal place.")]
        [TestCase(1, 0, ExpectedResult = "np")]
        [TestCase(3, 0, ExpectedResult = "Weight should be a positive decimal number in kg to one decimal place.")]
        [TestCase(4, 200, ExpectedResult = "It's too heavy!!!")]
        [TestCase(5, 800, ExpectedResult = "It's too heavy!!!")]
        [TestCase(2, 5.1, ExpectedResult = "np")]
        [TestCase(4, 3.2, ExpectedResult = "np")]
        public string checkweightTest(int i, decimal b)
        {
            
            string s = con.checkweight(i, b);

            return s;
        }

        [TestCase("Envelope - 250x350mm", "160.0","1.0", "0", "500", "0", ExpectedResult = 160)]
        [TestCase("Envelope - 250x350mm", "50.0", "1.0", "0", "500", "0", ExpectedResult = 50)]
        [TestCase("Pak - small - 350x400mm", "100.0", "1.0", "160.0", "500", "1", ExpectedResult = 160)]
        [TestCase("Pak - large - 450x550mm", "100.0", "1.0", "160.0", "500", "2", ExpectedResult = 200)]
        [TestCase("Box - small - 300x250x150mm", "110.0", "1.0", "160.0", "500", "12", ExpectedResult = 1820)]
        [TestCase("Box - medium - 400x350x250mm", "110.0", "1.0", "160.0", "500", "12", ExpectedResult = 1320)]
        [TestCase("Box - large - 500x450x350mm", "30.0", "1.0", "30.0", "500", "22", ExpectedResult = 660)]
        [TestCase("Tube - 1000x80mm", "80.0", "1.17", "130.0", "500", "5", ExpectedResult = 468)]
        public decimal calcostTest(string package, decimal fee, decimal rate, decimal MinimunFee, decimal penaltyfee, decimal we)
        {
            decimal s = con.calcost( package,  fee,  rate,  MinimunFee,  penaltyfee,  we);
            return s;
        }

        [TestCase("HKD","SH","HN", "Envelope", "Same Day", "250x350mm", ExpectedResult = true)]
        [TestCase("HKD", "ACD", "DFG", "Envelope", "Same Day", "250x350mm", ExpectedResult = true)]

        public bool checknullTest(string CurrencyCode, string Origin, string Destination, string Package, string Service, string Size)
        {
            return true;
        }
    }
}