﻿using NUnit.Framework;
using SinExWebApp20256966.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinExWebApp20256966.Controllers.Tests
{
    [TestFixture()]
    public class ShipmentTrackingsControllerTests
    {
        private ShipmentTrackingsController shipTrac = new ShipmentTrackingsController();
        [TestCaseSource("WaybillNumberCases")]
        public void validWaybillNumberTest(string WaybillNumber, bool expect)
        {
            bool result = shipTrac.validWaybillNumber(WaybillNumber);
            Assert.That(expect, Is.EqualTo(result));
        }

        static object[] WaybillNumberCases =
        {
            new object[] { "0000000000000001", true },
            new object[] { "0000000000014579", true },
            new object[] { "0000sa-098f97101", false },
            new object[] { "0000001", false },
            new object[] { "asdfkushdkfjhfds", false },
            new object[] { "@3khuAsdk1345742", false },
            new object[] { "()*asdjlakfhasda", false },
            new object[] { "1299aGASJFA&*^#!", false },
            new object[] { ")(!@(*$@)asfjali", false },
            new object[] { "ASDILGJ125631245", false },
        };
    }
}