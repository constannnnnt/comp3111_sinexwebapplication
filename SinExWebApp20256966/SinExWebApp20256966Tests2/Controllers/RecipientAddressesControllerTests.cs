﻿using NUnit.Framework;
using SinExWebApp20256966.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinExWebApp20256966.Controllers.Tests
{
    [TestFixture()]
    public class RecipientAddressesControllerTests
    {
        RecipientAddressesController RAC = new RecipientAddressesController();

        [TestCase("GD", "523456", 1)] // 0 means false, 1 means true
        [TestCase("GD", "515672", 1)] // 0 means false, 1 means true
        [TestCase("GD", "502346", 0)] // 0 means false, 1 means true
        [TestCase("GD", "537899", 0)] // 0 means false, 1 means true
        [TestCase("HK", "000000", 1)] // 0 means false, 1 means true
        [TestCase("MC", "000000", 1)] // 0 means false, 1 means true
        [TestCase("mc", "000000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "896000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "983000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "970000", 1)] // 0 means false, 1 means true
        [TestCase("TW", "984000", 0)] // 0 means false, 1 means true
        [TestCase("TW", "084000", 0)] // 0 means false, 1 means true
        public void ValidateProvincePostalCodeTest(string province, string PostalCode, int expect_result)
        {
            int result = RAC.ValidateProvincePostalCode(PostalCode, province);
            Assert.That(expect_result, Is.EqualTo(result));
        }
    }
}